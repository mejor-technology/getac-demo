// Garden Gnome Software - Skin
// Pano2VR 6.1.13/18080
// Filename: sub-0922.ggsk
// Generated 2021-09-22T14:09:08

function pano2vrSkin(player,base) {
	player.addVariable('map_open', 2, true);
	player.addVariable('vis_info_popup', 2, false);
	player.addVariable('vis_info_popup_2', 2, false);
	player.addVariable('vis_info_popup_3', 2, false);
	player.addVariable('vis_info_vr_popup', 2, false);
	player.addVariable('ht_anima_A', 2, false);
	player.addVariable('ht_anima_B', 2, false);
	player.addVariable('ht_anima_C', 2, false);
	player.addVariable('ExtValue', 1, 0);
	player.addVariable('ExtValueSu', 0, "en");
	player.addVariable('PanoOs', 0, "");
	player.addVariable('infoNo', 1, 0);
	player.addVariable('infoPano', 1, 0);
	player.addVariable('info2Pano', 1, 1);
	player.addVariable('info3Pano', 1, 109);
	player.addVariable('infolink', 0, "https:\/\/transportation-logistics-virtualexhibition.getac.com\/introductions\/");
	player.addVariable('infoMlink', 0, "https:\/\/transportation-logistics-virtualexhibition.getac.com\/mobile\/introductions\/");
	player.addVariable('infolinkPre', 0, "<iframe src=\"");
	player.addVariable('infolinkSuf', 0, "\" title=\"ok\" width=\"100%\" height=\"100%\" allowTransparency=\"true\" frameborder=\"0\"><\/iframe>");
	player.addVariable('arPre', 0, "<iframe src=\"https:\/\/transportation-logistics-virtualexhibition.getac.com\/model\/");
	player.addVariable('arSuf', 0, "\" name=\"panorama\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" frameborder=\"0\" style=\"float:left; margin-left:0px; width: 100%; height: 100%\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"><\/iframe>");
	player.addVariable('urlPre', 0, "https:\/\/www.getac.com\/");
	player.addVariable('urlSuf', 0, "");
	player.addVariable('UA_ID', 0, "GTM-5KQG6G5");
	player.addVariable('UA_category', 0, "showroom clicked");
	var me=this;
	var skin=this;
	var flag=false;
	var nodeMarker=[];
	var activeNodeMarker=[];
	var hotspotTemplates={};
	var skinKeyPressed = 0;
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	this.ggUserdata=player.userdata;
	this.lastSize={ w: -1,h: -1 };
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=[];
	this.elementMouseOver=[];
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	var hs,el,els,elo,ela,elHorScrollFg,elHorScrollBg,elVertScrollFg,elVertScrollBg,elCornerBg;
	if (typeof document.body.style['transform'] == 'undefined') {
		for(var i=0;i<prefixes.length;i++) {
			if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
				cssPrefix='-' + prefixes[i].toLowerCase() + '-';
				domTransition=prefixes[i] + 'Transition';
				domTransform=prefixes[i] + 'Transform';
			}
		}
	}
	
	player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	this.callNodeChange=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggNodeChange) {
				e.ggNodeChange();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	player.addListener('changenode', function() { me.ggUserdata=player.userdata; me.callNodeChange(me.divSkin); });
	
	var parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=[];
		var stack=[];
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		var hs='';
		this.ggCurrentTime=new Date().getTime();
		el=me._cnt_helper=document.createElement('div');
		el.ggId="Cnt_helper";
		el.ggDx=0;
		el.ggDy=2;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 89px;';
		hs+='left : -10000px;';
		hs+='opacity : 0.001;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 52px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._cnt_helper.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._cnt_helper.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._text_5=document.createElement('div');
		els=me._text_5__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 5";
		el.ggDx=0;
		el.ggDy=-31;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 26px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 52px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 52px;';
		hs+='height: 26px;';
		hs+='pointer-events: none;';
		hs+='background: #ffffff;';
		hs+='border: 0px solid #000000;';
		hs+='color: #000000;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		els.setAttribute('style',hs);
		me._text_5.ggUpdateText=function() {
			var hs=player.hotspot.title;
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._text_5.ggUpdateText();
		player.addListener('activehotspotchanged', function() {
			me._text_5.ggUpdateText();
		});
		el.appendChild(els);
		me._text_5.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_5.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._cnt_helper.appendChild(me._text_5);
		el=me._marker_mp3play=document.createElement('div');
		el.ggMarkerNodeId='';
		el.ggMarkerInstances = [];
		nodeMarker.push(el);
		el.ggId="Marker_Mp3Play";
		el.ggDx=0;
		el.ggDy=-1;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_mark ";
		el.ggType='mark';
		hs ='';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 0px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._marker_mp3play.ggIsActive=function() {
			return this.ggIsMarkerActive==true;
		}
		el.ggElementNodeId=function() {
			var hs=String(this.ggMarkerNodeId);
			if (hs.charAt(0)=='{') { // }
				return hs.substr(1, hs.length - 2);
			}
			return '';
		}
		me._marker_mp3play.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._marker_mp3play.ggNodeChange=function () {
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				me._mediapanoos.ggInitMedia("assets\/mp3\/pano\/"+player.getVariableValue('ExtValue')+"\/"+me.ggUserdata.customnodeid+".mp3");
			}
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				if (me._mediapanoos.ggApiPlayer) {
					if (me._mediapanoos.ggApiPlayerType == 'youtube') {
						let youtubeMediaFunction = function() {
							me._mediapanoos.ggApiPlayer.playVideo();
						};
						if (me._mediapanoos.ggApiPlayerReady) {
							youtubeMediaFunction();
						} else {
							let youtubeApiInterval = setInterval(function() {
								if (me._mediapanoos.ggApiPlayerReady) {
									clearInterval(youtubeApiInterval);
									youtubeMediaFunction();
								}
							}, 100);
						}
					} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
						me._mediapanoos.ggApiPlayer.play();
					}
				} else {
					player.playSound("media-panoos","1");
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c04")) || 
					((me.ggUserdata.customnodeid == "c041"))
				)
			) {
				player.setVariableValue('infoNo', Number("1"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c08")) || 
					((me.ggUserdata.customnodeid == "c081")) || 
					((me.ggUserdata.customnodeid == "c085"))
				)
			) {
				player.setVariableValue('infoNo', Number("3"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c09")) || 
					((me.ggUserdata.customnodeid == "c091"))
				)
			) {
				player.setVariableValue('infoNo', Number("5"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "b02")) || 
					((me.ggUserdata.customnodeid == "b021"))
				)
			) {
				player.setVariableValue('infoNo', Number("7"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "b05")) || 
					((me.ggUserdata.customnodeid == "b06")) || 
					((me.ggUserdata.customnodeid == "b061"))
				)
			) {
				player.setVariableValue('infoNo', Number("9"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "a03")) || 
					((me.ggUserdata.customnodeid == "a031"))
				)
			) {
				player.setVariableValue('infoNo', Number("11"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "a06")) || 
					((me.ggUserdata.customnodeid == "a07")) || 
					((me.ggUserdata.customnodeid == "a071"))
				)
			) {
				player.setVariableValue('infoNo', Number("13"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "d03")) || 
					((me.ggUserdata.customnodeid == "d031")) || 
					((me.ggUserdata.customnodeid == "d04"))
				)
			) {
				player.setVariableValue('infoNo', Number("15"));
			}
			if (
				(
					((me.ggUserdata.customnodeid == "d05")) || 
					((me.ggUserdata.customnodeid == "d06"))
				)
			) {
				player.setVariableValue('infoNo', Number("17"));
			}
			player.setVariableValue('infoPano', Number("0"));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info2Pano', Number("1"));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('infoNo')));
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility=(Number(me._text_1.style.opacity)>0||!me._text_1.style.opacity)?'inherit':'hidden';
			me._text_1.ggVisible=true;
		}
		me._cnt_helper.appendChild(me._marker_mp3play);
		el=me._mediapanoos=document.createElement('div');
		me._mediapanoos.seekbars = [];
		me._mediapanoos.ggInitMedia = function(media) {
			var notifySeekbars = function() {
				for (var i = 0; i < me._mediapanoos.seekbars.length; i++) {
					var seekbar = me.findElements(me._mediapanoos.seekbars[i]);
					if (seekbar.length > 0) seekbar[0].connectToMediaEl();
				}
			}
			while (me._mediapanoos.hasChildNodes()) {
				me._mediapanoos.removeChild(me._mediapanoos.lastChild);
			}
			if (me._mediapanoos__vid) {
				me._mediapanoos__vid.pause();
			}
			if(media == '') {
				notifySeekbars();
			if (me._mediapanoos.ggVideoNotLoaded ==false && me._mediapanoos.ggDeactivate) { me._mediapanoos.ggDeactivate(); }
				me._mediapanoos.ggVideoNotLoaded = true;
			var mediaObj = player.getMediaObject('mediapanoos');
			if (mediaObj) {
				mediaObj.autoplay = false;
			}
				return;
			}
			me._mediapanoos.ggVideoNotLoaded = false;
			me._mediapanoos__vid=document.createElement('video');
			me._mediapanoos__vid.className='ggskin ggskin_video';
			me._mediapanoos__vid.setAttribute('width', '100%');
			me._mediapanoos__vid.setAttribute('height', '100%');
			me._mediapanoos__vid.setAttribute('controlsList', 'nodownload');
			me._mediapanoos__vid.setAttribute('oncontextmenu', 'return false;');
			me._mediapanoos__source=document.createElement('source');
			me._mediapanoos__source.setAttribute('src', media);
			me._mediapanoos__vid.setAttribute('playsinline', 'playsinline');
			me._mediapanoos__vid.setAttribute('style', ';');
			me._mediapanoos__vid.style.outline = 'none';
			me._mediapanoos__vid.appendChild(me._mediapanoos__source);
			me._mediapanoos.appendChild(me._mediapanoos__vid);
			var videoEl = player.registerVideoElement('media-panoos', me._mediapanoos__vid);
			videoEl.autoplay = false;
			notifySeekbars();
			if (me._mediapanoos.ggMediaEnded) {
				me._mediapanoos__vid.addEventListener('ended', me._mediapanoos.ggMediaEnded);
			}
			me._mediapanoos.ggVideoSource = media;
		}
		el.ggId="media-panoos";
		el.ggDx=0;
		el.ggDy=29;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_video ";
		el.ggType='video';
		hs ='';
		hs+='height : 32px;';
		hs+='left : -10000px;';
		hs+='opacity : 0.001;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 32px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._mediapanoos.ggIsActive=function() {
			if (me._mediapanoos__vid != null) {
				return (me._mediapanoos__vid.paused == false && me._mediapanoos__vid.ended == false);
			} else {
				return false;
			}
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._mediapanoos.ggMediaEnded=function () {
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
		}
		me._mediapanoos.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._cnt_helper.appendChild(me._mediapanoos);
		me.divSkin.appendChild(me._cnt_helper);
		el=me._init_ga=document.createElement('div');
		els=me._init_ga__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="init_ga";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 21px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 1px;';
		hs+='visibility : hidden;';
		hs+='width : 307px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='pointer-events: none;';
		hs+='background: #000000;';
		hs+='border: 1px solid #ffffff;';
		hs+='border-radius: 5px;';
		hs+=cssPrefix + 'border-radius: 5px;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 5px 6px 5px 6px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="Please set your Google tracking ID";
var ua_id=player.getVariableValue('UA_ID');
if (!window.dataLayer) {
  window.dataLayer = [];
  let script = document.createElement('script');
  script.async=1;
  script.src = 'https://www.googletagmanager.com/gtag/js?id='+ua_id;
  document.head.appendChild(script);
}
function gtag() { window.dataLayer.push(arguments);}
window.gtag=gtag;
gtag('js', new Date());
gtag('config', ua_id);
		el.appendChild(els);
		me._init_ga.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._init_ga.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getVariableValue('UA_ID') == ""))
			)
			{
				newLogicStateVisible = 0;
			}
			else if (
				((player.getVariableValue('UA_category') == ""))
			)
			{
				newLogicStateVisible = 1;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._init_ga.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._init_ga.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._init_ga.style[domTransition]='';
				if (me._init_ga.ggCurrentLogicStateVisible == 0) {
					me._init_ga.style.visibility=(Number(me._init_ga.style.opacity)>0||!me._init_ga.style.opacity)?'inherit':'hidden';
					me._init_ga.ggVisible=true;
				}
				else if (me._init_ga.ggCurrentLogicStateVisible == 1) {
					me._init_ga.style.visibility="hidden";
					me._init_ga.ggVisible=false;
				}
				else {
					me._init_ga.style.visibility="hidden";
					me._init_ga.ggVisible=false;
				}
			}
		}
		me._init_ga.ggUpdatePosition=function (useTransition) {
		}
		me.divSkin.appendChild(me._init_ga);
		el=me._information_2=document.createElement('div');
		el.ggId="information_\u5ef6\u8b80\u8cc7\u8a0a2";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='z-index: 8;';
		hs+='height : 100%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_2.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._information_2.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width < 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._information_2.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._information_2.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._information_2.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_2.ggCurrentLogicStateScaling == 0) {
					me._information_2.ggParameter.sx = 0.65;
					me._information_2.ggParameter.sy = 0.65;
					me._information_2.style[domTransform]=parameterToTransform(me._information_2.ggParameter);
				}
				else {
					me._information_2.ggParameter.sx = 1;
					me._information_2.ggParameter.sy = 1;
					me._information_2.style[domTransform]=parameterToTransform(me._information_2.ggParameter);
				}
			}
		}
		me._information_2.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getVariableValue('vis_info_popup_2') == true))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._information_2.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._information_2.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._information_2.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_2.ggCurrentLogicStateVisible == 0) {
					me._information_2.style.visibility=(Number(me._information_2.style.opacity)>0||!me._information_2.style.opacity)?'inherit':'hidden';
					me._information_2.ggVisible=true;
				}
				else {
					me._information_2.style.visibility="hidden";
					me._information_2.ggVisible=false;
				}
			}
		}
		me._information_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._screentint_info_continue2=document.createElement('div');
		el.ggId="screentint_info_(continue)2";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : rgba(0,0,0,0.392157);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 200%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 200%;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._screentint_info_continue2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._screentint_info_continue2.onclick=function (e) {
			me._info_popup_close_continue_21.onclick.call(me._info_popup_close_continue_21);
		}
		me._screentint_info_continue2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_2.appendChild(me._screentint_info_continue2);
		el=me._information_bg_continue_2=document.createElement('div');
		el.ggId="information_bg_(continue)_2";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 20px;';
		hs+='border-radius : 20px;';
		hs+='background : rgba(30,25,25,0.666667);';
		hs+='border : 0px solid #ffffff;';
		hs+='cursor : default;';
		hs+='height : 640px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 500px;';
		hs+='pointer-events:auto;';
		hs+='box-shadow:15px 15px 15px black;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_bg_continue_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._information_bg_continue_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_2.appendChild(me._information_bg_continue_2);
		el=me._info_text_body_continue_more_2=document.createElement('div');
		els=me._info_text_body_continue_more_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="info_text_body_(continue)_more_2";
		el.ggDx=0;
		el.ggDy=20;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 547px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 450px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 450px;';
		hs+='height: 547px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		me._info_text_body_continue_more_2.ggUpdateText=function() {
			var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._info_text_body_continue_more_2.ggUpdateText();
		player.addListener('timer', function() {
			me._info_text_body_continue_more_2.ggUpdateText();
		});
		el.appendChild(els);
		me._info_text_body_continue_more_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_text_body_continue_more_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getViewerSize().width < 720))
			)
			{
				newLogicStateText = 0;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._info_text_body_continue_more_2.ggCurrentLogicStateText != newLogicStateText) {
				me._info_text_body_continue_more_2.ggCurrentLogicStateText = newLogicStateText;
				me._info_text_body_continue_more_2.style[domTransition]='';
				if (me._info_text_body_continue_more_2.ggCurrentLogicStateText == 0) {
					me._info_text_body_continue_more_2.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
					me._info_text_body_continue_more_2__text.innerHTML=me._info_text_body_continue_more_2.ggText;
					if (me._info_text_body_continue_more_2.ggUpdateText) {
					me._info_text_body_continue_more_2.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_text_body_continue_more_2.ggUpdatePosition) me._info_text_body_continue_more_2.ggUpdatePosition();
					}
				}
				else {
					me._info_text_body_continue_more_2.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
					me._info_text_body_continue_more_2__text.innerHTML=me._info_text_body_continue_more_2.ggText;
					if (me._info_text_body_continue_more_2.ggUpdateText) {
					me._info_text_body_continue_more_2.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_text_body_continue_more_2.ggUpdatePosition) me._info_text_body_continue_more_2.ggUpdatePosition();
					}
				}
			}
		}
		me._info_text_body_continue_more_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_2.appendChild(me._info_text_body_continue_more_2);
		el=me._info_popup_close_continue_21=document.createElement('div');
		els=me._info_popup_close_continue_21__img=document.createElement('img');
		els.className='ggskin ggskin_info_popup_close_continue_21';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAYAAACMo1E1AAAC30lEQVRYhcWYPW8TQRCG342QoIjEkYZ0SRcaUESNlBMVpf8EHVLcQhNLiA6J/APSUVDEXQoKG1EjW6KAVHE6UtkgGqqH4nad8eWO27uzk5GiSHuz7zye2W+nBgYkkvYkdSRt++ZdSYmkmaSxb5tI6kv67JybNYlVByoFjmlmx0C6KqhBLtgY6Ppvqc+mgMS0db2ftcHSIIH3RnjmAyY1NRLfb2a03rWBSoCRgerVhSrR7BnIUW1NYBc4M2C7baBK9APgWbS+/3UBbNw2WxVxxgawOo4p5bjSebmAoyrnMPgnq8pYCWAo8WGZU7qqMRYBaMdgWuQw8B971wlm4vd8/EH+g83atZSzAC4pzB7Q943dCJE14BWwERl0M0bX+3Y9R98SB6vMGvDG+34F1i'+
			'PAvnv/5xHac5bQ0KmzdAA7wE/f50sZYA7sB3A/Uj8sLR0BR7EljQVsCub7htIeCRheGYQtANuA+f5hcg7X6nS05pw7VXbgvJD0RNIJsCVpIOmBpFNJe865i6YxVGcylPTfIdtRAP74/6O6GTN6YVJM53BNhIzgI+Cvl/oNbLbUy5jawgEbZMsKBvAEuNNQL2Ru1nhCFICNgIemxI0AlzIhyHaIT5IeK7ttPXXOfVM2Sc4lPZN0DNxqGmNN2fVNyq52sWDrBWBTSXLOnecAP9QEDBwTu0P8/7BnwPy6Fkp5r8Rvy5T4Yywgl4fdTpO99W0VmPHdNoAvIrQX91bfWOdUcht4XQVm/LeAl4CL8F08lfjGMEOmMdlbhfmsTQtXDrOk9G4ILpyEh0UfbfZu4g5RnDXjdHjd5c2Vs/j2ZZzn98hVA7L45DGM7TBZNWAOLP6e'+
			'7MdAAFz6GMyNsUltfRafCqbAQdsses0DAzZspWkmSYDcryvoofYNVPXgryGecrkOBhv5gHv+z75shrZ9M66CDVnh82ufZtavC1W535VAJpJSXX1Nvyvpl3Kv6c65vhrYP/yQ5+qn4GJtAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="info_popup_close_(continue)_2-1";
		el.ggDx=218;
		el.ggDy=-285;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 32px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 32px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._info_popup_close_continue_21.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_popup_close_continue_21.onclick=function (e) {
			player.setVariableValue('vis_info_popup_2', false);
			me._info_text_body_continue_more_2.ggText="";
			me._info_text_body_continue_more_2.ggTextDiv.innerHTML=me._info_text_body_continue_more_2.ggText;
			if (me._info_text_body_continue_more_2.ggUpdateText) {
				me._info_text_body_continue_more_2.ggUpdateText=function() {
					var hs="";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._info_text_body_continue_more_2.ggUpdatePosition) {
				me._info_text_body_continue_more_2.ggUpdatePosition();
			}
			me._info_text_body_continue_more_2.ggTextDiv.scrollTop = 0;
			if (
				(
					((player.getViewerSize().width >= 720))
				)
			) {
				me._info_text_body_continue_more_2.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
				me._info_text_body_continue_more_2.ggTextDiv.innerHTML=me._info_text_body_continue_more_2.ggText;
				if (me._info_text_body_continue_more_2.ggUpdateText) {
					me._info_text_body_continue_more_2.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
				}
				if (me._info_text_body_continue_more_2.ggUpdatePosition) {
					me._info_text_body_continue_more_2.ggUpdatePosition();
				}
				me._info_text_body_continue_more_2.ggTextDiv.scrollTop = 0;
			}
			if (
				(
					((player.getViewerSize().width < 720))
				)
			) {
				me._info_text_body_continue_more_2.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
				me._info_text_body_continue_more_2.ggTextDiv.innerHTML=me._info_text_body_continue_more_2.ggText;
				if (me._info_text_body_continue_more_2.ggUpdateText) {
					me._info_text_body_continue_more_2.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('info2Pano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
				}
				if (me._info_text_body_continue_more_2.ggUpdatePosition) {
					me._info_text_body_continue_more_2.ggUpdatePosition();
				}
				me._info_text_body_continue_more_2.ggTextDiv.scrollTop = 0;
			}
		}
		me._info_popup_close_continue_21.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_2.appendChild(me._info_popup_close_continue_21);
		me.divSkin.appendChild(me._information_2);
		el=me._information_0=document.createElement('div');
		el.ggId="information_\u5ef6\u8b80\u8cc7\u8a0a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='z-index: 8;';
		hs+='height : 100%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_0.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._information_0.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width < 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._information_0.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._information_0.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._information_0.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_0.ggCurrentLogicStateScaling == 0) {
					me._information_0.ggParameter.sx = 0.65;
					me._information_0.ggParameter.sy = 0.65;
					me._information_0.style[domTransform]=parameterToTransform(me._information_0.ggParameter);
				}
				else {
					me._information_0.ggParameter.sx = 1;
					me._information_0.ggParameter.sy = 1;
					me._information_0.style[domTransform]=parameterToTransform(me._information_0.ggParameter);
				}
			}
		}
		me._information_0.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getVariableValue('vis_info_popup') == true))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._information_0.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._information_0.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._information_0.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_0.ggCurrentLogicStateVisible == 0) {
					me._information_0.style.visibility=(Number(me._information_0.style.opacity)>0||!me._information_0.style.opacity)?'inherit':'hidden';
					me._information_0.ggVisible=true;
				}
				else {
					me._information_0.style.visibility="hidden";
					me._information_0.ggVisible=false;
				}
			}
		}
		me._information_0.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._screentint_info_continue=document.createElement('div');
		el.ggId="screentint_info_(continue)";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : rgba(0,0,0,0.392157);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 200%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 200%;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._screentint_info_continue.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._screentint_info_continue.onclick=function (e) {
			me._info_popup_close_continue.onclick.call(me._info_popup_close_continue);
		}
		me._screentint_info_continue.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_0.appendChild(me._screentint_info_continue);
		el=me._information_bg_continue=document.createElement('div');
		el.ggId="information_bg_(continue)";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 20px;';
		hs+='border-radius : 20px;';
		hs+='background : rgba(30,25,25,0.666667);';
		hs+='border : 0px solid #ffffff;';
		hs+='cursor : default;';
		hs+='height : 640px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 500px;';
		hs+='pointer-events:auto;';
		hs+='box-shadow:15px 15px 15px black;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_bg_continue.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._information_bg_continue.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_0.appendChild(me._information_bg_continue);
		el=me._info_text_body_continue_more=document.createElement('div');
		els=me._info_text_body_continue_more__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="info_text_body_(continue)_more";
		el.ggDx=0;
		el.ggDy=20;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 547px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 450px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 450px;';
		hs+='height: 547px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		me._info_text_body_continue_more.ggUpdateText=function() {
			var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._info_text_body_continue_more.ggUpdateText();
		player.addListener('timer', function() {
			me._info_text_body_continue_more.ggUpdateText();
		});
		el.appendChild(els);
		me._info_text_body_continue_more.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_text_body_continue_more.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getViewerSize().width < 720))
			)
			{
				newLogicStateText = 0;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._info_text_body_continue_more.ggCurrentLogicStateText != newLogicStateText) {
				me._info_text_body_continue_more.ggCurrentLogicStateText = newLogicStateText;
				me._info_text_body_continue_more.style[domTransition]='';
				if (me._info_text_body_continue_more.ggCurrentLogicStateText == 0) {
					me._info_text_body_continue_more.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
					me._info_text_body_continue_more__text.innerHTML=me._info_text_body_continue_more.ggText;
					if (me._info_text_body_continue_more.ggUpdateText) {
					me._info_text_body_continue_more.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_text_body_continue_more.ggUpdatePosition) me._info_text_body_continue_more.ggUpdatePosition();
					}
				}
				else {
					me._info_text_body_continue_more.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
					me._info_text_body_continue_more__text.innerHTML=me._info_text_body_continue_more.ggText;
					if (me._info_text_body_continue_more.ggUpdateText) {
					me._info_text_body_continue_more.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_text_body_continue_more.ggUpdatePosition) me._info_text_body_continue_more.ggUpdatePosition();
					}
				}
			}
		}
		me._info_text_body_continue_more.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_0.appendChild(me._info_text_body_continue_more);
		el=me._info_popup_close_continue=document.createElement('div');
		els=me._info_popup_close_continue__img=document.createElement('img');
		els.className='ggskin ggskin_info_popup_close_continue';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAYAAACMo1E1AAAC30lEQVRYhcWYPW8TQRCG342QoIjEkYZ0SRcaUESNlBMVpf8EHVLcQhNLiA6J/APSUVDEXQoKG1EjW6KAVHE6UtkgGqqH4nad8eWO27uzk5GiSHuz7zye2W+nBgYkkvYkdSRt++ZdSYmkmaSxb5tI6kv67JybNYlVByoFjmlmx0C6KqhBLtgY6Ppvqc+mgMS0db2ftcHSIIH3RnjmAyY1NRLfb2a03rWBSoCRgerVhSrR7BnIUW1NYBc4M2C7baBK9APgWbS+/3UBbNw2WxVxxgawOo4p5bjSebmAoyrnMPgnq8pYCWAo8WGZU7qqMRYBaMdgWuQw8B971wlm4vd8/EH+g83atZSzAC4pzB7Q943dCJE14BWwERl0M0bX+3Y9R98SB6vMGvDG+34F1i'+
			'PAvnv/5xHac5bQ0KmzdAA7wE/f50sZYA7sB3A/Uj8sLR0BR7EljQVsCub7htIeCRheGYQtANuA+f5hcg7X6nS05pw7VXbgvJD0RNIJsCVpIOmBpFNJe865i6YxVGcylPTfIdtRAP74/6O6GTN6YVJM53BNhIzgI+Cvl/oNbLbUy5jawgEbZMsKBvAEuNNQL2Ru1nhCFICNgIemxI0AlzIhyHaIT5IeK7ttPXXOfVM2Sc4lPZN0DNxqGmNN2fVNyq52sWDrBWBTSXLOnecAP9QEDBwTu0P8/7BnwPy6Fkp5r8Rvy5T4Yywgl4fdTpO99W0VmPHdNoAvIrQX91bfWOdUcht4XQVm/LeAl4CL8F08lfjGMEOmMdlbhfmsTQtXDrOk9G4ILpyEh0UfbfZu4g5RnDXjdHjd5c2Vs/j2ZZzn98hVA7L45DGM7TBZNWAOLP6e'+
			'7MdAAFz6GMyNsUltfRafCqbAQdsses0DAzZspWkmSYDcryvoofYNVPXgryGecrkOBhv5gHv+z75shrZ9M66CDVnh82ufZtavC1W535VAJpJSXX1Nvyvpl3Kv6c65vhrYP/yQ5+qn4GJtAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="info_popup_close_(continue)";
		el.ggDx=218;
		el.ggDy=-285;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 32px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 32px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._info_popup_close_continue.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_popup_close_continue.onclick=function (e) {
			player.setVariableValue('vis_info_popup', false);
			if (me._mediainfoos.ggApiPlayer) {
				if (me._mediainfoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediainfoos.ggApiPlayer.pauseVideo();
						me._mediainfoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediainfoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediainfoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediainfoos.ggApiPlayerType == 'vimeo') {
					me._mediainfoos.ggApiPlayer.pause();
					me._mediainfoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-infoos");
			}
			me._img_voice_1.style[domTransition]='none';
			me._img_voice_1.style.visibility=(Number(me._img_voice_1.style.opacity)>0||!me._img_voice_1.style.opacity)?'inherit':'hidden';
			me._img_voice_1.ggVisible=true;
			me._img_mute_1.style[domTransition]='none';
			me._img_mute_1.style.visibility='hidden';
			me._img_mute_1.ggVisible=false;
			me._info_text_body_continue_more.ggText="";
			me._info_text_body_continue_more.ggTextDiv.innerHTML=me._info_text_body_continue_more.ggText;
			if (me._info_text_body_continue_more.ggUpdateText) {
				me._info_text_body_continue_more.ggUpdateText=function() {
					var hs="";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._info_text_body_continue_more.ggUpdatePosition) {
				me._info_text_body_continue_more.ggUpdatePosition();
			}
			me._info_text_body_continue_more.ggTextDiv.scrollTop = 0;
			if (
				(
					((player.getViewerSize().width >= 720))
				)
			) {
				me._info_text_body_continue_more.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
				me._info_text_body_continue_more.ggTextDiv.innerHTML=me._info_text_body_continue_more.ggText;
				if (me._info_text_body_continue_more.ggUpdateText) {
					me._info_text_body_continue_more.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
				}
				if (me._info_text_body_continue_more.ggUpdatePosition) {
					me._info_text_body_continue_more.ggUpdatePosition();
				}
				me._info_text_body_continue_more.ggTextDiv.scrollTop = 0;
			}
			if (
				(
					((player.getViewerSize().width < 720))
				)
			) {
				me._info_text_body_continue_more.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
				me._info_text_body_continue_more.ggTextDiv.innerHTML=me._info_text_body_continue_more.ggText;
				if (me._info_text_body_continue_more.ggUpdateText) {
					me._info_text_body_continue_more.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('infoPano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
				}
				if (me._info_text_body_continue_more.ggUpdatePosition) {
					me._info_text_body_continue_more.ggUpdatePosition();
				}
				me._info_text_body_continue_more.ggTextDiv.scrollTop = 0;
			}
		}
		me._info_popup_close_continue.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_0.appendChild(me._info_popup_close_continue);
		el=me._ctnosinfo=document.createElement('div');
		el.ggId="Ctn-os-info";
		el.ggDx=-210;
		el.ggDy=-285;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 25px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ctnosinfo.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._ctnosinfo.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._img_mute_1=document.createElement('div');
		els=me._img_mute_1__img=document.createElement('img');
		els.className='ggskin ggskin_img_mute_1';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAWCAYAAADTlvzyAAAA80lEQVRIib2VURXCMAxFMw7/q4RJwAGTgAQk4IA5AAfMAVLAwYoC5uDyQXYoBco6Wt5f06Q3SbNOJFKAAXbc1cTGx8Iq4ISjnLAauOIpF6zxQVmAel/HT7CkQG3hKQSbBAT23w4dC9SOLAHj2StgOSx+knewVXMHLNS2dtyb1MDe2eqArefepgauA649YJICA1CLtngWPWXj1Hvr0s0oWYUfqht0BaqcQ2N5faHaeYZWliJyEZFVURRnwIrIQfdt8J2cUKHh/kK9+/DrYKoaeI4B/izNuP0b0AFv/gpUaM3zFOYFKrTy7zUrUKGGxy9tHxt/A5PwIiQyXQetAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="img_mute_1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : hidden;';
		hs+='width : 25px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._img_mute_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._img_mute_1.onclick=function (e) {
			if (me._mediainfoos.ggApiPlayer) {
				if (me._mediainfoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediainfoos.ggApiPlayer.pauseVideo();
					};
					if (me._mediainfoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediainfoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediainfoos.ggApiPlayerType == 'vimeo') {
					me._mediainfoos.ggApiPlayer.pause();
				}
			} else {
				player.pauseSound("media-infoos");
			}
			me._img_mute_1.style[domTransition]='none';
			me._img_mute_1.style.visibility='hidden';
			me._img_mute_1.ggVisible=false;
			me._img_voice_1.style[domTransition]='none';
			me._img_voice_1.style.visibility=(Number(me._img_voice_1.style.opacity)>0||!me._img_voice_1.style.opacity)?'inherit':'hidden';
			me._img_voice_1.ggVisible=true;
			me._text_2.style[domTransition]='none';
			me._text_2.style.visibility='hidden';
			me._text_2.ggVisible=false;
		}
		me._img_mute_1.ggUpdatePosition=function (useTransition) {
		}
		me._ctnosinfo.appendChild(me._img_mute_1);
		el=me._img_voice_1=document.createElement('div');
		els=me._img_voice_1__img=document.createElement('img');
		els.className='ggskin ggskin_img_voice_1';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAABxklEQVRIib2VoXLbQBRFTzrm8ScIBQSZhLTACsrOJDvRJxiV2vsFVb9ADkyQDYKjGRHNhMggwUYGRUYFLXG/QAW+O9Y4sSUnSh552je7e+8+7b17dDG64qPjy4cjAJ1DF+RJNgaGwAyIjLOr1kDyJOsCE+BapT4QAmllTg+Igdg4O/f1Ru0SwFMFwEdvaxxpTiHAZiCavAROG/AZAwvgGLgXuf0geZJFQKFFe4nkSdbV/7kE/onUCOBo1xXOk+w7cFvD/CcwBx50gm/G2VWeZDHwQ2BBRxsGOmpXi/s1m1djXmGesr4MY4EcAyEXoyvKsizKt0Ws9VGlFqiWajzx/+QQ5i/COJuybhfAoHJCgKBNxT8qh8q/lfttgmwr/5f/+BTvahPEK3ypfKK8aA'+
			'VEyvaW86z8VfmvB1nwvpj4fYyzd/o+Uy68Tnq618WBmonLsgwr4+g13XQAZMvRVgua2Aqs9TAFUukF5FnAzDi73PmeGGfv8iT7o1bsNEiZ4qBCbsxG3DHU3C4xC1l7U23ItYca3hhni1oQAc2BgMMux9Q461u22+pfYbn9/AKce7b7orFOjLMr42wE3Kg0Y2OCe6PxSd4T/wEIRzGjINz2owAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="img_voice_1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='bottom : 0px;';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 25px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._img_voice_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._img_voice_1.onclick=function (e) {
			if (me._mediainfoos.ggApiPlayer) {
				if (me._mediainfoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediainfoos.ggApiPlayer.playVideo();
					};
					if (me._mediainfoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediainfoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediainfoos.ggApiPlayerType == 'vimeo') {
					me._mediainfoos.ggApiPlayer.play();
				}
			} else {
				player.playSound("media-infoos","1");
			}
			me._img_voice_1.style[domTransition]='none';
			me._img_voice_1.style.visibility='hidden';
			me._img_voice_1.ggVisible=false;
			me._img_mute_1.style[domTransition]='none';
			me._img_mute_1.style.visibility=(Number(me._img_mute_1.style.opacity)>0||!me._img_mute_1.style.opacity)?'inherit':'hidden';
			me._img_mute_1.ggVisible=true;
			me._text_2.style[domTransition]='none';
			me._text_2.style.visibility='hidden';
			me._text_2.ggVisible=false;
		}
		me._img_voice_1.ggUpdatePosition=function (useTransition) {
		}
		me._ctnosinfo.appendChild(me._img_voice_1);
		el=me._mediainfoos=document.createElement('div');
		me._mediainfoos.seekbars = [];
		me._mediainfoos.ggInitMedia = function(media) {
			var notifySeekbars = function() {
				for (var i = 0; i < me._mediainfoos.seekbars.length; i++) {
					var seekbar = me.findElements(me._mediainfoos.seekbars[i]);
					if (seekbar.length > 0) seekbar[0].connectToMediaEl();
				}
			}
			while (me._mediainfoos.hasChildNodes()) {
				me._mediainfoos.removeChild(me._mediainfoos.lastChild);
			}
			if (me._mediainfoos__vid) {
				me._mediainfoos__vid.pause();
			}
			if(media == '') {
				notifySeekbars();
			if (me._mediainfoos.ggVideoNotLoaded ==false && me._mediainfoos.ggDeactivate) { me._mediainfoos.ggDeactivate(); }
				me._mediainfoos.ggVideoNotLoaded = true;
			var mediaObj = player.getMediaObject('mediainfoos');
			if (mediaObj) {
				mediaObj.autoplay = false;
			}
				return;
			}
			me._mediainfoos.ggVideoNotLoaded = false;
			me._mediainfoos__vid=document.createElement('video');
			me._mediainfoos__vid.className='ggskin ggskin_video';
			me._mediainfoos__vid.setAttribute('width', '100%');
			me._mediainfoos__vid.setAttribute('height', '100%');
			me._mediainfoos__vid.setAttribute('controlsList', 'nodownload');
			me._mediainfoos__vid.setAttribute('oncontextmenu', 'return false;');
			me._mediainfoos__source=document.createElement('source');
			me._mediainfoos__source.setAttribute('src', media);
			me._mediainfoos__vid.setAttribute('playsinline', 'playsinline');
			me._mediainfoos__vid.setAttribute('style', ';');
			me._mediainfoos__vid.style.outline = 'none';
			me._mediainfoos__vid.appendChild(me._mediainfoos__source);
			me._mediainfoos.appendChild(me._mediainfoos__vid);
			var videoEl = player.registerVideoElement('media-infoos', me._mediainfoos__vid);
			videoEl.autoplay = false;
			notifySeekbars();
			me._mediainfoos.ggVideoSource = media;
		}
		el.ggId="media-infoos";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_video ";
		el.ggType='video';
		hs ='';
		hs+='height : 32px;';
		hs+='left : 201px;';
		hs+='opacity : 0.001;';
		hs+='position : absolute;';
		hs+='top : -2px;';
		hs+='visibility : inherit;';
		hs+='width : 32px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._mediainfoos.ggIsActive=function() {
			if (me._mediainfoos__vid != null) {
				return (me._mediainfoos__vid.paused == false && me._mediainfoos__vid.ended == false);
			} else {
				return false;
			}
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._mediainfoos.ggUpdatePosition=function (useTransition) {
		}
		me._ctnosinfo.appendChild(me._mediainfoos);
		el=me._text_2=document.createElement('div');
		els=me._text_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 2";
		el.ggDy=2;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 38px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 150px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 150px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="Click me!";
		el.appendChild(els);
		me._text_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_2.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._text_2.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._text_2.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._text_2.style[domTransition]='opacity 0s';
				if (me._text_2.ggCurrentLogicStateAlpha == 0) {
					me._text_2.style.visibility=me._text_2.ggVisible?'inherit':'hidden';
					me._text_2.style.opacity=1;
				}
				else {
					me._text_2.style.visibility="hidden";
					me._text_2.style.opacity=0;
				}
			}
		}
		me._text_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_2.ggCurrentLogicStateText != newLogicStateText) {
				me._text_2.ggCurrentLogicStateText = newLogicStateText;
				me._text_2.style[domTransition]='opacity 0s';
				if (me._text_2.ggCurrentLogicStateText == 0) {
					me._text_2.ggText="Cliquez";
					me._text_2__text.innerHTML=me._text_2.ggText;
					if (me._text_2.ggUpdateText) {
					me._text_2.ggUpdateText=function() {
						var hs="Cliquez";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_2.ggUpdatePosition) me._text_2.ggUpdatePosition();
					}
				}
				else if (me._text_2.ggCurrentLogicStateText == 1) {
					me._text_2.ggText="Hier klicken";
					me._text_2__text.innerHTML=me._text_2.ggText;
					if (me._text_2.ggUpdateText) {
					me._text_2.ggUpdateText=function() {
						var hs="Hier klicken";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_2.ggUpdatePosition) me._text_2.ggUpdatePosition();
					}
				}
				else if (me._text_2.ggCurrentLogicStateText == 2) {
					me._text_2.ggText="Cliccami";
					me._text_2__text.innerHTML=me._text_2.ggText;
					if (me._text_2.ggUpdateText) {
					me._text_2.ggUpdateText=function() {
						var hs="Cliccami";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_2.ggUpdatePosition) me._text_2.ggUpdatePosition();
					}
				}
				else if (me._text_2.ggCurrentLogicStateText == 3) {
					me._text_2.ggText="Haz clic";
					me._text_2__text.innerHTML=me._text_2.ggText;
					if (me._text_2.ggUpdateText) {
					me._text_2.ggUpdateText=function() {
						var hs="Haz clic";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_2.ggUpdatePosition) me._text_2.ggUpdatePosition();
					}
				}
				else if (me._text_2.ggCurrentLogicStateText == 4) {
					me._text_2.ggText="\u53d6\u6d88\u975c\u97f3";
					me._text_2__text.innerHTML=me._text_2.ggText;
					if (me._text_2.ggUpdateText) {
					me._text_2.ggUpdateText=function() {
						var hs="\u53d6\u6d88\u975c\u97f3";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_2.ggUpdatePosition) me._text_2.ggUpdatePosition();
					}
				}
				else {
					me._text_2.ggText="Click me!";
					me._text_2__text.innerHTML=me._text_2.ggText;
					if (me._text_2.ggUpdateText) {
					me._text_2.ggUpdateText=function() {
						var hs="Click me!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_2.ggUpdatePosition) me._text_2.ggUpdatePosition();
					}
				}
			}
		}
		me._text_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._ctnosinfo.appendChild(me._text_2);
		me._information_0.appendChild(me._ctnosinfo);
		me.divSkin.appendChild(me._information_0);
		el=me._information_vr=document.createElement('div');
		el.ggId="information_(VR)";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='z-index: 8;';
		hs+='height : 100%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_vr.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._information_vr.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._information_vr.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._information_vr.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._information_vr.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_vr.ggCurrentLogicStateScaling == 0) {
					me._information_vr.ggParameter.sx = 1;
					me._information_vr.ggParameter.sy = 1;
					me._information_vr.style[domTransform]=parameterToTransform(me._information_vr.ggParameter);
				}
				else if (me._information_vr.ggCurrentLogicStateScaling == 1) {
					me._information_vr.ggParameter.sx = 0.65;
					me._information_vr.ggParameter.sy = 0.65;
					me._information_vr.style[domTransform]=parameterToTransform(me._information_vr.ggParameter);
				}
				else {
					me._information_vr.ggParameter.sx = 1;
					me._information_vr.ggParameter.sy = 1;
					me._information_vr.style[domTransform]=parameterToTransform(me._information_vr.ggParameter);
				}
			}
		}
		me._information_vr.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getVariableValue('vis_info_vr_popup') == true))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._information_vr.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._information_vr.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._information_vr.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_vr.ggCurrentLogicStateVisible == 0) {
					me._information_vr.style.visibility=(Number(me._information_vr.style.opacity)>0||!me._information_vr.style.opacity)?'inherit':'hidden';
					me._information_vr.ggVisible=true;
				}
				else {
					me._information_vr.style.visibility="hidden";
					me._information_vr.ggVisible=false;
				}
			}
		}
		me._information_vr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._screentint_info_vr=document.createElement('div');
		el.ggId="screentint_info_(VR)";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : rgba(0,0,0,0.392157);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 200%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 200%;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._screentint_info_vr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._screentint_info_vr.onclick=function (e) {
			player.setVariableValue('vis_info_vr_popup', false);
		}
		me._screentint_info_vr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_vr.appendChild(me._screentint_info_vr);
		el=me._information_bg_vr=document.createElement('div');
		el.ggId="information_bg_(VR)";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 8px;';
		hs+='border-radius : 8px;';
		hs+='background : rgba(30,25,25,0.666667);';
		hs+='border : 0px solid #ffffff;';
		hs+='cursor : default;';
		hs+='height : 530px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 540px;';
		hs+='pointer-events:auto;';
		hs+='box-shadow:15px 15px 15px black;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_bg_vr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._information_bg_vr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_vr.appendChild(me._information_bg_vr);
		el=me._image_qr=document.createElement('div');
		els=me._image_qr__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._image_qr.ggUpdatePosition();}
		el.ggText=basePath + "";
		els.setAttribute('src', el.ggText);
		els['ondragstart']=function() { return false; };
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Image_QR";
		el.ggDx=-200;
		el.ggDy=205;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='background : #ffffff;';
		hs+='border : 1px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 80px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._image_qr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._image_qr.logicBlock_externalurl = function() {
			var newLogicStateExternalUrl;
			if (
				((me.ggUserdata.customnodeid == "c08")) || 
				((me.ggUserdata.customnodeid == "c081"))
			)
			{
				newLogicStateExternalUrl = 0;
			}
			else if (
				((me.ggUserdata.customnodeid == "c04")) || 
				((me.ggUserdata.customnodeid == "c041"))
			)
			{
				newLogicStateExternalUrl = 1;
			}
			else if (
				((me.ggUserdata.customnodeid == "c09")) || 
				((me.ggUserdata.customnodeid == "c091"))
			)
			{
				newLogicStateExternalUrl = 2;
			}
			else if (
				((me.ggUserdata.customnodeid == "b02")) || 
				((me.ggUserdata.customnodeid == "b021"))
			)
			{
				newLogicStateExternalUrl = 3;
			}
			else if (
				((me.ggUserdata.customnodeid == "b05")) || 
				((me.ggUserdata.customnodeid == "b06")) || 
				((me.ggUserdata.customnodeid == "b061"))
			)
			{
				newLogicStateExternalUrl = 4;
			}
			else if (
				((me.ggUserdata.customnodeid == "a03")) || 
				((me.ggUserdata.customnodeid == "a031"))
			)
			{
				newLogicStateExternalUrl = 5;
			}
			else if (
				((me.ggUserdata.customnodeid == "a06")) || 
				((me.ggUserdata.customnodeid == "a07")) || 
				((me.ggUserdata.customnodeid == "a071"))
			)
			{
				newLogicStateExternalUrl = 6;
			}
			else if (
				((me.ggUserdata.customnodeid == "d03")) || 
				((me.ggUserdata.customnodeid == "d031")) || 
				((me.ggUserdata.customnodeid == "d04"))
			)
			{
				newLogicStateExternalUrl = 7;
			}
			else if (
				((me.ggUserdata.customnodeid == "d05")) || 
				((me.ggUserdata.customnodeid == "d06"))
			)
			{
				newLogicStateExternalUrl = 8;
			}
			else {
				newLogicStateExternalUrl = -1;
			}
			if (me._image_qr.ggCurrentLogicStateExternalUrl != newLogicStateExternalUrl) {
				me._image_qr.ggCurrentLogicStateExternalUrl = newLogicStateExternalUrl;
				me._image_qr.style[domTransition]='';
				if (me._image_qr.ggCurrentLogicStateExternalUrl == 0) {
					me._image_qr.ggText=basePath + "assets/qrcode/3.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 1) {
					me._image_qr.ggText=basePath + "assets/qrcode/1.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 2) {
					me._image_qr.ggText=basePath + "assets/qrcode/2.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 3) {
					me._image_qr.ggText=basePath + "assets/qrcode/4.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 4) {
					me._image_qr.ggText=basePath + "assets/qrcode/5.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 5) {
					me._image_qr.ggText=basePath + "assets/qrcode/6.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 6) {
					me._image_qr.ggText=basePath + "assets/qrcode/7.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 7) {
					me._image_qr.ggText=basePath + "assets/qrcode/9.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else if (me._image_qr.ggCurrentLogicStateExternalUrl == 8) {
					me._image_qr.ggText=basePath + "assets/qrcode/8.jpg";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
				else {
					me._image_qr.ggText=basePath + "";
					me._image_qr__img.style.width = '0px';
					me._image_qr__img.style.height = '0px';
					me._image_qr__img.src=me._image_qr.ggText;
				}
			}
		}
		me._image_qr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._image_qr.clientWidth;
			var parentHeight = me._image_qr.clientHeight;
			var img = me._image_qr__img;
			var aspectRatioDiv = me._image_qr.clientWidth / me._image_qr.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			if (img.naturalWidth < parentWidth) parentWidth = img.naturalWidth;
			if (img.naturalHeight < parentHeight) parentHeight = img.naturalHeight;
			var currentWidth,currentHeight;
			currentWidth = parentWidth;
			currentHeight = parentHeight;
			img.style.width=parentWidth + 'px';
			img.style.height=parentHeight + 'px';
			img.style.left='0px';
			img.style.top='0px';
		}
		me._information_vr.appendChild(me._image_qr);
		el=me._text_qr=document.createElement('div');
		els=me._text_qr__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text_QR";
		el.ggDx=47;
		el.ggDy=199;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text info";
		el.ggType='text';
		hs ='';
		hs+='height : 71px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 387px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 387px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 14px;font-family: Arial;";
		els.setAttribute('style',hs);
		els.innerHTML="Scan the QR code with your smartphone, place the device in your environment. You are ready to play with it!<br\/>";
		el.appendChild(els);
		me._text_qr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_qr.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 0))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 4;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 5;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_qr.ggCurrentLogicStateText != newLogicStateText) {
				me._text_qr.ggCurrentLogicStateText = newLogicStateText;
				me._text_qr.style[domTransition]='';
				if (me._text_qr.ggCurrentLogicStateText == 0) {
					me._text_qr.ggText="Scan the QR code with your smartphone, place the device in your environment. You are ready to play with it!";
					me._text_qr__text.innerHTML=me._text_qr.ggText;
					if (me._text_qr.ggUpdateText) {
					me._text_qr.ggUpdateText=function() {
						var hs="Scan the QR code with your smartphone, place the device in your environment. You are ready to play with it!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_qr.ggUpdatePosition) me._text_qr.ggUpdatePosition();
					}
				}
				else if (me._text_qr.ggCurrentLogicStateText == 1) {
					me._text_qr.ggText="Scannez le QR code avec votre smartphone, placez l\'appareil dans votre environnement. Vous \xeates pr\xeat \xe0 jouer avec !";
					me._text_qr__text.innerHTML=me._text_qr.ggText;
					if (me._text_qr.ggUpdateText) {
					me._text_qr.ggUpdateText=function() {
						var hs="Scannez le QR code avec votre smartphone, placez l\'appareil dans votre environnement. Vous \xeates pr\xeat \xe0 jouer avec !";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_qr.ggUpdatePosition) me._text_qr.ggUpdatePosition();
					}
				}
				else if (me._text_qr.ggCurrentLogicStateText == 2) {
					me._text_qr.ggText="Scannen Sie den QR-Code mit dem Smartphone, platzieren Sie das Ger\xe4t in Ihrer Umgebung. Schon k\xf6nnen Sie damit spielen!";
					me._text_qr__text.innerHTML=me._text_qr.ggText;
					if (me._text_qr.ggUpdateText) {
					me._text_qr.ggUpdateText=function() {
						var hs="Scannen Sie den QR-Code mit dem Smartphone, platzieren Sie das Ger\xe4t in Ihrer Umgebung. Schon k\xf6nnen Sie damit spielen!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_qr.ggUpdatePosition) me._text_qr.ggUpdatePosition();
					}
				}
				else if (me._text_qr.ggCurrentLogicStateText == 3) {
					me._text_qr.ggText="Scannerizza il codice QR con il tuo smartphone, posiziona il dispositivo nel tuo ambiente. Ora sei pronto!";
					me._text_qr__text.innerHTML=me._text_qr.ggText;
					if (me._text_qr.ggUpdateText) {
					me._text_qr.ggUpdateText=function() {
						var hs="Scannerizza il codice QR con il tuo smartphone, posiziona il dispositivo nel tuo ambiente. Ora sei pronto!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_qr.ggUpdatePosition) me._text_qr.ggUpdatePosition();
					}
				}
				else if (me._text_qr.ggCurrentLogicStateText == 4) {
					me._text_qr.ggText="Escanea el c\xf3digo QR con tu smartphone, coloca el dispositivo en tu entorno. \xa1Est\xe1s listo para jugar con \xe9l!";
					me._text_qr__text.innerHTML=me._text_qr.ggText;
					if (me._text_qr.ggUpdateText) {
					me._text_qr.ggUpdateText=function() {
						var hs="Escanea el c\xf3digo QR con tu smartphone, coloca el dispositivo en tu entorno. \xa1Est\xe1s listo para jugar con \xe9l!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_qr.ggUpdatePosition) me._text_qr.ggUpdatePosition();
					}
				}
				else if (me._text_qr.ggCurrentLogicStateText == 5) {
					me._text_qr.ggText="\u7528\u4f60\u7684\u667a\u80fd\u624b\u673a\u626b\u63cfQR\u7801\uff0c\u628a\u8bbe\u5907\u653e\u5728\u4f60\u7684\u73af\u5883\u4e2d\u3002\u4f60\u5df2\u7ecf\u51c6\u5907\u597d\u548c\u5b83\u4e00\u8d77\u73a9\u4e86!";
					me._text_qr__text.innerHTML=me._text_qr.ggText;
					if (me._text_qr.ggUpdateText) {
					me._text_qr.ggUpdateText=function() {
						var hs="\u7528\u4f60\u7684\u667a\u80fd\u624b\u673a\u626b\u63cfQR\u7801\uff0c\u628a\u8bbe\u5907\u653e\u5728\u4f60\u7684\u73af\u5883\u4e2d\u3002\u4f60\u5df2\u7ecf\u51c6\u5907\u597d\u548c\u5b83\u4e00\u8d77\u73a9\u4e86!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_qr.ggUpdatePosition) me._text_qr.ggUpdatePosition();
					}
				}
				else {
					me._text_qr.ggText="Scan the QR code with your smartphone, place the device in your environment. You are ready to play with it!\n";
					me._text_qr__text.innerHTML=me._text_qr.ggText;
					if (me._text_qr.ggUpdateText) {
					me._text_qr.ggUpdateText=function() {
						var hs="Scan the QR code with your smartphone, place the device in your environment. You are ready to play with it!\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_qr.ggUpdatePosition) me._text_qr.ggUpdatePosition();
					}
				}
			}
		}
		me._text_qr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_vr.appendChild(me._text_qr);
		el=me._info_text_body_vr=document.createElement('div');
		els=me._info_text_body_vr__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="info_text_body_(VR)";
		el.ggDx=0;
		el.ggDy=16;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 263px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 480px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 480px;';
		hs+='height: 263px;';
		hs+='border: 0px solid #6dff2a;';
		hs+='color: rgba(115,248,255,1);';
		hs+='font-size: 20px;';
		hs+='font-weight: inherit;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740<br\/>\u7db2     \u5740";
		el.appendChild(els);
		me._info_text_body_vr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_text_body_vr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_vr.appendChild(me._info_text_body_vr);
		el=me._info_title2_vr=document.createElement('div');
		els=me._info_title2_vr__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="info_title2_(VR)";
		el.ggDx=0;
		el.ggDy=-160;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text info";
		el.ggType='text';
		hs ='';
		hs+='height : 71px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 480px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 480px;';
		hs+='height: auto;';
		hs+='border: 0px solid #33c3ba;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 14px;font-family: Arial;";
		els.setAttribute('style',hs);
		els.innerHTML="Explore Getac Devices Now \u2014 With Augmented Reality Experience Getac technology in your world. Get to know every angle of our devices in your everyday environment. Click on the product to get started in 3D!";
		el.appendChild(els);
		me._info_title2_vr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_title2_vr.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 0))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 4;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 5;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._info_title2_vr.ggCurrentLogicStateText != newLogicStateText) {
				me._info_title2_vr.ggCurrentLogicStateText = newLogicStateText;
				me._info_title2_vr.style[domTransition]='';
				if (me._info_title2_vr.ggCurrentLogicStateText == 0) {
					me._info_title2_vr.ggText="Explore Getac Devices Now \u2014 With Augmented Reality | Experience Getac technology in your world. Get to know every angle of our devices in your everyday environment. Click on the product to get started in 3D!";
					me._info_title2_vr__text.innerHTML=me._info_title2_vr.ggText;
					if (me._info_title2_vr.ggUpdateText) {
					me._info_title2_vr.ggUpdateText=function() {
						var hs="Explore Getac Devices Now \u2014 With Augmented Reality | Experience Getac technology in your world. Get to know every angle of our devices in your everyday environment. Click on the product to get started in 3D!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title2_vr.ggUpdatePosition) me._info_title2_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title2_vr.ggCurrentLogicStateText == 1) {
					me._info_title2_vr.ggText="D\xe9couvrez les appareils Getac maintenant - avec la r\xe9alit\xe9 augment\xe9e | D\xe9couvrez la technologie Getac dans votre secteur. Apprenez \xe0 conna\xeetre tous les aspects de nos appareils dans votre environnement quotidien. Cliquez sur le produit pour commencer en 3D !";
					me._info_title2_vr__text.innerHTML=me._info_title2_vr.ggText;
					if (me._info_title2_vr.ggUpdateText) {
					me._info_title2_vr.ggUpdateText=function() {
						var hs="D\xe9couvrez les appareils Getac maintenant - avec la r\xe9alit\xe9 augment\xe9e | D\xe9couvrez la technologie Getac dans votre secteur. Apprenez \xe0 conna\xeetre tous les aspects de nos appareils dans votre environnement quotidien. Cliquez sur le produit pour commencer en 3D !";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title2_vr.ggUpdatePosition) me._info_title2_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title2_vr.ggCurrentLogicStateText == 2) {
					me._info_title2_vr.ggText="Entdecken Sie Getac-Ger\xe4te jetzt - mit Augmented Reality | Erleben Sie Getac-Technologie in Ihrer Welt. Lernen Sie unsere Ger\xe4te in Ihrer t\xe4glichen Umgebung von allen Seiten kennen. Klicken Sie auf das Produkt, um es in 3D zu erleben!";
					me._info_title2_vr__text.innerHTML=me._info_title2_vr.ggText;
					if (me._info_title2_vr.ggUpdateText) {
					me._info_title2_vr.ggUpdateText=function() {
						var hs="Entdecken Sie Getac-Ger\xe4te jetzt - mit Augmented Reality | Erleben Sie Getac-Technologie in Ihrer Welt. Lernen Sie unsere Ger\xe4te in Ihrer t\xe4glichen Umgebung von allen Seiten kennen. Klicken Sie auf das Produkt, um es in 3D zu erleben!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title2_vr.ggUpdatePosition) me._info_title2_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title2_vr.ggCurrentLogicStateText == 3) {
					me._info_title2_vr.ggText="Esplora ora i dispositivi Getac - con la realt\xe0 aumentata | Prova la tecnologia Getac nel tuo mondo. Scopri ogni angolo dei nostri dispositivi nel tuo ambiente quotidiano. Clicca sul prodotto per iniziare l\'esperienza in 3D!";
					me._info_title2_vr__text.innerHTML=me._info_title2_vr.ggText;
					if (me._info_title2_vr.ggUpdateText) {
					me._info_title2_vr.ggUpdateText=function() {
						var hs="Esplora ora i dispositivi Getac - con la realt\xe0 aumentata | Prova la tecnologia Getac nel tuo mondo. Scopri ogni angolo dei nostri dispositivi nel tuo ambiente quotidiano. Clicca sul prodotto per iniziare l\'esperienza in 3D!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title2_vr.ggUpdatePosition) me._info_title2_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title2_vr.ggCurrentLogicStateText == 4) {
					me._info_title2_vr.ggText="Explore los dispositivos Getac ahora - Con tecnologi\xeda de realidad aumentada | Experimente la tecnolog\xeda Getac en su mundo. Conozca todos los \xe1ngulos de nuestros dispositivos en su entorno cotidiano. Haga clic en el producto para visualizar en 3D.";
					me._info_title2_vr__text.innerHTML=me._info_title2_vr.ggText;
					if (me._info_title2_vr.ggUpdateText) {
					me._info_title2_vr.ggUpdateText=function() {
						var hs="Explore los dispositivos Getac ahora - Con tecnologi\xeda de realidad aumentada | Experimente la tecnolog\xeda Getac en su mundo. Conozca todos los \xe1ngulos de nuestros dispositivos en su entorno cotidiano. Haga clic en el producto para visualizar en 3D.";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title2_vr.ggUpdatePosition) me._info_title2_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title2_vr.ggCurrentLogicStateText == 5) {
					me._info_title2_vr.ggText="\u7acb\u5373\u63a2\u7d22Getac\u8bbe\u5907 - \u4f7f\u7528\u865a\u62df\u73b0\u5b9e\u6280\u672f|\u5728\u60a8\u7684\u4e16\u754c\u4e2d\u4f53\u9a8cGetac\u6280\u672f\u3002\u5728\u60a8\u7684\u65e5\u5e38\u73af\u5883\u4e2d\u4e86\u89e3\u6211\u4eec\u8bbe\u5907\u7684\u6bcf\u4e2a\u89d2\u5ea6\u3002\u70b9\u51fb\u4ea7\u54c1\uff0c\u4ee53D\u65b9\u5f0f\u5f00\u59cb!";
					me._info_title2_vr__text.innerHTML=me._info_title2_vr.ggText;
					if (me._info_title2_vr.ggUpdateText) {
					me._info_title2_vr.ggUpdateText=function() {
						var hs="\u7acb\u5373\u63a2\u7d22Getac\u8bbe\u5907 - \u4f7f\u7528\u865a\u62df\u73b0\u5b9e\u6280\u672f|\u5728\u60a8\u7684\u4e16\u754c\u4e2d\u4f53\u9a8cGetac\u6280\u672f\u3002\u5728\u60a8\u7684\u65e5\u5e38\u73af\u5883\u4e2d\u4e86\u89e3\u6211\u4eec\u8bbe\u5907\u7684\u6bcf\u4e2a\u89d2\u5ea6\u3002\u70b9\u51fb\u4ea7\u54c1\uff0c\u4ee53D\u65b9\u5f0f\u5f00\u59cb!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title2_vr.ggUpdatePosition) me._info_title2_vr.ggUpdatePosition();
					}
				}
				else {
					me._info_title2_vr.ggText="Explore Getac Devices Now \u2014 With Augmented Reality Experience Getac technology in your world. Get to know every angle of our devices in your everyday environment. Click on the product to get started in 3D!";
					me._info_title2_vr__text.innerHTML=me._info_title2_vr.ggText;
					if (me._info_title2_vr.ggUpdateText) {
					me._info_title2_vr.ggUpdateText=function() {
						var hs="Explore Getac Devices Now \u2014 With Augmented Reality Experience Getac technology in your world. Get to know every angle of our devices in your everyday environment. Click on the product to get started in 3D!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title2_vr.ggUpdatePosition) me._info_title2_vr.ggUpdatePosition();
					}
				}
			}
		}
		me._info_title2_vr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_vr.appendChild(me._info_title2_vr);
		el=me._info_popup_close_vr=document.createElement('div');
		els=me._info_popup_close_vr__img=document.createElement('img');
		els.className='ggskin ggskin_info_popup_close_vr';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAYAAACMo1E1AAAC30lEQVRYhcWYPW8TQRCG342QoIjEkYZ0SRcaUESNlBMVpf8EHVLcQhNLiA6J/APSUVDEXQoKG1EjW6KAVHE6UtkgGqqH4nad8eWO27uzk5GiSHuz7zye2W+nBgYkkvYkdSRt++ZdSYmkmaSxb5tI6kv67JybNYlVByoFjmlmx0C6KqhBLtgY6Ppvqc+mgMS0db2ftcHSIIH3RnjmAyY1NRLfb2a03rWBSoCRgerVhSrR7BnIUW1NYBc4M2C7baBK9APgWbS+/3UBbNw2WxVxxgawOo4p5bjSebmAoyrnMPgnq8pYCWAo8WGZU7qqMRYBaMdgWuQw8B971wlm4vd8/EH+g83atZSzAC4pzB7Q943dCJE14BWwERl0M0bX+3Y9R98SB6vMGvDG+34F1i'+
			'PAvnv/5xHac5bQ0KmzdAA7wE/f50sZYA7sB3A/Uj8sLR0BR7EljQVsCub7htIeCRheGYQtANuA+f5hcg7X6nS05pw7VXbgvJD0RNIJsCVpIOmBpFNJe865i6YxVGcylPTfIdtRAP74/6O6GTN6YVJM53BNhIzgI+Cvl/oNbLbUy5jawgEbZMsKBvAEuNNQL2Ru1nhCFICNgIemxI0AlzIhyHaIT5IeK7ttPXXOfVM2Sc4lPZN0DNxqGmNN2fVNyq52sWDrBWBTSXLOnecAP9QEDBwTu0P8/7BnwPy6Fkp5r8Rvy5T4Yywgl4fdTpO99W0VmPHdNoAvIrQX91bfWOdUcht4XQVm/LeAl4CL8F08lfjGMEOmMdlbhfmsTQtXDrOk9G4ILpyEh0UfbfZu4g5RnDXjdHjd5c2Vs/j2ZZzn98hVA7L45DGM7TBZNWAOLP6e'+
			'7MdAAFz6GMyNsUltfRafCqbAQdsses0DAzZspWkmSYDcryvoofYNVPXgryGecrkOBhv5gHv+z75shrZ9M66CDVnh82ufZtavC1W535VAJpJSXX1Nvyvpl3Kv6c65vhrYP/yQ5+qn4GJtAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="info_popup_close_(VR)";
		el.ggDx=240;
		el.ggDy=-235;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 32px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 32px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._info_popup_close_vr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_popup_close_vr.onclick=function (e) {
			player.setVariableValue('vis_info_vr_popup', false);
		}
		me._info_popup_close_vr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_vr.appendChild(me._info_popup_close_vr);
		el=me._info_title_vr=document.createElement('div');
		els=me._info_title_vr__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="info_title_(VR)";
		el.ggDx=0;
		el.ggDy=-222;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text info";
		el.ggType='text';
		hs ='';
		hs+='height : 34px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 480px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 480px;';
		hs+='height: auto;';
		hs+='border: 0px solid #33c3ba;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 16px;font-family: Arial;font-weight:bold;";
		els.setAttribute('style',hs);
		els.innerHTML="\u6a19\u984c | \u6a19\u984c |\u6a19\u984c | \u6a19\u984c<br\/>";
		el.appendChild(els);
		me._info_title_vr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_title_vr.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 0))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 4;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 5;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._info_title_vr.ggCurrentLogicStateText != newLogicStateText) {
				me._info_title_vr.ggCurrentLogicStateText = newLogicStateText;
				me._info_title_vr.style[domTransition]='';
				if (me._info_title_vr.ggCurrentLogicStateText == 0) {
					me._info_title_vr.ggText="Experience Getac Devices with Augmented Reality\n";
					me._info_title_vr__text.innerHTML=me._info_title_vr.ggText;
					if (me._info_title_vr.ggUpdateText) {
					me._info_title_vr.ggUpdateText=function() {
						var hs="Experience Getac Devices with Augmented Reality\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title_vr.ggUpdatePosition) me._info_title_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title_vr.ggCurrentLogicStateText == 1) {
					me._info_title_vr.ggText="D\xe9couvrez les appareils Getac gr\xe2ce \xe0 la r\xe9alit\xe9 augment\xe9e\n";
					me._info_title_vr__text.innerHTML=me._info_title_vr.ggText;
					if (me._info_title_vr.ggUpdateText) {
					me._info_title_vr.ggUpdateText=function() {
						var hs="D\xe9couvrez les appareils Getac gr\xe2ce \xe0 la r\xe9alit\xe9 augment\xe9e\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title_vr.ggUpdatePosition) me._info_title_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title_vr.ggCurrentLogicStateText == 2) {
					me._info_title_vr.ggText="Erleben Sie Getac-Ger\xe4te in Augmented Reality\n";
					me._info_title_vr__text.innerHTML=me._info_title_vr.ggText;
					if (me._info_title_vr.ggUpdateText) {
					me._info_title_vr.ggUpdateText=function() {
						var hs="Erleben Sie Getac-Ger\xe4te in Augmented Reality\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title_vr.ggUpdatePosition) me._info_title_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title_vr.ggCurrentLogicStateText == 3) {
					me._info_title_vr.ggText="Prova i dispositivi Getac con la realt\xe0 aumentata\n";
					me._info_title_vr__text.innerHTML=me._info_title_vr.ggText;
					if (me._info_title_vr.ggUpdateText) {
					me._info_title_vr.ggUpdateText=function() {
						var hs="Prova i dispositivi Getac con la realt\xe0 aumentata\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title_vr.ggUpdatePosition) me._info_title_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title_vr.ggCurrentLogicStateText == 4) {
					me._info_title_vr.ggText="Experimente los dispositivos de Getac con la realidad aumentada\n";
					me._info_title_vr__text.innerHTML=me._info_title_vr.ggText;
					if (me._info_title_vr.ggUpdateText) {
					me._info_title_vr.ggUpdateText=function() {
						var hs="Experimente los dispositivos de Getac con la realidad aumentada\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title_vr.ggUpdatePosition) me._info_title_vr.ggUpdatePosition();
					}
				}
				else if (me._info_title_vr.ggCurrentLogicStateText == 5) {
					me._info_title_vr.ggText="\u5229\u7528\u865a\u62df\u73b0\u5b9e\u6280\u672f\u4f53\u9a8cGetac\u8bbe\u5907\n";
					me._info_title_vr__text.innerHTML=me._info_title_vr.ggText;
					if (me._info_title_vr.ggUpdateText) {
					me._info_title_vr.ggUpdateText=function() {
						var hs="\u5229\u7528\u865a\u62df\u73b0\u5b9e\u6280\u672f\u4f53\u9a8cGetac\u8bbe\u5907\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title_vr.ggUpdatePosition) me._info_title_vr.ggUpdatePosition();
					}
				}
				else {
					me._info_title_vr.ggText="\u6a19\u984c | \u6a19\u984c |\u6a19\u984c | \u6a19\u984c\n";
					me._info_title_vr__text.innerHTML=me._info_title_vr.ggText;
					if (me._info_title_vr.ggUpdateText) {
					me._info_title_vr.ggUpdateText=function() {
						var hs="\u6a19\u984c | \u6a19\u984c |\u6a19\u984c | \u6a19\u984c\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_title_vr.ggUpdatePosition) me._info_title_vr.ggUpdatePosition();
					}
				}
			}
		}
		me._info_title_vr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_vr.appendChild(me._info_title_vr);
		me.divSkin.appendChild(me._information_vr);
		el=me._button_silhouette_next_previous=document.createElement('div');
		el.ggId="button_silhouette_next_previous";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1.5,sy:1.5 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='z-index: 0;';
		hs+='bottom : 65px;';
		hs+='height : 70px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._button_silhouette_next_previous.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._button_silhouette_next_previous.logicBlock_position = function() {
			var newLogicStatePosition;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._button_silhouette_next_previous.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._button_silhouette_next_previous.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._button_silhouette_next_previous.style[domTransition]='left 0s, bottom 0s, ' + cssPrefix + 'transform 0s';
				if (me._button_silhouette_next_previous.ggCurrentLogicStatePosition == 0) {
					this.ggDx = 0;
					me._button_silhouette_next_previous.style.bottom='70px';
					me._button_silhouette_next_previous.ggUpdatePosition(true);
				}
				else {
					me._button_silhouette_next_previous.ggDx=0;
					me._button_silhouette_next_previous.style.bottom='65px';
					me._button_silhouette_next_previous.ggUpdatePosition(true);
				}
			}
		}
		me._button_silhouette_next_previous.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStateScaling = 0;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._button_silhouette_next_previous.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._button_silhouette_next_previous.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._button_silhouette_next_previous.style[domTransition]='left 0s, bottom 0s, ' + cssPrefix + 'transform 0s';
				if (me._button_silhouette_next_previous.ggCurrentLogicStateScaling == 0) {
					me._button_silhouette_next_previous.ggParameter.sx = 1.2;
					me._button_silhouette_next_previous.ggParameter.sy = 1.2;
					me._button_silhouette_next_previous.style[domTransform]=parameterToTransform(me._button_silhouette_next_previous.ggParameter);
				}
				else {
					me._button_silhouette_next_previous.ggParameter.sx = 1.5;
					me._button_silhouette_next_previous.ggParameter.sy = 1.5;
					me._button_silhouette_next_previous.style[domTransform]=parameterToTransform(me._button_silhouette_next_previous.ggParameter);
				}
			}
		}
		me._button_silhouette_next_previous.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		el=me._pano_prev_b=document.createElement('div');
		els=me._pano_prev_b__img=document.createElement('img');
		els.className='ggskin ggskin_pano_prev_b';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKgAAABQCAYAAACNij7AAAAMQElEQVR4nO3dfXATZR4H8C9Jm2xempfmrWmbvkFpub5ASuFA3g+sBQfrOI6ep4cIVEGFOx05PT1P9OZGRkd8vzk9LTdjK8rVF+ihWMGKCAhF8Ci0xULBFlKapk3S5r1pcn+UMGn6QlLy2n0+f3U3m80P5jv77PPs7rOT3G43CCJaMSJdAEGMhQSUiGokoERUIwElohoJKBHVSECJqBbn9TcPQFakCiEIL2YArcDQgGZZLeatbRcvRKYkggDA5fIglkgu8BMEjwJDA4q2ixfw0rbXIlMZQQDIyczAw5s2XVsecg7KYDAGwl0QQXiTShKHLA8JKIvFsoW1GoLwIZVKodNqWz3L3gHVJiWnUhGoiSCGYFOUw/O3d0A72RTFjEA9BHGNRCqDMiVV71keNg'+
			'4qEQrCWxFBeJHKZQBwyrM8JKC6Lm2HNDHR9zsEETZJySkO7+UhAXW73R2q1JTwVkQQXoQiMQtAg2d5SED5fH5rWnpGuGsiCACDY6Bms8nsvW5IQDlc3vHMKVOcYa2KIK5Spaag327/xXudbyepNTlVFQeCiIC09AzEsVj13ut8A2o2Ggw9OZkZYSuKIDzyCgudfH7Cce91w4aZ+vsdp4rU08NXFUFgcHiTx09w4+pdTB7DAiqVyb/PzS9wha0yggCgLiyA0WgYdivdSDcsH0lNy2CQAXsinOYvWgypTL7Hd/2Id9TrurQ/qwsLQl8VQWCweVdlZALAEd/PRgyoVCbfs2DJEtLME2GhLixAZ4fmPAbvpB9itGeS9smTkl0qpSK0lREEgAVLlrgUyuTdI3026kNzFlPfjzcvXRa6qqKUTCyi5s8qksvEorDdesjlUHHz'+
			'ZxXJ05RJvHD9ZrRQKRWQJyW7AOwb6fNRB+VFiZLK4jlzi3dUVzOtNnvICowma39/37Ss7OxEAFgOoLWlpefQ4UOa5vMXjKH4PZlYRM2eWSQv/vWcZBabzQQAh90+sL2ioqGt48qw5m4iunnpMlgtljoWizXi52NdNWq1mE0dJYsXpe7aWxua6qIMm6KG/H9kZWcnZmVnJ5r6eu0Hv61r+77+hDYYv5OmTOItXDA/eVp+gdz3M09Q6UAiFGDe4iUAsGO0bca8rCmWSN8quXXli7XfHphEl6PoSPgJAvbylWXZS0tKs44f/UFTd+iwxmK1BXzPwvxZRfLCGUWKFJVqzDE8uhw9S5YuQZe284RMrugcbRvmli1bxtqHtq/XuCgebsHZc+eDXmC06TPq7dNnzBh2VPNgxsUxVOkZwoULFqQmy6SUoafHbjSZ+sfaJ5dDxS'+
			'2aOzvp7rvvyskrmC4XCIXssbZvOt2gPXWmsWe8/4ZYwaHYKH9ow4BAKHoTwKgt0yQ/5gctsFmtLz6x+QlaHEXnzyqSL19Zlu3v9pfb23t/OHJIc+J0U7f3eplYRN1y8zLV5OypEn+bbV2X1vzqm2//FGjNseieO8pQPOemRlGiZPNY2/kTUOi7de8crPuGNuei9911Z/ZI54dj8ZynWqzWAbV6ptzT2fKXw24feHnbtuPjOXWINRKhAC+9sg0A1gAYtXkH/AwogAK7zfb3Z595mtlt7A1CidHvsY2PzJDK5GEZ9qFbz/3Rh8qROXnKUVGi5IXrbevv5GENvb3G/93z27tvsLTY8c5775829fWG5Zxmf+3eVrqEMyczA/nT1f2iRMkr/mzv9+x2Mrlia/50db86L3fcxcUSi9XmrKqsanLY7SGdbeXwdwfagzV8Fe04'+
			'FBvr1q932x32DzHCZc2RBDL9otnusH+4bv0jbg41Zkd0wmjruGKu2fVZS6j239rS0rNn3zdtodp/tLl9RSmYTGYbn5+w09/vBDQ/KJ+fsNPp7L+49v5VgVcXo06cbuqu+/qroE/5p+vSmndUV4cs/NEmJzMDC5eWOIUi8fOBfC/gCWz5CYIn86er++cVFwX61Zi17+BhTdPphqA1ww67faCysqqZDj12YLBp3/j4425Hv6MK1+m1+xrPDMvmXqPh9XtXP0Cru50+rfnvBV2XNigdme0VFQ1degNtJmpbe/8q2K3WpkCado9xTQEukcnr+vr6vltXXu6iy/moxWpzvvPe+6dvtNP0Zc2uFrr02AGgrLQEU3+VZxYlSraM5/vjnqNeKpO/nCAQttPpfNRitTm3V1Q0jDekJ+uPaejSYwcAdV4uSleWOXk8/lPws9fu64'+
			'ZeoiAUiTfn5uWb7rmj7EZ2E1PaOq6Y99fubb3+lkNdbm/vra7ZQ5v51VVKBco3POKyWi3vwudJzUDc6Fs+zBwu788Ll93ipFOn6fv6E9qT9cc0/m6v69Ka/11Z2RTKmqKJRCjAk08/4+53Oj8QisTDHoQLRDBeQ9PKYrEeu2/NOlqFtLpmz4XWlpbr3nXksNsHPqn+pIVOPfZNf9jkslutx8bTKfIVrPcktVot5nfXrH8YdArpjurqluv17D/+aEcTXTpFHIqNp/602cXnJ9T7c53dH0F7kZdQJN5jNOj/QaeQWqw2Z2VlVfNonaYva3a1hOpxkWgTinACQX7THB1D2qU32LZXVDT4rm863aClS489VOEErn9HfcAoitNiNOiNM2fPURs6Oxjtmo6g7j8aGU2mfkNXpyUtLS0BwKSfm5t0lTvpcRlTpVRg06aNIQkn'+
			'4P/9oOOR5XA4Xt27+/M4utzoDAw+4kGXDpFKqcBTT//FbbNajoUinEBoAwoAWWazaetP9fW8HdXVoMMjI3Qxr7gI965+wGWz2f55o0NJYwl1QAGAZ+rrfdug75FsfellBglp7CsrLUHpbbc7+4yG1yQyeV0ofyscAQUAGHq6/0pxuLPf2PbKpLMXLoblN4ng4lBsbHx4A5KSU3uEItFzuIErRP4KW0ABwGjQ38rhcB/cW7OLVuelE0FOZgbKN2wYiI+Pb+QnCP6GcV5bD1RYA3pVltFgeL6v1yB64/U3GHR5CC+WlZWWoHRlmdPR76gKxtWhQEQioAAAo8HwR4piL/30448Y+w4eikgNxNhUSgXWlZe7ROLEbn6C4AWEoUn3FbGAXlVgNpue1bS3UVVVVcz2joButiZChEOxUbJ4EW678y7ou3W7xBLpu5GqJdIBBQ'+
			'Dou3UPiiXSst3VO1H77QEyHBVB6rxc/G7VajeTyWy7+vxQRI8aURHQqxRGg/45l8uV+tnOj5mHjp+IdD20IhEKsHbtGmRNmdpvsZj/FcqxzUBEU0A95prNpsf03TrOhx9UMsiQVGhJhAKUrVyJWXNvcppNfXvEEmkVwtRD90c0BhTA4JBUXHz8/Zr2NuqzTz5lkqAGlyeY8xZfmwLxLUS4OR9J1AbUw2oxP+Byu5dr2tuor778gnnyTHOkS4ppsRJMj6gPqIfniGq32bif/2fnpBOnz5DOVAByMjMwb968mAmmR8wE1MtcfbduNZfHSz74zT5G7f46kMH+kXEoNory81CyYoVLlCix99vttdF2jnk9sRhQD4WuS7tKIBTN117RMGq/+IJBjqqD1Hm5UKtnonjO3AGDvufi1Ve8jPgWjWgXywH1tqyzQ3ObQpk8+WT9'+
			'UZz88UfQLaw5mRkoUk/Hgt8sc7lcLktfb2+NPEn5NWKgGR/LRAmoB89o0C+2Wa23KJTJk1uaGweOHz3KPHvuHCbaVSpP852TOw3Fc+a4XC6XxeHoPyIUiXYjApckQ2WiBdQbD0Bhl7ZzBYfLneZ0OOLPnDoVd7a5Cc0t52LuvJVDsZE7ORO5OTnIzS9wpaZlMPTdukvxbPZ+Pj/hAGL8SDmaiRxQX1kmU1+xzWqdS3E4qvi4eNbF1nPMs42NaGtvQ9slTdSElkOxkaZUQpWagrT0DEyeOtWRlJzC0nfrLtlstgZlSuoBAMMe1JuI6BRQXwoAWR2XL82kKKqAnyBQxLNY8ed/bnb0Go2s9l9+gU6ng667BxabNeinCJ4QAkBuzlRwuRyoMjKRlJziEIrELF2XtsPtdndwuNwGPj+hCTQJpC86B3QkPABZABQdly/lcj'+
			'icdOfAgJjD5Qp4PD4PAIwGvcPQ091vs9muvWDhbGPjiDtTpaeDyxvcLJ7FsjIZDFd61pRr3+vs0GgpDseq7+4+p1Aq9Rwu7zgG3xk0IZvr8SABDVwWBoPsrXCUbTsxPGy0PBKOFwkoEdWCOrMIQQQbCSgR1UhAiahGAkpENRJQIqqRgBJR7f/05eKDB5UCEQAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Pano Prev_B";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='bottom : 0px;';
		hs+='cursor : pointer;';
		hs+='height : 40px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 84px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 0%';
		me._pano_prev_b.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._pano_prev_b.onclick=function (e) {
			player.openNext("{"+player.getPrevNode()+"}","");
		}
		me._pano_prev_b.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._button_silhouette_next_previous.appendChild(me._pano_prev_b);
		el=me._pano_next_a=document.createElement('div');
		els=me._pano_next_a__img=document.createElement('img');
		els.className='ggskin ggskin_pano_next_a';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKgAAABQCAYAAACNij7AAAALzElEQVR4nO2de1QU1xnAP/YxM7uzL9gHokLYFcJDiIFIGgFfCYnNIcRELa1ZTg3oSdLmj+ZRPZ6cNjFJ25MmmrfVFBNjmkfrsYlJTywac2KNa2jkkSDIgkGEFQj7YGd2Z9kHrNs/FCsIwsI+Zpn7+wd2Ht98HH7nfvfO3DsbFwgEAIFgK7xoJ4BAXA8kKILVIEERrAYJimA1SFAEq0GCIlgNEhTBagTRToDl5F7+qbPbrIk0RUlUGo1uyOcT+nw+DAAgQamKF2KYcKIA1ICN8nq9Pr5A4CcIwuN2u+0up9OeuiCtHwDOAQBz+acr7H9NDBKHbtQDCQA6hnFmOSgqDcNxnVyuUAkxTNjf1+um7AMiq9kCNqsFAACMbe2jTrYODICNdkwYPEObOupz8v'+
			'x5QJLkpX3Z2SDEMLcu7UYRAEB/X68Zx3FrIBA4G69UnYZL4vaH6g+NRbgoqI5hnIvdg4O5YpLMIEkJ2dFu9P3Y24d1d50H04WeSaULFxnaVFApE0ClUkFGdjak6tL8PB7vIuN09Hs8ntNJ8+b/BzjW2nJB0ESGcS73uN1L5HKF1sU441qamgRtxlbo7ukBUx+7GyilXAYp8+dCZkYG3KBb4E/PzObTFDUwNORrUqk1NQBwOto5hpPZKqjOajGvFYnFBQI+X1RXW8trM7aC8ewPUWkZQ02GNhXy8xZB9k03D6s0ieCgqXMqteZzAPgGZlnrOpsE1Vkt5rVyuaKIYZy8+tqT/BPf1LK+hZwpSrkMMtPTIO+WWyCv4CfQ39fbQYhEh+WK+GMwC2SNdUET7TbraowgSnxeL8EVKSdCROCQn7MQlq5Y4b9BlxagKXuTWpO4'+
			'H2K4GxCrguZaLeYqlVpzo+HYV2AwGKCt83y0c2IVSrkMipfcBktvv8OP4bgnEAgckEikn0OMtaoxJShN2UsFQuEGn9dL1PzrM77hVB24Pd5op8V68hZmQlHxUli4KG94kHHWKxKUb0GM3L6KBUFJu82qxwiipNfUTXx97BjfUNcQ7ZxiEqVcBqvLyqBoxUqwmPsb1JrEN4HlorJZUNJus+pJqay084d2waefHERlPESICBzuWrEcVt1zr9/hoL9ns6isFHSklPeauolP/vkxH4kZHkZEvXdd+UiL+gKwrI/KNkFzaYra4vcPx+/ZvTsOiRkZRkp/QWHRsMvp+DxeqfprtHMagS2CktSAbZtILM78+B9/5x392hDtfDhJclIi6PV6/9zkFA9JSp4HFtyeirqgDOMsxzBcf+qkQfDRgQNoVM4CihbnwwMbKgMe92CrIk'+
			'G5DaJY9qMpaCJN2Z/x+/0pqJyzDxGBw8YNv4ScRXlDDpp6TanWfBWNPKIiKE3ZS0Ui8UPHvzwiOHioBrWaLCZvYSZseuTRwPDwULNEKnseItyaRlpQkhqwPUmIxLe+/vIO1GrGCCOt6Y3ZC10kKdkKl6b8RYRICqqjKerZH3svJLzxl12o1YxBSpYWwfoNlcAwzn0SiXR/JK4ZEUFtFvNKqVzxWM1nBwWf1hwJ+/UQ4SM5KRG2PvW7gMc9+K0iQflcuK8XdkHdg65KHo+3pnrXTl5jizGs10JEBhGBw9Ytmy8q4hNsEqnsUQhjvzSsglIDtqcvBgK3vv7qK3FcnQI3m6mq0MPNBQVh7ZeGS1CSGrBtYxhn5gsvvsTjSn+zuCBfk6rVyQ9/cdRksVOeaOcTCdavWQ3LSlYNYxj2OIRB0nAISjJOx067zab+8/btnBgM'+
			'iUWE4OFNG3NUag05su2tXbu+6+77kVXPtcNF0eJ8qKjaFBZJQ/3iBpJxOnb2mLo5IycAwJqye7RXywkAUFlVlauOVxDRyimSGOoa4P139gh8Pt8rAKALZexQCkoyTsfO7+vr1S++/Cpn5CwtuT0lKydXM3Y7huP8igp9plhEcOLlGOGSNGSCulzMG3abTf3RgQOhCsl68nOylIXLlidPtF+l1pDr161Lj2RO0SQckoZEUGrA9rTdZuVUWU9JmkOWrb5/Uvl06ekJpSW3p0QiJzYQaklnLKjVYt7MMM4CLo3WxSJCoK/QZ2E4zp/K8YXLlicXF+Rf0w2YrRjqGuDUSYOAcTqehkuvFpo2MxKUpuylUql02Z7qas7ICQDw8KaNORKpDA/mnDvu+qkuJWnOjP5ZscQ7738AZ42tapqyvzSTODMRVCdXxP/6tR07eFy6CV'+
			'9Rvi597Ih9KmA4zq+sqsrlyqAJAODtfe+B00EnWy3mzdONMV1BSZeLeeGjfXs5tZCtuCBfM96IfapgOM5/eNPGHK5I6vZ4YU91NU8qlS6zWcwrpxNjWoJSA7Zt7WdaSC4tzchcoJXfXbZ6xiNylVpDrim7RxuKnGIBU18/VO/ayZPKFY8BQGKw5wctKMM4ywmROOvtfe8Fe2rMkpI0h/z5L9ZnhSpeVk6upmRp4dxQxWM7jS1GOH70sICm7M8Ee26wgiZiQkz/+ss74rgyKBKLCMHadWvTpzpinyor71ylzc/JUoYyJps5eKgG/H5/invQVRnMeUEJyjgdvz/+5REBl/qdD1ZUZE1nUDQVylbfn86Vkb3b44U9u3fH8fmC+yCIUj9lQW0W80qBQJh68FDNtBKMRdaVlWrnJSfLwhUfw3G+vkKfxZVBU1vneTj+5ZGg'+
			'Sv1UBSUJsfhXH+7by5nSXlyQr8kruDXs/USJVIY/WFERsv4t2zl4qAZwnEgBgCVTOX5KgtptVv2FrvMkV17alZI0h5zOiJ1xOrw9JlPQr3Cel5wsqyjnxjN7t8cLH+7bG+dyMY9P5fiplBaSlMpKP929e4apxQbqeAVRWVWVO/mR/6fHZHI0fdfQf+JUg3kkxqo7S5KDuWealZOrKe48R4/EmM0Y6hrgvp+ViwOBQPlki+8mnbDsYpin2ltbit58qzqkSbKR8SYeX4/W5tPmuvp6s7Gjk54o3sqiwrk35eVppvpo9G/73m2eKN5sIkObCo9v3jIkxDA9XGdN02SCkgCwf8uTT8yKLx+YjIrydemTtXo+r9df99/a3m/rG8zBLOsoLsjXFNy2ZO5k8vu8Xv+zf/xT7VTjxjJbf/uEf37KDZ+IxOTeiY65bom326z68+'+
			'c6OCEnAIBMPvEMeMbp8H597KvuhuYzA4Nuz3CwsU+cajCfONVgzlyglRcVFs3VpacnjHcchuN8sYgQTOcascbhfx/iVz70yN0AMC1BSVIqK/3iyBehzyyG6DGZHLXfGHobmlttoYhn7OikjR2dtDpeQSwvLkpaeNOixLEPAVLmJpFcKPONLUZ4wOMRk6SkBACOjnfMhCWepuylTgf9yDPP/YEzXzibuUArz8nOTkjPzFSaurroSKzOFIsIQeHifE3uzXkaB0V7u86fo0/WNZi50IICXFpwV66vsEiksgfH2z+hoIzT8e7+D95Xc+XWEiI6iAgcdmzffhEnRL+BcVaETtQ6JgqFQmVDc0t4s0NwHrfHC3W1tTyaou4db/+4gtIUtb6utpZTs+QR0cNgMABfwC8eb9+4gmKYcEljY314s0IgLtPWeR6GfD4MxllkN56g'+
			'iTweT4xe9IWIJGeamvjjlflrBGUY5/Izp5s4M3JHsIPLFfuWsduvEdHjdi9prEflHRFZGluMIFcoEmDMXNFrBJXLFVrj2R8ilhgCMcJZ4xk/jOmHjhVUd/Gin8+VR5sIdtHa3My3WsyjRvPXCIr6n4ho0W3qBj6Pl3b1tlEyWi3mPFNXV2SzQiAu032hF+KVqvlXbxslqFAo1HabuiObFQJxmau6llcGSqMEFQiFqsFBTry5GsFSOtqNPgC4Mid3lKAkKSG5tKQYwT4cNI3BBC0oJ9ZnI9iNqasL3IOuKy8FvlpQXX9frzsKOSEQo+jv64sf+X1UiXe5mMhng0CMgZRKxxd0yOcTRT4dBOL/GNvaQSQSXRF01JoksZiEDG1qpHNCIK6QPH/eqM9XC+rCcNy8oaoqshkhEGPw+y9emQwS6e+LRyCCAj13R7AaJCiC1S'+
			'BBEawGCYpgNUhQBKtBgiJYzf8AUubRWapwzfAAAAAASUVORK5CYII=';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Pano Next_A";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='bottom : 33px;';
		hs+='cursor : pointer;';
		hs+='height : 40px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 84px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 100%';
		me._pano_next_a.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._pano_next_a.onclick=function (e) {
			player.openNext("{"+player.getNextNode()+"}","");
		}
		me._pano_next_a.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._button_silhouette_next_previous.appendChild(me._pano_next_a);
		el=me._text_3=document.createElement('div');
		els=me._text_3__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 3";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='bottom : 65px;';
		hs+='height : 27px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : hidden;';
		hs+='width : 95px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='bottom:  0px;';
		hs+='width: 95px;';
		hs+='height: 27px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		els.setAttribute('style',hs);
		els.innerHTML="To Entrance";
		el.appendChild(els);
		me._text_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._button_silhouette_next_previous.appendChild(me._text_3);
		el=me._image_3=document.createElement('div');
		els=me._image_3__img=document.createElement('img');
		els.className='ggskin ggskin_image_3';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAAA6CAYAAAAOeSEWAAAFFUlEQVRogdWbL3AjNxTGf+qEubQ2jnHNfTilTXCCE3oXepPSXmGTg70eTXCCa+5iG9s44YtfgZ4y8lvtetertd1vJpN4bUv69P4/KY6eICID4GdgrD+D6HeMBVAAK+AVWDrn3vpYk8s5mBI8A6bApMNQr/hNmDvn/s2xNshEVkSGwCXwS47xDF6BGTDrKvFOZDuQfMOrs1XpbXgGnpxzRcvvAR3IisglcEH1ggtgjVfHBbCqWqSInAIjvOpPgNOaqQvg0Tn30nbNrcmqNO/wziaFJfAP3t52koDOMcVv5rDiYwvg9zZztCIrImfADWlpzvA7ntWTisgEuCVN+hVPeN1krMZkleht4q0lnuSi6Vi7YMtG3zvnZtvGaERWRD6RdkLfdrGdXaGh7Qo4T7'+
			'z9cZuEt5IVkSu8x41R4NWnV2lWoULLCuBzHeFashWDrnXQnZxPLqgH/4NNtS7wEk76jR+2DHaURAFUgp/xBAMG+EiRRJKs2ob9UlCTgxMNqCA8FpGb1OerJHuBD/IxjopogBK+N4/PVTM3UCIbpYAxnprGskPAOTcH5uZxSbopyV6Z10vn3GOuhfWIezbVeSIi0/gDG2RVqmdmkP8DUdTEvpnH1/ELK1kr1dmhYuku0CwqDjsjDZ9AmezUvO4kVREZqmffJ+yay2R1B+KFzTMk9SPgu4ik0ru+MKdsu6ewKVkrVevddsUAuBGRv7WC6RVqu3btE9gkGy+kaFJFtMQI+CIid+oI+4QlewZKVsUcq3CfTmkKfBWRyx7t2a5/LCKDIFmrXn174FCqPdhYmAOqykvzeBzI2hbLvrKlEXAnIl9S6V1HWIGdBrI2D37NPPE2'+
			'TPCqfZ1RtS2HHwPZjV3tqyPfABfkC1WWwzCQ3Xfgr0MIVQ+ZQ9Wosng/AozpFqpKpnjMZAOmwG872HKp9j6J3jgmVY7xBDzv0DgoNfED2TX+ePGYMMe3anM5y8VJ6qmITA5Y2r3hm95d5y/ZeSC7YlOyh1DpnQ+sKmBzh0VMNsaEfFVPE7zgieZs6Nmwtaoiuy/7XQJ/5W7mRVccAt6cc8UJ+HakiMQeeSwiwx4zqTe88+lLe2xxsYDNOGsnzl6N4O3yCX9E0aeZJBsRdWRzt1LmeJK5bXMD0UF2QBE29j30OOfmRpVHIjLNIIE1XmX3Fcpsh/R9/TZdtG6/q3RXzrmP+yKqjsmq8Ht7yZJ9ptyZs03zxjjA2ZA9mV/GG71BVhdnpXuzLQnX/nDfTbRaaDlYe5qRqnqsdAek71KESU6Br1TfnukdKoxP5vHSmk+JrE'+
			'rXHgFOU+psTr8PRhYvjDg9LIA/7YeS9ax6YKvOt3HnIHHMn7th1gh68Gyd0ksqIaor3h8pdxnvROS04j7D3m1Wtc1GjHnVEeu2CyRDvD1aB5Us9p1zvzZfajeIyDW+QRej9s5HbVtGVcHeWYCKEnAfZzkiMtB7Wa2IQoMeVHRJowl6VWXdzAfKF9Aa3eJJdioq0KRPZQvmLNhy1bfxdaWtZCucURWyqrGSPKessgEz55wNk5WoJduSKMBPTSeumXOIL7w/UF1mFvg+VasipZLsDkShhRorqfAz0nkmbE9OZvgqqnXeXSfZD/iOQqtkoUlnUol+bzMuGa76VpLVwPyoiwvX2sf43Q8SSaGJR27jyOb4jKhzmdjIG2v4WWO6GRoKghrGf3dBgf6bCx2u3qeQ9f96mkKrlFs8sZDDrvDFfm/Hpf8By+cXdSvW7mAAAAAA'+
			'SUVORK5CYII=';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Image 3";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='bottom : 75px;';
		hs+='cursor : pointer;';
		hs+='height : 39px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : hidden;';
		hs+='width : 39px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._image_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._image_3.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.ggUserdata.customnodeid == "end"))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._image_3.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._image_3.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._image_3.style[domTransition]='';
				if (me._image_3.ggCurrentLogicStateVisible == 0) {
					me._image_3.style.visibility=(Number(me._image_3.style.opacity)>0||!me._image_3.style.opacity)?'inherit':'hidden';
					me._image_3.ggVisible=true;
				}
				else {
					me._image_3.style.visibility="hidden";
					me._image_3.ggVisible=false;
				}
			}
		}
		me._image_3.onclick=function (e) {
			player.openNext("{"+player.getNextNode()+"}","");
		}
		me._image_3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._button_silhouette_next_previous.appendChild(me._image_3);
		me.divSkin.appendChild(me._button_silhouette_next_previous);
		el=me._footer=document.createElement('div');
		el.ggId="footer";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='bottom : 0px;';
		hs+='height : 36px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 100%;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._footer.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._footer.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		el=me._footer_bg=document.createElement('div');
		el.ggId="footer_bg";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : #1b1b1b;';
		hs+='border : 0px solid #000000;';
		hs+='bottom : 0px;';
		hs+='cursor : default;';
		hs+='height : 40px;';
		hs+='left : -10000px;';
		hs+='opacity : 0.45;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 100%;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._footer_bg.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._footer_bg.logicBlock_size = function() {
			var newLogicStateSize;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStateSize = 0;
			}
			else {
				newLogicStateSize = -1;
			}
			if (me._footer_bg.ggCurrentLogicStateSize != newLogicStateSize) {
				me._footer_bg.ggCurrentLogicStateSize = newLogicStateSize;
				me._footer_bg.style[domTransition]='width 0s, height 0s, opacity 0s, background-color 0s';
				if (me._footer_bg.ggCurrentLogicStateSize == 0) {
					me._footer_bg.style.width='100%';
					me._footer_bg.style.height='60px';
					skin.updateSize(me._footer_bg);
				}
				else {
					me._footer_bg.style.width='100%';
					me._footer_bg.style.height='40px';
					skin.updateSize(me._footer_bg);
				}
			}
		}
		me._footer_bg.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._footer_bg.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._footer_bg.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._footer_bg.style[domTransition]='width 0s, height 0s, opacity 0s, background-color 0s';
				if (me._footer_bg.ggCurrentLogicStateAlpha == 0) {
					me._footer_bg.style.visibility=me._footer_bg.ggVisible?'inherit':'hidden';
					me._footer_bg.style.opacity=1;
				}
				else {
					me._footer_bg.style.visibility=me._footer_bg.ggVisible?'inherit':'hidden';
					me._footer_bg.style.opacity=0.45;
				}
			}
		}
		me._footer_bg.logicBlock_backgroundcolor = function() {
			var newLogicStateBackgroundColor;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStateBackgroundColor = 0;
			}
			else {
				newLogicStateBackgroundColor = -1;
			}
			if (me._footer_bg.ggCurrentLogicStateBackgroundColor != newLogicStateBackgroundColor) {
				me._footer_bg.ggCurrentLogicStateBackgroundColor = newLogicStateBackgroundColor;
				me._footer_bg.style[domTransition]='width 0s, height 0s, opacity 0s, background-color 0s';
				if (me._footer_bg.ggCurrentLogicStateBackgroundColor == 0) {
					me._footer_bg.style.backgroundColor="rgba(0,0,0,1)";
				}
				else {
					me._footer_bg.style.backgroundColor="rgba(27,27,27,1)";
				}
			}
		}
		me._footer_bg.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._footer.appendChild(me._footer_bg);
		el=me._menu_footer2=document.createElement('div');
		el.ggId="menu_footer2";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 5px;';
		hs+='border-radius : 5px;';
		hs+='border : 0px solid #000000;';
		hs+='bottom : 0px;';
		hs+='cursor : default;';
		hs+='height : 40px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : hidden;';
		hs+='width : 600px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._menu_footer2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._menu_footer2.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._menu_footer2.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._menu_footer2.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._menu_footer2.style[domTransition]='';
				if (me._menu_footer2.ggCurrentLogicStateVisible == 0) {
					me._menu_footer2.style.visibility=(Number(me._menu_footer2.style.opacity)>0||!me._menu_footer2.style.opacity)?'inherit':'hidden';
					me._menu_footer2.ggVisible=true;
				}
				else {
					me._menu_footer2.style.visibility="hidden";
					me._menu_footer2.ggVisible=false;
				}
			}
		}
		me._menu_footer2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		el=me._text_1_6_2=document.createElement('div');
		els=me._text_1_6_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_6_2";
		el.ggDx=45;
		el.ggDy=3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size:7px;";
		els.setAttribute('style',hs);
		els.innerHTML="Information Sources";
		el.appendChild(els);
		me._text_1_6_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_6_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_6_2.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_6_2.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_6_2.style[domTransition]='';
				if (me._text_1_6_2.ggCurrentLogicStateText == 0) {
					me._text_1_6_2.ggText="Sources d\'information";
					me._text_1_6_2__text.innerHTML=me._text_1_6_2.ggText;
					if (me._text_1_6_2.ggUpdateText) {
					me._text_1_6_2.ggUpdateText=function() {
						var hs="Sources d\'information";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_2.ggUpdatePosition) me._text_1_6_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_2.ggCurrentLogicStateText == 1) {
					me._text_1_6_2.ggText="Informationsquellen";
					me._text_1_6_2__text.innerHTML=me._text_1_6_2.ggText;
					if (me._text_1_6_2.ggUpdateText) {
					me._text_1_6_2.ggUpdateText=function() {
						var hs="Informationsquellen";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_2.ggUpdatePosition) me._text_1_6_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_2.ggCurrentLogicStateText == 2) {
					me._text_1_6_2.ggText="Fonti di informazione";
					me._text_1_6_2__text.innerHTML=me._text_1_6_2.ggText;
					if (me._text_1_6_2.ggUpdateText) {
					me._text_1_6_2.ggUpdateText=function() {
						var hs="Fonti di informazione";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_2.ggUpdatePosition) me._text_1_6_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_2.ggCurrentLogicStateText == 3) {
					me._text_1_6_2.ggText="Fuentes de informaci\xf3n";
					me._text_1_6_2__text.innerHTML=me._text_1_6_2.ggText;
					if (me._text_1_6_2.ggUpdateText) {
					me._text_1_6_2.ggUpdateText=function() {
						var hs="Fuentes de informaci\xf3n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_2.ggUpdatePosition) me._text_1_6_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_2.ggCurrentLogicStateText == 4) {
					me._text_1_6_2.ggText="\u4fe1\u606f\u6765\u6e90";
					me._text_1_6_2__text.innerHTML=me._text_1_6_2.ggText;
					if (me._text_1_6_2.ggUpdateText) {
					me._text_1_6_2.ggUpdateText=function() {
						var hs="\u4fe1\u606f\u6765\u6e90";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_2.ggUpdatePosition) me._text_1_6_2.ggUpdatePosition();
					}
				}
				else {
					me._text_1_6_2.ggText="Information Sources";
					me._text_1_6_2__text.innerHTML=me._text_1_6_2.ggText;
					if (me._text_1_6_2.ggUpdateText) {
					me._text_1_6_2.ggUpdateText=function() {
						var hs="Information Sources";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_2.ggUpdatePosition) me._text_1_6_2.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_6_2.onclick=function (e) {
			player.setVariableValue('vis_info_popup_3', true);
		}
		me._text_1_6_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer2.appendChild(me._text_1_6_2);
		el=me._text_1_5_2=document.createElement('div');
		els=me._text_1_5_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_5_2";
		el.ggDx=-45;
		el.ggDy=3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size:7px;";
		els.setAttribute('style',hs);
		els.innerHTML="Cookie Policy";
		el.appendChild(els);
		me._text_1_5_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_5_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_5_2.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_5_2.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_5_2.style[domTransition]='';
				if (me._text_1_5_2.ggCurrentLogicStateText == 0) {
					me._text_1_5_2.ggText="Politique en mati\xe8re de cookies ";
					me._text_1_5_2__text.innerHTML=me._text_1_5_2.ggText;
					if (me._text_1_5_2.ggUpdateText) {
					me._text_1_5_2.ggUpdateText=function() {
						var hs="Politique en mati\xe8re de cookies ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_2.ggUpdatePosition) me._text_1_5_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_2.ggCurrentLogicStateText == 1) {
					me._text_1_5_2.ggText="Cookie Richtlinie ";
					me._text_1_5_2__text.innerHTML=me._text_1_5_2.ggText;
					if (me._text_1_5_2.ggUpdateText) {
					me._text_1_5_2.ggUpdateText=function() {
						var hs="Cookie Richtlinie ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_2.ggUpdatePosition) me._text_1_5_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_2.ggCurrentLogicStateText == 2) {
					me._text_1_5_2.ggText="Policy sui cookie ";
					me._text_1_5_2__text.innerHTML=me._text_1_5_2.ggText;
					if (me._text_1_5_2.ggUpdateText) {
					me._text_1_5_2.ggUpdateText=function() {
						var hs="Policy sui cookie ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_2.ggUpdatePosition) me._text_1_5_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_2.ggCurrentLogicStateText == 3) {
					me._text_1_5_2.ggText="Pol\xedtica de cookies ";
					me._text_1_5_2__text.innerHTML=me._text_1_5_2.ggText;
					if (me._text_1_5_2.ggUpdateText) {
					me._text_1_5_2.ggUpdateText=function() {
						var hs="Pol\xedtica de cookies ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_2.ggUpdatePosition) me._text_1_5_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_2.ggCurrentLogicStateText == 4) {
					me._text_1_5_2.ggText="Cookie \u653f\u7b56 ";
					me._text_1_5_2__text.innerHTML=me._text_1_5_2.ggText;
					if (me._text_1_5_2.ggUpdateText) {
					me._text_1_5_2.ggUpdateText=function() {
						var hs="Cookie \u653f\u7b56 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_2.ggUpdatePosition) me._text_1_5_2.ggUpdatePosition();
					}
				}
				else {
					me._text_1_5_2.ggText="Cookie Policy";
					me._text_1_5_2__text.innerHTML=me._text_1_5_2.ggText;
					if (me._text_1_5_2.ggUpdateText) {
					me._text_1_5_2.ggUpdateText=function() {
						var hs="Cookie Policy";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_2.ggUpdatePosition) me._text_1_5_2.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_5_2.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 90))
				)
			) {
				player.openUrl(player.getVariableValue('urlPre')+""+player.getVariableValue('ExtValueSu')+"\/cookie-policy\/","_blank");
			}
			if (
				(
					((player.getVariableValue('ExtValue') == 90))
				)
			) {
				player.openUrl("https:\/\/www.getac.com.cn\/zh\/cookie-policy\/","_blank");
			}
		}
		me._text_1_5_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer2.appendChild(me._text_1_5_2);
		el=me._text_1_4_2=document.createElement('div');
		els=me._text_1_4_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_4_2";
		el.ggDx=90;
		el.ggDy=-20;
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size:7px;";
		els.setAttribute('style',hs);
		els.innerHTML="Term of Use<br\/>";
		el.appendChild(els);
		me._text_1_4_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_4_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_4_2.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_4_2.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_4_2.style[domTransition]='';
				if (me._text_1_4_2.ggCurrentLogicStateText == 0) {
					me._text_1_4_2.ggText="Conditions d\'utilisation ";
					me._text_1_4_2__text.innerHTML=me._text_1_4_2.ggText;
					if (me._text_1_4_2.ggUpdateText) {
					me._text_1_4_2.ggUpdateText=function() {
						var hs="Conditions d\'utilisation ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_2.ggUpdatePosition) me._text_1_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_2.ggCurrentLogicStateText == 1) {
					me._text_1_4_2.ggText="Nutzungsbedingungen ";
					me._text_1_4_2__text.innerHTML=me._text_1_4_2.ggText;
					if (me._text_1_4_2.ggUpdateText) {
					me._text_1_4_2.ggUpdateText=function() {
						var hs="Nutzungsbedingungen ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_2.ggUpdatePosition) me._text_1_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_2.ggCurrentLogicStateText == 2) {
					me._text_1_4_2.ggText="Termini d\'uso ";
					me._text_1_4_2__text.innerHTML=me._text_1_4_2.ggText;
					if (me._text_1_4_2.ggUpdateText) {
					me._text_1_4_2.ggUpdateText=function() {
						var hs="Termini d\'uso ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_2.ggUpdatePosition) me._text_1_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_2.ggCurrentLogicStateText == 3) {
					me._text_1_4_2.ggText="T\xe9rminos de uso ";
					me._text_1_4_2__text.innerHTML=me._text_1_4_2.ggText;
					if (me._text_1_4_2.ggUpdateText) {
					me._text_1_4_2.ggUpdateText=function() {
						var hs="T\xe9rminos de uso ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_2.ggUpdatePosition) me._text_1_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_2.ggCurrentLogicStateText == 4) {
					me._text_1_4_2.ggText="\u4f7f\u7528\u6761\u6b3e ";
					me._text_1_4_2__text.innerHTML=me._text_1_4_2.ggText;
					if (me._text_1_4_2.ggUpdateText) {
					me._text_1_4_2.ggUpdateText=function() {
						var hs="\u4f7f\u7528\u6761\u6b3e ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_2.ggUpdatePosition) me._text_1_4_2.ggUpdatePosition();
					}
				}
				else {
					me._text_1_4_2.ggText="Term of Use\n";
					me._text_1_4_2__text.innerHTML=me._text_1_4_2.ggText;
					if (me._text_1_4_2.ggUpdateText) {
					me._text_1_4_2.ggUpdateText=function() {
						var hs="Term of Use\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_2.ggUpdatePosition) me._text_1_4_2.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_4_2.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 90))
				)
			) {
				player.openUrl(player.getVariableValue('urlPre')+""+player.getVariableValue('ExtValueSu')+"\/terms-of-use\/","_blank");
			}
			if (
				(
					((player.getVariableValue('ExtValue') == 90))
				)
			) {
				player.openUrl("https:\/\/www.getac.com.cn\/zh\/terms-of-use\/","_blank");
			}
		}
		me._text_1_4_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer2.appendChild(me._text_1_4_2);
		el=me._text_1_3_2=document.createElement('div');
		els=me._text_1_3_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_3_2";
		el.ggDx=0;
		el.ggDy=-20;
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size:7px;";
		els.setAttribute('style',hs);
		els.innerHTML="Privacy Policy";
		el.appendChild(els);
		me._text_1_3_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_3_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_3_2.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_3_2.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_3_2.style[domTransition]='';
				if (me._text_1_3_2.ggCurrentLogicStateText == 0) {
					me._text_1_3_2.ggText="Politique de Confidentialit\xe9 ";
					me._text_1_3_2__text.innerHTML=me._text_1_3_2.ggText;
					if (me._text_1_3_2.ggUpdateText) {
					me._text_1_3_2.ggUpdateText=function() {
						var hs="Politique de Confidentialit\xe9 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_2.ggUpdatePosition) me._text_1_3_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_2.ggCurrentLogicStateText == 1) {
					me._text_1_3_2.ggText="Datenschutzrichtlinie ";
					me._text_1_3_2__text.innerHTML=me._text_1_3_2.ggText;
					if (me._text_1_3_2.ggUpdateText) {
					me._text_1_3_2.ggUpdateText=function() {
						var hs="Datenschutzrichtlinie ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_2.ggUpdatePosition) me._text_1_3_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_2.ggCurrentLogicStateText == 2) {
					me._text_1_3_2.ggText="Informativa sulla Privacy ";
					me._text_1_3_2__text.innerHTML=me._text_1_3_2.ggText;
					if (me._text_1_3_2.ggUpdateText) {
					me._text_1_3_2.ggUpdateText=function() {
						var hs="Informativa sulla Privacy ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_2.ggUpdatePosition) me._text_1_3_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_2.ggCurrentLogicStateText == 3) {
					me._text_1_3_2.ggText="Pol\xedtica de privacidad ";
					me._text_1_3_2__text.innerHTML=me._text_1_3_2.ggText;
					if (me._text_1_3_2.ggUpdateText) {
					me._text_1_3_2.ggUpdateText=function() {
						var hs="Pol\xedtica de privacidad ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_2.ggUpdatePosition) me._text_1_3_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_2.ggCurrentLogicStateText == 4) {
					me._text_1_3_2.ggText="\u9690\u79c1\u653f\u7b56 ";
					me._text_1_3_2__text.innerHTML=me._text_1_3_2.ggText;
					if (me._text_1_3_2.ggUpdateText) {
					me._text_1_3_2.ggUpdateText=function() {
						var hs="\u9690\u79c1\u653f\u7b56 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_2.ggUpdatePosition) me._text_1_3_2.ggUpdatePosition();
					}
				}
				else {
					me._text_1_3_2.ggText="Privacy Policy";
					me._text_1_3_2__text.innerHTML=me._text_1_3_2.ggText;
					if (me._text_1_3_2.ggUpdateText) {
					me._text_1_3_2.ggUpdateText=function() {
						var hs="Privacy Policy";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_2.ggUpdatePosition) me._text_1_3_2.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_3_2.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 90))
				)
			) {
				player.openUrl(player.getVariableValue('urlPre')+""+player.getVariableValue('ExtValueSu')+"\/privacy-policy\/","_blank");
			}
			if (
				(
					((player.getVariableValue('ExtValue') == 90))
				)
			) {
				player.openUrl("https:\/\/www.getac.com.cn\/zh\/privacy-policy\/","_blank");
			}
		}
		me._text_1_3_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer2.appendChild(me._text_1_3_2);
		el=me._text_1_2_2=document.createElement('div');
		els=me._text_1_2_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_2_2";
		el.ggDx=-90;
		el.ggDy=-20;
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size:7px;";
		els.setAttribute('style',hs);
		els.innerHTML="Imprint";
		el.appendChild(els);
		me._text_1_2_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_2_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_2_2.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_2_2.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_2_2.style[domTransition]='';
				if (me._text_1_2_2.ggCurrentLogicStateText == 0) {
					me._text_1_2_2.ggText="Mentions l\xe9gales ";
					me._text_1_2_2__text.innerHTML=me._text_1_2_2.ggText;
					if (me._text_1_2_2.ggUpdateText) {
					me._text_1_2_2.ggUpdateText=function() {
						var hs="Mentions l\xe9gales ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_2.ggUpdatePosition) me._text_1_2_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_2.ggCurrentLogicStateText == 1) {
					me._text_1_2_2.ggText="Impressum ";
					me._text_1_2_2__text.innerHTML=me._text_1_2_2.ggText;
					if (me._text_1_2_2.ggUpdateText) {
					me._text_1_2_2.ggUpdateText=function() {
						var hs="Impressum ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_2.ggUpdatePosition) me._text_1_2_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_2.ggCurrentLogicStateText == 2) {
					me._text_1_2_2.ggText="Imprint ";
					me._text_1_2_2__text.innerHTML=me._text_1_2_2.ggText;
					if (me._text_1_2_2.ggUpdateText) {
					me._text_1_2_2.ggUpdateText=function() {
						var hs="Imprint ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_2.ggUpdatePosition) me._text_1_2_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_2.ggCurrentLogicStateText == 3) {
					me._text_1_2_2.ggText="Pie de imprenta ";
					me._text_1_2_2__text.innerHTML=me._text_1_2_2.ggText;
					if (me._text_1_2_2.ggUpdateText) {
					me._text_1_2_2.ggUpdateText=function() {
						var hs="Pie de imprenta ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_2.ggUpdatePosition) me._text_1_2_2.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_2.ggCurrentLogicStateText == 4) {
					me._text_1_2_2.ggText="\u7248\u6743\u6807\u8bb0 ";
					me._text_1_2_2__text.innerHTML=me._text_1_2_2.ggText;
					if (me._text_1_2_2.ggUpdateText) {
					me._text_1_2_2.ggUpdateText=function() {
						var hs="\u7248\u6743\u6807\u8bb0 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_2.ggUpdatePosition) me._text_1_2_2.ggUpdatePosition();
					}
				}
				else {
					me._text_1_2_2.ggText="Imprint";
					me._text_1_2_2__text.innerHTML=me._text_1_2_2.ggText;
					if (me._text_1_2_2.ggUpdateText) {
					me._text_1_2_2.ggUpdateText=function() {
						var hs="Imprint";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_2.ggUpdatePosition) me._text_1_2_2.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_2_2.onclick=function (e) {
			player.openUrl(player.getVariableValue('urlPre')+"de\/impressum\/","_blank");
		}
		me._text_1_2_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer2.appendChild(me._text_1_2_2);
		me._footer.appendChild(me._menu_footer2);
		el=me._menu_footer=document.createElement('div');
		el.ggId="menu_footer";
		el.ggDx=-5;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 5px;';
		hs+='border-radius : 5px;';
		hs+='border : 0px solid #000000;';
		hs+='bottom : 0px;';
		hs+='cursor : default;';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='visibility : hidden;';
		hs+='width : 600px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._menu_footer.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._menu_footer.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getViewerSize().width >= 1080))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._menu_footer.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._menu_footer.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._menu_footer.style[domTransition]='';
				if (me._menu_footer.ggCurrentLogicStateVisible == 0) {
					me._menu_footer.style.visibility=(Number(me._menu_footer.style.opacity)>0||!me._menu_footer.style.opacity)?'inherit':'hidden';
					me._menu_footer.ggVisible=true;
				}
				else {
					me._menu_footer.style.visibility="hidden";
					me._menu_footer.ggVisible=false;
				}
			}
		}
		me._menu_footer.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		el=me._text_1_6_1=document.createElement('div');
		els=me._text_1_6_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_6_1";
		el.ggDx=350;
		el.ggDy=-3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 25px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="Information Sources";
		el.appendChild(els);
		me._text_1_6_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_6_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_6_1.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_6_1.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_6_1.style[domTransition]='';
				if (me._text_1_6_1.ggCurrentLogicStateText == 0) {
					me._text_1_6_1.ggText="Sources d\'information";
					me._text_1_6_1__text.innerHTML=me._text_1_6_1.ggText;
					if (me._text_1_6_1.ggUpdateText) {
					me._text_1_6_1.ggUpdateText=function() {
						var hs="Sources d\'information";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_1.ggUpdatePosition) me._text_1_6_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_1.ggCurrentLogicStateText == 1) {
					me._text_1_6_1.ggText="Informationsquellen";
					me._text_1_6_1__text.innerHTML=me._text_1_6_1.ggText;
					if (me._text_1_6_1.ggUpdateText) {
					me._text_1_6_1.ggUpdateText=function() {
						var hs="Informationsquellen";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_1.ggUpdatePosition) me._text_1_6_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_1.ggCurrentLogicStateText == 2) {
					me._text_1_6_1.ggText="Fonti di informazione";
					me._text_1_6_1__text.innerHTML=me._text_1_6_1.ggText;
					if (me._text_1_6_1.ggUpdateText) {
					me._text_1_6_1.ggUpdateText=function() {
						var hs="Fonti di informazione";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_1.ggUpdatePosition) me._text_1_6_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_1.ggCurrentLogicStateText == 3) {
					me._text_1_6_1.ggText="Fuentes de informaci\xf3n";
					me._text_1_6_1__text.innerHTML=me._text_1_6_1.ggText;
					if (me._text_1_6_1.ggUpdateText) {
					me._text_1_6_1.ggUpdateText=function() {
						var hs="Fuentes de informaci\xf3n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_1.ggUpdatePosition) me._text_1_6_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_6_1.ggCurrentLogicStateText == 4) {
					me._text_1_6_1.ggText="\u4fe1\u606f\u6765\u6e90";
					me._text_1_6_1__text.innerHTML=me._text_1_6_1.ggText;
					if (me._text_1_6_1.ggUpdateText) {
					me._text_1_6_1.ggUpdateText=function() {
						var hs="\u4fe1\u606f\u6765\u6e90";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_1.ggUpdatePosition) me._text_1_6_1.ggUpdatePosition();
					}
				}
				else {
					me._text_1_6_1.ggText="Information Sources";
					me._text_1_6_1__text.innerHTML=me._text_1_6_1.ggText;
					if (me._text_1_6_1.ggUpdateText) {
					me._text_1_6_1.ggUpdateText=function() {
						var hs="Information Sources";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_6_1.ggUpdatePosition) me._text_1_6_1.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_6_1.onclick=function (e) {
			player.setVariableValue('vis_info_popup_3', true);
		}
		me._text_1_6_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer.appendChild(me._text_1_6_1);
		el=me._text_1_5_1=document.createElement('div');
		els=me._text_1_5_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_5_1";
		el.ggDx=175;
		el.ggDy=-2;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 25px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="Cookie Policy";
		el.appendChild(els);
		me._text_1_5_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_5_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_5_1.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_5_1.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_5_1.style[domTransition]='';
				if (me._text_1_5_1.ggCurrentLogicStateText == 0) {
					me._text_1_5_1.ggText="Politique en mati\xe8re de cookies ";
					me._text_1_5_1__text.innerHTML=me._text_1_5_1.ggText;
					if (me._text_1_5_1.ggUpdateText) {
					me._text_1_5_1.ggUpdateText=function() {
						var hs="Politique en mati\xe8re de cookies ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_1.ggUpdatePosition) me._text_1_5_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_1.ggCurrentLogicStateText == 1) {
					me._text_1_5_1.ggText="Cookie Richtlinie ";
					me._text_1_5_1__text.innerHTML=me._text_1_5_1.ggText;
					if (me._text_1_5_1.ggUpdateText) {
					me._text_1_5_1.ggUpdateText=function() {
						var hs="Cookie Richtlinie ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_1.ggUpdatePosition) me._text_1_5_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_1.ggCurrentLogicStateText == 2) {
					me._text_1_5_1.ggText="Policy sui cookie ";
					me._text_1_5_1__text.innerHTML=me._text_1_5_1.ggText;
					if (me._text_1_5_1.ggUpdateText) {
					me._text_1_5_1.ggUpdateText=function() {
						var hs="Policy sui cookie ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_1.ggUpdatePosition) me._text_1_5_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_1.ggCurrentLogicStateText == 3) {
					me._text_1_5_1.ggText="Pol\xedtica de cookies ";
					me._text_1_5_1__text.innerHTML=me._text_1_5_1.ggText;
					if (me._text_1_5_1.ggUpdateText) {
					me._text_1_5_1.ggUpdateText=function() {
						var hs="Pol\xedtica de cookies ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_1.ggUpdatePosition) me._text_1_5_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_5_1.ggCurrentLogicStateText == 4) {
					me._text_1_5_1.ggText="Cookie \u653f\u7b56 ";
					me._text_1_5_1__text.innerHTML=me._text_1_5_1.ggText;
					if (me._text_1_5_1.ggUpdateText) {
					me._text_1_5_1.ggUpdateText=function() {
						var hs="Cookie \u653f\u7b56 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_1.ggUpdatePosition) me._text_1_5_1.ggUpdatePosition();
					}
				}
				else {
					me._text_1_5_1.ggText="Cookie Policy";
					me._text_1_5_1__text.innerHTML=me._text_1_5_1.ggText;
					if (me._text_1_5_1.ggUpdateText) {
					me._text_1_5_1.ggUpdateText=function() {
						var hs="Cookie Policy";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_5_1.ggUpdatePosition) me._text_1_5_1.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_5_1.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 90))
				)
			) {
				player.openUrl(player.getVariableValue('urlPre')+""+player.getVariableValue('ExtValueSu')+"\/cookie-policy\/","_blank");
			}
			if (
				(
					((player.getVariableValue('ExtValue') == 90))
				)
			) {
				player.openUrl("https:\/\/www.getac.com.cn\/zh\/cookie-policy\/","_blank");
			}
		}
		me._text_1_5_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer.appendChild(me._text_1_5_1);
		el=me._text_1_4_1=document.createElement('div');
		els=me._text_1_4_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_4_1";
		el.ggDx=0;
		el.ggDy=-3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 25px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="Term of Use<br\/>";
		el.appendChild(els);
		me._text_1_4_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_4_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_4_1.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_4_1.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_4_1.style[domTransition]='';
				if (me._text_1_4_1.ggCurrentLogicStateText == 0) {
					me._text_1_4_1.ggText="Conditions d\'utilisation ";
					me._text_1_4_1__text.innerHTML=me._text_1_4_1.ggText;
					if (me._text_1_4_1.ggUpdateText) {
					me._text_1_4_1.ggUpdateText=function() {
						var hs="Conditions d\'utilisation ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_1.ggUpdatePosition) me._text_1_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_1.ggCurrentLogicStateText == 1) {
					me._text_1_4_1.ggText="Nutzungsbedingungen ";
					me._text_1_4_1__text.innerHTML=me._text_1_4_1.ggText;
					if (me._text_1_4_1.ggUpdateText) {
					me._text_1_4_1.ggUpdateText=function() {
						var hs="Nutzungsbedingungen ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_1.ggUpdatePosition) me._text_1_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_1.ggCurrentLogicStateText == 2) {
					me._text_1_4_1.ggText="Termini d\'uso ";
					me._text_1_4_1__text.innerHTML=me._text_1_4_1.ggText;
					if (me._text_1_4_1.ggUpdateText) {
					me._text_1_4_1.ggUpdateText=function() {
						var hs="Termini d\'uso ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_1.ggUpdatePosition) me._text_1_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_1.ggCurrentLogicStateText == 3) {
					me._text_1_4_1.ggText="T\xe9rminos de uso ";
					me._text_1_4_1__text.innerHTML=me._text_1_4_1.ggText;
					if (me._text_1_4_1.ggUpdateText) {
					me._text_1_4_1.ggUpdateText=function() {
						var hs="T\xe9rminos de uso ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_1.ggUpdatePosition) me._text_1_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_4_1.ggCurrentLogicStateText == 4) {
					me._text_1_4_1.ggText="\u4f7f\u7528\u6761\u6b3e ";
					me._text_1_4_1__text.innerHTML=me._text_1_4_1.ggText;
					if (me._text_1_4_1.ggUpdateText) {
					me._text_1_4_1.ggUpdateText=function() {
						var hs="\u4f7f\u7528\u6761\u6b3e ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_1.ggUpdatePosition) me._text_1_4_1.ggUpdatePosition();
					}
				}
				else {
					me._text_1_4_1.ggText="Term of Use\n";
					me._text_1_4_1__text.innerHTML=me._text_1_4_1.ggText;
					if (me._text_1_4_1.ggUpdateText) {
					me._text_1_4_1.ggUpdateText=function() {
						var hs="Term of Use\n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_4_1.ggUpdatePosition) me._text_1_4_1.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_4_1.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 90))
				)
			) {
				player.openUrl(player.getVariableValue('urlPre')+""+player.getVariableValue('ExtValueSu')+"\/terms-of-use\/","_blank");
			}
			if (
				(
					((player.getVariableValue('ExtValue') == 90))
				)
			) {
				player.openUrl("https:\/\/www.getac.com.cn\/zh\/terms-of-use\/","_blank");
			}
		}
		me._text_1_4_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer.appendChild(me._text_1_4_1);
		el=me._text_1_3_1=document.createElement('div');
		els=me._text_1_3_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_3_1";
		el.ggDx=-175;
		el.ggDy=-3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 25px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="Privacy Policy";
		el.appendChild(els);
		me._text_1_3_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_3_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_3_1.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_3_1.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_3_1.style[domTransition]='';
				if (me._text_1_3_1.ggCurrentLogicStateText == 0) {
					me._text_1_3_1.ggText="Politique de Confidentialit\xe9 ";
					me._text_1_3_1__text.innerHTML=me._text_1_3_1.ggText;
					if (me._text_1_3_1.ggUpdateText) {
					me._text_1_3_1.ggUpdateText=function() {
						var hs="Politique de Confidentialit\xe9 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_1.ggUpdatePosition) me._text_1_3_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_1.ggCurrentLogicStateText == 1) {
					me._text_1_3_1.ggText="Datenschutzrichtlinie ";
					me._text_1_3_1__text.innerHTML=me._text_1_3_1.ggText;
					if (me._text_1_3_1.ggUpdateText) {
					me._text_1_3_1.ggUpdateText=function() {
						var hs="Datenschutzrichtlinie ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_1.ggUpdatePosition) me._text_1_3_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_1.ggCurrentLogicStateText == 2) {
					me._text_1_3_1.ggText="Informativa sulla Privacy ";
					me._text_1_3_1__text.innerHTML=me._text_1_3_1.ggText;
					if (me._text_1_3_1.ggUpdateText) {
					me._text_1_3_1.ggUpdateText=function() {
						var hs="Informativa sulla Privacy ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_1.ggUpdatePosition) me._text_1_3_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_1.ggCurrentLogicStateText == 3) {
					me._text_1_3_1.ggText="Pol\xedtica de privacidad ";
					me._text_1_3_1__text.innerHTML=me._text_1_3_1.ggText;
					if (me._text_1_3_1.ggUpdateText) {
					me._text_1_3_1.ggUpdateText=function() {
						var hs="Pol\xedtica de privacidad ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_1.ggUpdatePosition) me._text_1_3_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_3_1.ggCurrentLogicStateText == 4) {
					me._text_1_3_1.ggText="\u9690\u79c1\u653f\u7b56 ";
					me._text_1_3_1__text.innerHTML=me._text_1_3_1.ggText;
					if (me._text_1_3_1.ggUpdateText) {
					me._text_1_3_1.ggUpdateText=function() {
						var hs="\u9690\u79c1\u653f\u7b56 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_1.ggUpdatePosition) me._text_1_3_1.ggUpdatePosition();
					}
				}
				else {
					me._text_1_3_1.ggText="Privacy Policy";
					me._text_1_3_1__text.innerHTML=me._text_1_3_1.ggText;
					if (me._text_1_3_1.ggUpdateText) {
					me._text_1_3_1.ggUpdateText=function() {
						var hs="Privacy Policy";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_3_1.ggUpdatePosition) me._text_1_3_1.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_3_1.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 90))
				)
			) {
				player.openUrl(player.getVariableValue('urlPre')+""+player.getVariableValue('ExtValueSu')+"\/privacy-policy\/","_blank");
			}
			if (
				(
					((player.getVariableValue('ExtValue') == 90))
				)
			) {
				player.openUrl("https:\/\/www.getac.com.cn\/zh\/privacy-policy\/","_blank");
			}
		}
		me._text_1_3_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer.appendChild(me._text_1_3_1);
		el=me._text_1_2_1=document.createElement('div');
		els=me._text_1_2_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1_2_1";
		el.ggDx=-350;
		el.ggDy=-3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 190px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 190px;';
		hs+='height: 25px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="Imprint";
		el.appendChild(els);
		me._text_1_2_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1_2_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1_2_1.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1_2_1.ggCurrentLogicStateText = newLogicStateText;
				me._text_1_2_1.style[domTransition]='';
				if (me._text_1_2_1.ggCurrentLogicStateText == 0) {
					me._text_1_2_1.ggText="Mentions l\xe9gales ";
					me._text_1_2_1__text.innerHTML=me._text_1_2_1.ggText;
					if (me._text_1_2_1.ggUpdateText) {
					me._text_1_2_1.ggUpdateText=function() {
						var hs="Mentions l\xe9gales ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_1.ggUpdatePosition) me._text_1_2_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_1.ggCurrentLogicStateText == 1) {
					me._text_1_2_1.ggText="Impressum ";
					me._text_1_2_1__text.innerHTML=me._text_1_2_1.ggText;
					if (me._text_1_2_1.ggUpdateText) {
					me._text_1_2_1.ggUpdateText=function() {
						var hs="Impressum ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_1.ggUpdatePosition) me._text_1_2_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_1.ggCurrentLogicStateText == 2) {
					me._text_1_2_1.ggText="Imprint ";
					me._text_1_2_1__text.innerHTML=me._text_1_2_1.ggText;
					if (me._text_1_2_1.ggUpdateText) {
					me._text_1_2_1.ggUpdateText=function() {
						var hs="Imprint ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_1.ggUpdatePosition) me._text_1_2_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_1.ggCurrentLogicStateText == 3) {
					me._text_1_2_1.ggText="Pie de imprenta ";
					me._text_1_2_1__text.innerHTML=me._text_1_2_1.ggText;
					if (me._text_1_2_1.ggUpdateText) {
					me._text_1_2_1.ggUpdateText=function() {
						var hs="Pie de imprenta ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_1.ggUpdatePosition) me._text_1_2_1.ggUpdatePosition();
					}
				}
				else if (me._text_1_2_1.ggCurrentLogicStateText == 4) {
					me._text_1_2_1.ggText="\u7248\u6743\u6807\u8bb0 ";
					me._text_1_2_1__text.innerHTML=me._text_1_2_1.ggText;
					if (me._text_1_2_1.ggUpdateText) {
					me._text_1_2_1.ggUpdateText=function() {
						var hs="\u7248\u6743\u6807\u8bb0 ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_1.ggUpdatePosition) me._text_1_2_1.ggUpdatePosition();
					}
				}
				else {
					me._text_1_2_1.ggText="Imprint";
					me._text_1_2_1__text.innerHTML=me._text_1_2_1.ggText;
					if (me._text_1_2_1.ggUpdateText) {
					me._text_1_2_1.ggUpdateText=function() {
						var hs="Imprint";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1_2_1.ggUpdatePosition) me._text_1_2_1.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1_2_1.onclick=function (e) {
			player.openUrl(player.getVariableValue('urlPre')+"de\/impressum\/","_blank");
		}
		me._text_1_2_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu_footer.appendChild(me._text_1_2_1);
		me._footer.appendChild(me._menu_footer);
		me.divSkin.appendChild(me._footer);
		el=me._banner=document.createElement('div');
		el.ggId="banner";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 36px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 100%;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._banner.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._banner.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		el=me._banner_bg=document.createElement('div');
		el.ggId="banner_bg";
		el.ggDx=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : #d2451e;';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 36px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 100%;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._banner_bg.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._banner_bg.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._banner.appendChild(me._banner_bg);
		el=me._image_1=document.createElement('div');
		els=me._image_1__img=document.createElement('img');
		els.className='ggskin ggskin_image_1';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAAAeCAYAAAA4h6CKAAAFrElEQVRoge2a3XHbRhDHf9bwnerA6EBMBUKegodgTFcgdhCigkAVgK7AVAWmBy94M1hByApCVmCxguTh9sjjYfFxiGwno/xnOCJxh9u92+89vfll+SshqIoyAmJgJh+Ae2XqFqiTLM2DCLxiTIZOrIpyASyBu4Gv3AOrETy9WvQKoyrKJZAD08C1j0mWbsYw9VrRKgxxR2t0FzQE65HvvVqowqiKcgbUtFvDScZrYAcckiw9eO8flPf+Rwfe+AG8RxAnjMtaJ1n6/K2ZGwux6gUwB26TLI1+JD9DcWUZPYL4DCz+5UK4xSjLb87j7Y/hJhy+m9qgC+IpydLFt2JCDnHOJWW2Gdse4wY3A5OBNfDOe1aP5MVN3QG+YgS7cV3ywDUjzN5iIJJ1pxhPs5'+
			'Npq7ObqooyB35X1vqcZOk8hHggozkmZe7L1vYYy9y1TaiK8gC89R6/HyLIqihjjEUN2esTsOzzErJmzrAk6P1EXorQBXHE+N4Xh7jENcPrljugrooysofgxAYLXxAAc6HlYm21O/DALB4wGh5rg0JvFbDmMcnSjXVTy5ZJ+beIEQOytTZMMQK02rvkOj5oePB+H72uwBpdiH24r4pykWTp2n1YFeVc1mzb2xaTaR6cZzu4xAzNNI8+oZeA+OSaJrMnjDatHM2Pacaxd2IdB8ZZ7drhZc5FEHuhVQM7VwnFAjc0rdgevJ23AD4qNBt70zDxGHLR28qQd30XoGHn+O2P6IKI/XiQZGktNL5482ciqD7LOtKsd9bO9znG/+ddQTnJ0oMc9B/e0K394rgmH3vM3no9zIT2w6z7XqbbHF08Ahs5QM0KG4KwEIH4j2fiatb2'+
			'QUvwXvYE704heLjtGV/RPIvBggC4oSUIdWUtcDbJoT6/lr+5MvbYR6sP4vo06+5cNzBFjTrox+jBOqgua+tN7Qe8uxhKBNiJ3/UZtr70n0Kz7lNoPQDn+BDRVFL/N1yErSVAT6FKNkGXaK80kyyN/WfS4S28x8ckS5/F9/uYAl8VNxSKBi/0WIUL0eyFrBOSWVkafqEJI5Rs8H3GQGgaahl+ycLRVxaNbt23iAhhxfBax8dO1vBxGuN6b0Yy0YYuYWjaMxb+RqMBc65QFeUKk6W1CWIrnw/yaUAOPA6l3YaJEPRd1dg7DG1jdcf8x5F0/M1qdLvaJiuaxaKNXxtfq51WiYuuBmTdMdaKCSYPbxx+VZTzkJu6FnOFjkN5ifvxDjdx6JjvH2xfR7rL4l8MN5jKUkNbi6QNGsPH79ByDz0of19H+lPQOJBGX02i4ka0/6'+
			'SM3VdFuQ5Yq+9Qjv5gS4YVilBhxN7vzosySXW1eFfLX+1dn8Yg2ACet4w/VEX5SRhSURXlraS02sG6h1Ir40HWVxVlJAWeC00YXVoe2pzUPIfrBrXxu1BFq4py5t5n7OhO8bY0DzTmclGi4eckS2tZP6bZY4IBdwPS91liWguRN/aX8spjWzxS5h8x7ZVnb16E3hwE2Lp1VlWUNXpBu+iKu85FVg7kbp0xx2hy28HeKwS7YG/pgHOP6QPN4PmAuXP4QrPyj7kWtppiKlhUReneWcwxglxiDt8t7N4CB3HJzxh/H9OtmLX3e0mzEz0FPlVFuZcxV9gR1zeaAOf7DNuZjDHaMKa/76JN23P0jU4xytBn2lpV+5mmT38L/OlV9laQOc0295T2e5ETTQX1u8s7cdVa+/yO/qLyKcnS56uiT/LrGcM10Mce45rU7CTJ0uck'+
			'S2eE1xdbWfegjOXoCYiLcw9M7miG7O8IvKcZf64s3kLW/YlhfT2Xr0ckdjb+VcfC8WdLuiVrzbAOrEvs+nOMArjWaC/qa5QiTFkr4mJ17jpbjKU3MibxAkt5x2r+UehuuATmrxjrs7wcBuzN3Zd/dlaYdr0zX63CaCESOz8PQxj7L8Nmbt/r35P+BqnfXGBgIcXMAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Image 1";
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 4%;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 64px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._image_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._image_1.logicBlock_position = function() {
			var newLogicStatePosition;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._image_1.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._image_1.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._image_1.style[domTransition]='left 0s, top 0s';
				if (me._image_1.ggCurrentLogicStatePosition == 0) {
					me._image_1.style.left='1%';
					this.ggDy = 0;
					me._image_1.ggUpdatePosition(true);
				}
				else {
					me._image_1.style.left='4%';
					me._image_1.ggDy=0;
					me._image_1.ggUpdatePosition(true);
				}
			}
		}
		me._image_1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._banner_text_1=document.createElement('div');
		els=me._banner_text_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="banner_Text 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 16px;';
		hs+='left : 85px;';
		hs+='position : absolute;';
		hs+='top : 4px;';
		hs+='visibility : inherit;';
		hs+='width : 400px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 400px;';
		hs+='height: 16px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="TRANSPORT & LOGISTICS VIRTUAL EXHIBITION";
		el.appendChild(els);
		me._banner_text_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._banner_text_1.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getViewerSize().width < 640))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._banner_text_1.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._banner_text_1.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._banner_text_1.style[domTransition]='';
				if (me._banner_text_1.ggCurrentLogicStateVisible == 0) {
					me._banner_text_1.style.visibility="hidden";
					me._banner_text_1.ggVisible=false;
				}
				else {
					me._banner_text_1.style.visibility=(Number(me._banner_text_1.style.opacity)>0||!me._banner_text_1.style.opacity)?'inherit':'hidden';
					me._banner_text_1.ggVisible=true;
				}
			}
		}
		me._banner_text_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._banner_text_1.ggCurrentLogicStateText != newLogicStateText) {
				me._banner_text_1.ggCurrentLogicStateText = newLogicStateText;
				me._banner_text_1.style[domTransition]='';
				if (me._banner_text_1.ggCurrentLogicStateText == 0) {
					me._banner_text_1.ggText="EXPOSITION VIRTUELLE SUR LE TRANSPORT ET LA LOGISTIQUE";
					me._banner_text_1__text.innerHTML=me._banner_text_1.ggText;
					if (me._banner_text_1.ggUpdateText) {
					me._banner_text_1.ggUpdateText=function() {
						var hs="EXPOSITION VIRTUELLE SUR LE TRANSPORT ET LA LOGISTIQUE";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_1.ggUpdatePosition) me._banner_text_1.ggUpdatePosition();
					}
				}
				else if (me._banner_text_1.ggCurrentLogicStateText == 1) {
					me._banner_text_1.ggText="TRANSPORT & LOGISTIK VIRTUELLE MESSE";
					me._banner_text_1__text.innerHTML=me._banner_text_1.ggText;
					if (me._banner_text_1.ggUpdateText) {
					me._banner_text_1.ggUpdateText=function() {
						var hs="TRANSPORT & LOGISTIK VIRTUELLE MESSE";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_1.ggUpdatePosition) me._banner_text_1.ggUpdatePosition();
					}
				}
				else if (me._banner_text_1.ggCurrentLogicStateText == 2) {
					me._banner_text_1.ggText="FIERA VIRTUALE PER TRASPORTO E LOGISTICA";
					me._banner_text_1__text.innerHTML=me._banner_text_1.ggText;
					if (me._banner_text_1.ggUpdateText) {
					me._banner_text_1.ggUpdateText=function() {
						var hs="FIERA VIRTUALE PER TRASPORTO E LOGISTICA";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_1.ggUpdatePosition) me._banner_text_1.ggUpdatePosition();
					}
				}
				else if (me._banner_text_1.ggCurrentLogicStateText == 3) {
					me._banner_text_1.ggText="EXPOSICI\xd3N VIRTUAL DE TRANSPORTE Y LOG\xcdSTICA";
					me._banner_text_1__text.innerHTML=me._banner_text_1.ggText;
					if (me._banner_text_1.ggUpdateText) {
					me._banner_text_1.ggUpdateText=function() {
						var hs="EXPOSICI\xd3N VIRTUAL DE TRANSPORTE Y LOG\xcdSTICA";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_1.ggUpdatePosition) me._banner_text_1.ggUpdatePosition();
					}
				}
				else if (me._banner_text_1.ggCurrentLogicStateText == 4) {
					me._banner_text_1.ggText="\u8fd0\u8f93\u548c\u7269\u6d41\u865a\u62df\u5c55\u89c8";
					me._banner_text_1__text.innerHTML=me._banner_text_1.ggText;
					if (me._banner_text_1.ggUpdateText) {
					me._banner_text_1.ggUpdateText=function() {
						var hs="\u8fd0\u8f93\u548c\u7269\u6d41\u865a\u62df\u5c55\u89c8";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_1.ggUpdatePosition) me._banner_text_1.ggUpdatePosition();
					}
				}
				else {
					me._banner_text_1.ggText="TRANSPORT & LOGISTICS VIRTUAL EXHIBITION";
					me._banner_text_1__text.innerHTML=me._banner_text_1.ggText;
					if (me._banner_text_1.ggUpdateText) {
					me._banner_text_1.ggUpdateText=function() {
						var hs="TRANSPORT & LOGISTICS VIRTUAL EXHIBITION";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_1.ggUpdatePosition) me._banner_text_1.ggUpdatePosition();
					}
				}
			}
		}
		me._banner_text_1.ggUpdatePosition=function (useTransition) {
		}
		me._image_1.appendChild(me._banner_text_1);
		el=me._banner_text_2=document.createElement('div');
		els=me._banner_text_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="banner_Text 2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.75,sy:0.75 };
		el.ggVisible=false;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 32px;';
		hs+='left : 70px;';
		hs+='position : absolute;';
		hs+='top : -4px;';
		hs+='visibility : hidden;';
		hs+='width : 200px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='0% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 200px;';
		hs+='height: 32px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: left;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="SyntaxError: Parse error";
		el.appendChild(els);
		me._banner_text_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._banner_text_2.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getViewerSize().width < 640))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._banner_text_2.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._banner_text_2.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._banner_text_2.style[domTransition]='';
				if (me._banner_text_2.ggCurrentLogicStateVisible == 0) {
					me._banner_text_2.style.visibility=(Number(me._banner_text_2.style.opacity)>0||!me._banner_text_2.style.opacity)?'inherit':'hidden';
					me._banner_text_2.ggVisible=true;
				}
				else {
					me._banner_text_2.style.visibility="hidden";
					me._banner_text_2.ggVisible=false;
				}
			}
		}
		me._banner_text_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._banner_text_2.ggCurrentLogicStateText != newLogicStateText) {
				me._banner_text_2.ggCurrentLogicStateText = newLogicStateText;
				me._banner_text_2.style[domTransition]='';
				if (me._banner_text_2.ggCurrentLogicStateText == 0) {
					me._banner_text_2.ggText="EXPOSITION VIRTUELLE SUR LE<br>TRANSPORT ET LA LOGISTIQUE";
					me._banner_text_2__text.innerHTML=me._banner_text_2.ggText;
					if (me._banner_text_2.ggUpdateText) {
					me._banner_text_2.ggUpdateText=function() {
						var hs="EXPOSITION VIRTUELLE SUR LE<br>TRANSPORT ET LA LOGISTIQUE";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_2.ggUpdatePosition) me._banner_text_2.ggUpdatePosition();
					}
				}
				else if (me._banner_text_2.ggCurrentLogicStateText == 1) {
					me._banner_text_2.ggText="TRANSPORT & LOGISTIK<br>VIRTUELLE MESSE";
					me._banner_text_2__text.innerHTML=me._banner_text_2.ggText;
					if (me._banner_text_2.ggUpdateText) {
					me._banner_text_2.ggUpdateText=function() {
						var hs="TRANSPORT & LOGISTIK<br>VIRTUELLE MESSE";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_2.ggUpdatePosition) me._banner_text_2.ggUpdatePosition();
					}
				}
				else if (me._banner_text_2.ggCurrentLogicStateText == 2) {
					me._banner_text_2.ggText="FIERA VIRTUALE PER<br>TRASPORTO E LOGISTICA";
					me._banner_text_2__text.innerHTML=me._banner_text_2.ggText;
					if (me._banner_text_2.ggUpdateText) {
					me._banner_text_2.ggUpdateText=function() {
						var hs="FIERA VIRTUALE PER<br>TRASPORTO E LOGISTICA";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_2.ggUpdatePosition) me._banner_text_2.ggUpdatePosition();
					}
				}
				else if (me._banner_text_2.ggCurrentLogicStateText == 3) {
					me._banner_text_2.ggText="EXPOSICI\xd3N VIRTUAL DE<br>TRANSPORTE Y LOG\xcdSTICA";
					me._banner_text_2__text.innerHTML=me._banner_text_2.ggText;
					if (me._banner_text_2.ggUpdateText) {
					me._banner_text_2.ggUpdateText=function() {
						var hs="EXPOSICI\xd3N VIRTUAL DE<br>TRANSPORTE Y LOG\xcdSTICA";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_2.ggUpdatePosition) me._banner_text_2.ggUpdatePosition();
					}
				}
				else if (me._banner_text_2.ggCurrentLogicStateText == 4) {
					me._banner_text_2.ggText="\u8fd0\u8f93\u548c\u7269\u6d41\u865a\u62df\u5c55\u89c8";
					me._banner_text_2__text.innerHTML=me._banner_text_2.ggText;
					if (me._banner_text_2.ggUpdateText) {
					me._banner_text_2.ggUpdateText=function() {
						var hs="\u8fd0\u8f93\u548c\u7269\u6d41\u865a\u62df\u5c55\u89c8";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_2.ggUpdatePosition) me._banner_text_2.ggUpdatePosition();
					}
				}
				else {
					me._banner_text_2.ggText="TRANSPORT & LOGISTICS<br>VIRTUAL EXHIBITION ";
					me._banner_text_2__text.innerHTML=me._banner_text_2.ggText;
					if (me._banner_text_2.ggUpdateText) {
					me._banner_text_2.ggUpdateText=function() {
						var hs="TRANSPORT & LOGISTICS<br>VIRTUAL EXHIBITION ";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_text_2.ggUpdatePosition) me._banner_text_2.ggUpdatePosition();
					}
				}
			}
		}
		me._banner_text_2.ggUpdatePosition=function (useTransition) {
		}
		me._image_1.appendChild(me._banner_text_2);
		me._banner.appendChild(me._image_1);
		el=me._menu=document.createElement('div');
		el.ggId="menu";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 16px;';
		hs+='position : absolute;';
		hs+='right : 5%;';
		hs+='top : 12px;';
		hs+='visibility : inherit;';
		hs+='width : 150px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._menu.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._menu.logicBlock_position = function() {
			var newLogicStatePosition;
			if (
				((player.getViewerSize().width < 1080))
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._menu.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._menu.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._menu.style[domTransition]='right 0s, top 0s';
				if (me._menu.ggCurrentLogicStatePosition == 0) {
					me._menu.style.right='2%';
					me._menu.style.top='12px';
				}
				else {
					me._menu.style.right='5%';
					me._menu.style.top='12px';
				}
			}
		}
		me._menu.ggUpdatePosition=function (useTransition) {
		}
		el=me._banner_menu1=document.createElement('div');
		els=me._banner_menu1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="banner_menu1";
		el.ggDy=6;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 27px;';
		hs+='position : absolute;';
		hs+='right : 73px;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 90px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='right: 0px;';
		hs+='top:  0px;';
		hs+='width: 90px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: right;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="Website";
		el.appendChild(els);
		me._banner_menu1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._banner_menu1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._banner_menu1.ggCurrentLogicStateText != newLogicStateText) {
				me._banner_menu1.ggCurrentLogicStateText = newLogicStateText;
				me._banner_menu1.style[domTransition]='';
				if (me._banner_menu1.ggCurrentLogicStateText == 0) {
					me._banner_menu1.ggText="Site Internet";
					me._banner_menu1__text.innerHTML=me._banner_menu1.ggText;
					if (me._banner_menu1.ggUpdateText) {
					me._banner_menu1.ggUpdateText=function() {
						var hs="Site Internet";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_menu1.ggUpdatePosition) me._banner_menu1.ggUpdatePosition();
					}
				}
				else if (me._banner_menu1.ggCurrentLogicStateText == 1) {
					me._banner_menu1.ggText="Webseite";
					me._banner_menu1__text.innerHTML=me._banner_menu1.ggText;
					if (me._banner_menu1.ggUpdateText) {
					me._banner_menu1.ggUpdateText=function() {
						var hs="Webseite";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_menu1.ggUpdatePosition) me._banner_menu1.ggUpdatePosition();
					}
				}
				else if (me._banner_menu1.ggCurrentLogicStateText == 2) {
					me._banner_menu1.ggText="Sito Web";
					me._banner_menu1__text.innerHTML=me._banner_menu1.ggText;
					if (me._banner_menu1.ggUpdateText) {
					me._banner_menu1.ggUpdateText=function() {
						var hs="Sito Web";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_menu1.ggUpdatePosition) me._banner_menu1.ggUpdatePosition();
					}
				}
				else if (me._banner_menu1.ggCurrentLogicStateText == 3) {
					me._banner_menu1.ggText="P\xe1gina web";
					me._banner_menu1__text.innerHTML=me._banner_menu1.ggText;
					if (me._banner_menu1.ggUpdateText) {
					me._banner_menu1.ggUpdateText=function() {
						var hs="P\xe1gina web";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_menu1.ggUpdatePosition) me._banner_menu1.ggUpdatePosition();
					}
				}
				else if (me._banner_menu1.ggCurrentLogicStateText == 4) {
					me._banner_menu1.ggText="\u7f51\u7ad9";
					me._banner_menu1__text.innerHTML=me._banner_menu1.ggText;
					if (me._banner_menu1.ggUpdateText) {
					me._banner_menu1.ggUpdateText=function() {
						var hs="\u7f51\u7ad9";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_menu1.ggUpdatePosition) me._banner_menu1.ggUpdatePosition();
					}
				}
				else {
					me._banner_menu1.ggText="Website";
					me._banner_menu1__text.innerHTML=me._banner_menu1.ggText;
					if (me._banner_menu1.ggUpdateText) {
					me._banner_menu1.ggUpdateText=function() {
						var hs="Website";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._banner_menu1.ggUpdatePosition) me._banner_menu1.ggUpdatePosition();
					}
				}
			}
		}
		me._banner_menu1.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') == 0))
				)
			) {
				player.openUrl("https:\/\/www.getac.com\/en\/","_blank");
			}
			if (
				(
					((player.getVariableValue('ExtValue') == 18))
				)
			) {
				player.openUrl("https:\/\/www.getac.com\/fr\/","_blank");
			}
			player.openUrl(player.getVariableValue('urlPre')+""+player.getVariableValue('ExtValueSu')+"\/","_blank");
		}
		me._banner_menu1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu.appendChild(me._banner_menu1);
		el=me._banner_menu_=document.createElement('div');
		els=me._banner_menu___text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="banner_menu_";
		el.ggDy=6;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 27px;';
		hs+='position : absolute;';
		hs+='right : 58px;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 4px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='right: 0px;';
		hs+='top:  0px;';
		hs+='width: 4px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: right;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="|";
		el.appendChild(els);
		me._banner_menu_.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._banner_menu_.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu.appendChild(me._banner_menu_);
		el=me._menu_lan=document.createElement('div');
		el.ggId="menu_lan";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 5px;';
		hs+='border-radius : 5px;';
		hs+='background : #6c6c6c;';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 155px;';
		hs+='left : 89px;';
		hs+='position : absolute;';
		hs+='top : 25px;';
		hs+='visibility : hidden;';
		hs+='width : 70px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._menu_lan.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._menu_lan.onmouseover=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 108)) && 
					((player.getIsMobile() == false))
				)
			) {
				me._menu_lan.style[domTransition]='none';
				me._menu_lan.style.visibility=(Number(me._menu_lan.style.opacity)>0||!me._menu_lan.style.opacity)?'inherit':'hidden';
				me._menu_lan.ggVisible=true;
			}
		}
		me._menu_lan.onmouseout=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 108)) && 
					((player.getIsMobile() == false))
				)
			) {
				me._menu_lan.style[domTransition]='none';
				me._menu_lan.style.visibility='hidden';
				me._menu_lan.ggVisible=false;
			}
		}
		me._menu_lan.ggUpdatePosition=function (useTransition) {
		}
		el=me._txt_menu_cn=document.createElement('div');
		els=me._txt_menu_cn__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="txt_menu_cn";
		el.ggDx=1;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 130px;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 80px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="\u7b80\u4f53\u4e2d\u6587";
		el.appendChild(els);
		me._txt_menu_cn.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._txt_menu_cn.onclick=function (e) {
			me._menu_lan.style[domTransition]='none';
			me._menu_lan.style.visibility='hidden';
			me._menu_lan.ggVisible=false;
			me._txt_banner_menu_lan.ggText="\u7b80\u4f53\u4e2d\u6587";
			me._txt_banner_menu_lan.ggTextDiv.innerHTML=me._txt_banner_menu_lan.ggText;
			if (me._txt_banner_menu_lan.ggUpdateText) {
				me._txt_banner_menu_lan.ggUpdateText=function() {
					var hs="\u7b80\u4f53\u4e2d\u6587";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._txt_banner_menu_lan.ggUpdatePosition) {
				me._txt_banner_menu_lan.ggUpdatePosition();
			}
			me._txt_banner_menu_lan.ggTextDiv.scrollTop = 0;
			player.setVariableValue('ExtValue', Number("90"));
			player.setVariableValue('ExtValueSu', "zh");
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				me._mediapanoos.ggInitMedia("assets\/mp3\/pano\/"+player.getVariableValue('ExtValue')+"\/"+me.ggUserdata.customnodeid+".mp3");
			}
			player.setVariableValue('infoPano', Number("0"));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info2Pano', Number("1"));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info3Pano', Number("114"));
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
						me._mediapanoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
					me._mediapanoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-panoos");
			}
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility=(Number(me._text_1.style.opacity)>0||!me._text_1.style.opacity)?'inherit':'hidden';
			me._text_1.ggVisible=true;
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			history.replaceState({}, player.userdata.title,location.href.substring(0,location.href.search('lang')+5) + pano.getVariableValue('ExtValueSu'));
			if (player.transitionsDisabled) {
				me._banner_text_2.style[domTransition]='none';
			} else {
				me._banner_text_2.style[domTransition]='all 0ms ease-out 0ms';
			}
			me._banner_text_2.ggParameter.rx=0;me._banner_text_2.ggParameter.ry=5;
			me._banner_text_2.style[domTransform]=parameterToTransform(me._banner_text_2.ggParameter);
		}
		me._txt_menu_cn.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._menu_lan.appendChild(me._txt_menu_cn);
		el=me._txt_menu_es=document.createElement('div');
		els=me._txt_menu_es__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="txt_menu_es";
		el.ggDx=1;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 105px;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 80px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="Espa\xf1ol";
		el.appendChild(els);
		me._txt_menu_es.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._txt_menu_es.onclick=function (e) {
			me._menu_lan.style[domTransition]='none';
			me._menu_lan.style.visibility='hidden';
			me._menu_lan.ggVisible=false;
			me._txt_banner_menu_lan.ggText="Espa\xf1ol";
			me._txt_banner_menu_lan.ggTextDiv.innerHTML=me._txt_banner_menu_lan.ggText;
			if (me._txt_banner_menu_lan.ggUpdateText) {
				me._txt_banner_menu_lan.ggUpdateText=function() {
					var hs="Espa\xf1ol";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._txt_banner_menu_lan.ggUpdatePosition) {
				me._txt_banner_menu_lan.ggUpdatePosition();
			}
			me._txt_banner_menu_lan.ggTextDiv.scrollTop = 0;
			player.setVariableValue('ExtValue', Number("72"));
			player.setVariableValue('ExtValueSu', "es");
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				me._mediapanoos.ggInitMedia("assets\/mp3\/pano\/"+player.getVariableValue('ExtValue')+"\/"+me.ggUserdata.customnodeid+".mp3");
			}
			player.setVariableValue('infoPano', Number("0"));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info2Pano', Number("1"));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info3Pano', Number("113"));
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
						me._mediapanoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
					me._mediapanoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-panoos");
			}
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility=(Number(me._text_1.style.opacity)>0||!me._text_1.style.opacity)?'inherit':'hidden';
			me._text_1.ggVisible=true;
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			history.replaceState({}, player.userdata.title,location.href.substring(0,location.href.search('lang')+5) + pano.getVariableValue('ExtValueSu'));
			if (player.transitionsDisabled) {
				me._banner_text_2.style[domTransition]='none';
			} else {
				me._banner_text_2.style[domTransition]='all 0ms ease-out 0ms';
			}
			me._banner_text_2.ggParameter.rx=0;me._banner_text_2.ggParameter.ry=0;
			me._banner_text_2.style[domTransform]=parameterToTransform(me._banner_text_2.ggParameter);
		}
		me._txt_menu_es.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._menu_lan.appendChild(me._txt_menu_es);
		el=me._txt_menu_it=document.createElement('div');
		els=me._txt_menu_it__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="txt_menu_it";
		el.ggDx=1;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 80px;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 80px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="Italiano<br\/>";
		el.appendChild(els);
		me._txt_menu_it.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._txt_menu_it.onclick=function (e) {
			me._menu_lan.style[domTransition]='none';
			me._menu_lan.style.visibility='hidden';
			me._menu_lan.ggVisible=false;
			me._txt_banner_menu_lan.ggText="Italiano";
			me._txt_banner_menu_lan.ggTextDiv.innerHTML=me._txt_banner_menu_lan.ggText;
			if (me._txt_banner_menu_lan.ggUpdateText) {
				me._txt_banner_menu_lan.ggUpdateText=function() {
					var hs="Italiano";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._txt_banner_menu_lan.ggUpdatePosition) {
				me._txt_banner_menu_lan.ggUpdatePosition();
			}
			me._txt_banner_menu_lan.ggTextDiv.scrollTop = 0;
			player.setVariableValue('ExtValue', Number("54"));
			player.setVariableValue('ExtValueSu', "it");
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				me._mediapanoos.ggInitMedia("assets\/mp3\/pano\/"+player.getVariableValue('ExtValue')+"\/"+me.ggUserdata.customnodeid+".mp3");
			}
			player.setVariableValue('infoPano', Number("0"));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info2Pano', Number("1"));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info3Pano', Number("112"));
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
						me._mediapanoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
					me._mediapanoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-panoos");
			}
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility=(Number(me._text_1.style.opacity)>0||!me._text_1.style.opacity)?'inherit':'hidden';
			me._text_1.ggVisible=true;
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			history.replaceState({}, player.userdata.title,location.href.substring(0,location.href.search('lang')+5) + pano.getVariableValue('ExtValueSu'));
			if (player.transitionsDisabled) {
				me._banner_text_2.style[domTransition]='none';
			} else {
				me._banner_text_2.style[domTransition]='all 0ms ease-out 0ms';
			}
			me._banner_text_2.ggParameter.rx=0;me._banner_text_2.ggParameter.ry=0;
			me._banner_text_2.style[domTransform]=parameterToTransform(me._banner_text_2.ggParameter);
		}
		me._txt_menu_it.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._menu_lan.appendChild(me._txt_menu_it);
		el=me._txt_menu_de=document.createElement('div');
		els=me._txt_menu_de__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="txt_menu_de";
		el.ggDx=1;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 55px;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 80px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="Deutsch";
		el.appendChild(els);
		me._txt_menu_de.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._txt_menu_de.onclick=function (e) {
			me._menu_lan.style[domTransition]='none';
			me._menu_lan.style.visibility='hidden';
			me._menu_lan.ggVisible=false;
			me._txt_banner_menu_lan.ggText="Deutsch";
			me._txt_banner_menu_lan.ggTextDiv.innerHTML=me._txt_banner_menu_lan.ggText;
			if (me._txt_banner_menu_lan.ggUpdateText) {
				me._txt_banner_menu_lan.ggUpdateText=function() {
					var hs="Deutsch";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._txt_banner_menu_lan.ggUpdatePosition) {
				me._txt_banner_menu_lan.ggUpdatePosition();
			}
			me._txt_banner_menu_lan.ggTextDiv.scrollTop = 0;
			player.setVariableValue('ExtValue', Number("36"));
			player.setVariableValue('ExtValueSu', "de");
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				me._mediapanoos.ggInitMedia("assets\/mp3\/pano\/"+player.getVariableValue('ExtValue')+"\/"+me.ggUserdata.customnodeid+".mp3");
			}
			player.setVariableValue('infoPano', Number("0"));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info2Pano', Number("1"));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info3Pano', Number("111"));
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
						me._mediapanoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
					me._mediapanoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-panoos");
			}
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility=(Number(me._text_1.style.opacity)>0||!me._text_1.style.opacity)?'inherit':'hidden';
			me._text_1.ggVisible=true;
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			history.replaceState({}, player.userdata.title,location.href.substring(0,location.href.search('lang')+5) + pano.getVariableValue('ExtValueSu'));
			if (player.transitionsDisabled) {
				me._banner_text_2.style[domTransition]='none';
			} else {
				me._banner_text_2.style[domTransition]='all 0ms ease-out 0ms';
			}
			me._banner_text_2.ggParameter.rx=0;me._banner_text_2.ggParameter.ry=0;
			me._banner_text_2.style[domTransform]=parameterToTransform(me._banner_text_2.ggParameter);
		}
		me._txt_menu_de.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._menu_lan.appendChild(me._txt_menu_de);
		el=me._txt_menu_fr=document.createElement('div');
		els=me._txt_menu_fr__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="txt_menu_fr";
		el.ggDx=1;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 30px;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 80px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="Fran\xe7ais<br\/>";
		el.appendChild(els);
		me._txt_menu_fr.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._txt_menu_fr.onclick=function (e) {
			me._menu_lan.style[domTransition]='none';
			me._menu_lan.style.visibility='hidden';
			me._menu_lan.ggVisible=false;
			me._txt_banner_menu_lan.ggText="Fran\xe7ais";
			me._txt_banner_menu_lan.ggTextDiv.innerHTML=me._txt_banner_menu_lan.ggText;
			if (me._txt_banner_menu_lan.ggUpdateText) {
				me._txt_banner_menu_lan.ggUpdateText=function() {
					var hs="Fran\xe7ais";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._txt_banner_menu_lan.ggUpdatePosition) {
				me._txt_banner_menu_lan.ggUpdatePosition();
			}
			me._txt_banner_menu_lan.ggTextDiv.scrollTop = 0;
			player.setVariableValue('ExtValue', Number("18"));
			player.setVariableValue('ExtValueSu', "fr");
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				me._mediapanoos.ggInitMedia("assets\/mp3\/pano\/"+player.getVariableValue('ExtValue')+"\/"+me.ggUserdata.customnodeid+".mp3");
			}
			player.setVariableValue('infoPano', Number("0"));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info2Pano', Number("1"));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info3Pano', Number("110"));
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
						me._mediapanoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
					me._mediapanoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-panoos");
			}
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility=(Number(me._text_1.style.opacity)>0||!me._text_1.style.opacity)?'inherit':'hidden';
			me._text_1.ggVisible=true;
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			history.replaceState({}, player.userdata.title,location.href.substring(0,location.href.search('lang')+5) + pano.getVariableValue('ExtValueSu'));
			if (player.transitionsDisabled) {
				me._banner_text_2.style[domTransition]='none';
			} else {
				me._banner_text_2.style[domTransition]='all 0ms ease-out 0ms';
			}
			me._banner_text_2.ggParameter.rx=0;me._banner_text_2.ggParameter.ry=0;
			me._banner_text_2.style[domTransform]=parameterToTransform(me._banner_text_2.ggParameter);
		}
		me._txt_menu_fr.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._menu_lan.appendChild(me._txt_menu_fr);
		el=me._txt_menu_en=document.createElement('div');
		els=me._txt_menu_en__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="txt_menu_en";
		el.ggDx=1;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : 5px;';
		hs+='visibility : inherit;';
		hs+='width : 80px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 80px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="English";
		el.appendChild(els);
		me._txt_menu_en.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._txt_menu_en.onclick=function (e) {
			me._menu_lan.style[domTransition]='none';
			me._menu_lan.style.visibility='hidden';
			me._menu_lan.ggVisible=false;
			me._txt_banner_menu_lan.ggText="English";
			me._txt_banner_menu_lan.ggTextDiv.innerHTML=me._txt_banner_menu_lan.ggText;
			if (me._txt_banner_menu_lan.ggUpdateText) {
				me._txt_banner_menu_lan.ggUpdateText=function() {
					var hs="English";
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (me._txt_banner_menu_lan.ggUpdatePosition) {
				me._txt_banner_menu_lan.ggUpdatePosition();
			}
			me._txt_banner_menu_lan.ggTextDiv.scrollTop = 0;
			player.setVariableValue('ExtValue', Number("0"));
			player.setVariableValue('ExtValueSu', "en");
			if (
				(
					((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
				)
			) {
				me._mediapanoos.ggInitMedia("assets\/mp3\/pano\/"+player.getVariableValue('ExtValue')+"\/"+me.ggUserdata.customnodeid+".mp3");
			}
			player.setVariableValue('infoPano', Number("0"));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('infoPano', player.getVariableValue('infoPano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info2Pano', Number("1"));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('ExtValue')));
			player.setVariableValue('info2Pano', player.getVariableValue('info2Pano') + Number(player.getVariableValue('infoNo')));
			player.setVariableValue('info3Pano', Number("109"));
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
						me._mediapanoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
					me._mediapanoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-panoos");
			}
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility=(Number(me._text_1.style.opacity)>0||!me._text_1.style.opacity)?'inherit':'hidden';
			me._text_1.ggVisible=true;
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			history.replaceState({}, player.userdata.title,location.href.substring(0,location.href.search('lang')+5) + pano.getVariableValue('ExtValueSu'));
			if (player.transitionsDisabled) {
				me._banner_text_2.style[domTransition]='none';
			} else {
				me._banner_text_2.style[domTransition]='all 0ms ease-out 0ms';
			}
			me._banner_text_2.ggParameter.rx=0;me._banner_text_2.ggParameter.ry=0;
			me._banner_text_2.style[domTransform]=parameterToTransform(me._banner_text_2.ggParameter);
		}
		me._txt_menu_en.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
			}
		}
		me._menu_lan.appendChild(me._txt_menu_en);
		me._menu.appendChild(me._menu_lan);
		el=me._txt_banner_menu_lan=document.createElement('div');
		els=me._txt_banner_menu_lan__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="txt_banner_menu_lan";
		el.ggDy=5;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 27px;';
		hs+='position : absolute;';
		hs+='right : 0px;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='right: 0px;';
		hs+='top:  0px;';
		hs+='width: 50px;';
		hs+='height: auto;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: right;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		hs+="font-size: 12px;";
		els.setAttribute('style',hs);
		els.innerHTML="English";
		el.appendChild(els);
		me._txt_banner_menu_lan.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._txt_banner_menu_lan.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getVariableValue('ExtValue') == 108))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._txt_banner_menu_lan.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._txt_banner_menu_lan.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._txt_banner_menu_lan.style[domTransition]='';
				if (me._txt_banner_menu_lan.ggCurrentLogicStateVisible == 0) {
					me._txt_banner_menu_lan.style.visibility="hidden";
					me._txt_banner_menu_lan.ggVisible=false;
				}
				else {
					me._txt_banner_menu_lan.style.visibility=(Number(me._txt_banner_menu_lan.style.opacity)>0||!me._txt_banner_menu_lan.style.opacity)?'inherit':'hidden';
					me._txt_banner_menu_lan.ggVisible=true;
				}
			}
		}
		me._txt_banner_menu_lan.onclick=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 108)) && 
					((player.getIsMobile() == true))
				)
			) {
				me._menu_lan.ggVisible = !me._menu_lan.ggVisible;
				var flag=me._menu_lan.ggVisible;
				me._menu_lan.style[domTransition]='none';
				me._menu_lan.style.visibility=((flag)&&(Number(me._menu_lan.style.opacity)>0||!me._menu_lan.style.opacity))?'inherit':'hidden';
			}
		}
		me._txt_banner_menu_lan.onmouseover=function (e) {
			if (
				(
					((player.getVariableValue('ExtValue') != 108)) && 
					((player.getIsMobile() == false))
				)
			) {
				me._menu_lan.style[domTransition]='none';
				me._menu_lan.style.visibility=(Number(me._menu_lan.style.opacity)>0||!me._menu_lan.style.opacity)?'inherit':'hidden';
				me._menu_lan.ggVisible=true;
			}
		}
		me._txt_banner_menu_lan.onmouseout=function (e) {
			if (e && e.toElement) {
				var current = e.toElement;
				while (current = current.parentNode) {
				if (current == me._txt_banner_menu_lan__text)
					return;
				}
			}
			if (
				(
					((player.getVariableValue('ExtValue') != 108)) && 
					((player.getIsMobile() == false))
				)
			) {
				me._menu_lan.style[domTransition]='none';
				me._menu_lan.style.visibility='hidden';
				me._menu_lan.ggVisible=false;
			}
		}
		me._txt_banner_menu_lan.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._menu.appendChild(me._txt_banner_menu_lan);
		me._banner.appendChild(me._menu);
		me.divSkin.appendChild(me._banner);
		el=me._ctn_timer=document.createElement('div');
		el.ggId="Ctn_Timer";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 89px;';
		hs+='position : absolute;';
		hs+='right : 21px;';
		hs+='top : 18px;';
		hs+='visibility : inherit;';
		hs+='width : 97px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ctn_timer.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._ctn_timer.ggUpdatePosition=function (useTransition) {
		}
		el=me._timer_maphide=document.createElement('div');
		el.ggTimestamp=this.ggCurrentTime;
		el.ggLastIsActive=true;
		el.ggTimeout=2000;
		el.ggId="Timer_MapHide";
		el.ggDy=35;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_timer ";
		el.ggType='timer';
		hs ='';
		hs+='height : 20px;';
		hs+='left : -2px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 100px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._timer_maphide.ggIsActive=function() {
			return (me._timer_maphide.ggTimestamp + me._timer_maphide.ggTimeout) >= me.ggCurrentTime;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._timer_maphide.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me._timer_maphide.ggIsActive() == false))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._timer_maphide.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._timer_maphide.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._timer_maphide.style[domTransition]='';
				if (me._timer_maphide.ggCurrentLogicStateVisible == 0) {
					me._timer_maphide.style.visibility="hidden";
					me._timer_maphide.ggVisible=false;
				}
				else {
					me._timer_maphide.style.visibility=(Number(me._timer_maphide.style.opacity)>0||!me._timer_maphide.style.opacity)?'inherit':'hidden';
					me._timer_maphide.ggVisible=true;
				}
			}
		}
		me._timer_maphide.ggDeactivate=function () {
			if (
				(
					((player.getViewerSize().width < 1080))
				)
			) {
				me._image_arrowwrapper_l.onclick.call(me._image_arrowwrapper_l);
			}
		}
		me._timer_maphide.ggCurrentLogicStateVisible = -1;
		me._timer_maphide.ggUpdateConditionTimer=function () {
			me._timer_maphide.logicBlock_visible();
		}
		me._timer_maphide.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._ctn_timer.appendChild(me._timer_maphide);
		el=me._timer_flashing=document.createElement('div');
		el.ggTimestamp=this.ggCurrentTime;
		el.ggLastIsActive=true;
		el.ggTimeout=500;
		el.ggId="Timer_Flashing";
		el.ggDy=12;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_timer ";
		el.ggType='timer';
		hs ='';
		hs+='height : 20px;';
		hs+='left : -2px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 100px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._timer_flashing.ggIsActive=function() {
			return (me._timer_flashing.ggTimestamp==0 ? false : (Math.floor((me.ggCurrentTime - me._timer_flashing.ggTimestamp) / me._timer_flashing.ggTimeout) % 2 == 0));
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._timer_flashing.ggActivate=function () {
			player.setVariableValue('ht_anima_B', !player.getVariableValue('ht_anima_B'));
		}
		me._timer_flashing.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._ctn_timer.appendChild(me._timer_flashing);
		el=me._timer_direction=document.createElement('div');
		el.ggTimestamp=this.ggCurrentTime;
		el.ggLastIsActive=true;
		el.ggTimeout=250;
		el.ggId="Timer_Direction";
		el.ggDy=-11;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_timer ";
		el.ggType='timer';
		hs ='';
		hs+='height : 20px;';
		hs+='left : -2px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 100px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._timer_direction.ggIsActive=function() {
			return (me._timer_direction.ggTimestamp==0 ? false : (Math.floor((me.ggCurrentTime - me._timer_direction.ggTimestamp) / me._timer_direction.ggTimeout) % 2 == 0));
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._timer_direction.ggActivate=function () {
			player.setVariableValue('ht_anima_A', !player.getVariableValue('ht_anima_A'));
		}
		me._timer_direction.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._ctn_timer.appendChild(me._timer_direction);
		el=me._timer_arrowdouble=document.createElement('div');
		el.ggTimestamp=this.ggCurrentTime;
		el.ggLastIsActive=true;
		el.ggTimeout=250;
		el.ggId="Timer_ArrowDouble";
		el.ggDy=-34;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_timer ";
		el.ggType='timer';
		hs ='';
		hs+='height : 20px;';
		hs+='left : -2px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 100px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._timer_arrowdouble.ggIsActive=function() {
			return (me._timer_arrowdouble.ggTimestamp==0 ? false : (Math.floor((me.ggCurrentTime - me._timer_arrowdouble.ggTimestamp) / me._timer_arrowdouble.ggTimeout) % 2 == 0));
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._timer_arrowdouble.ggActivate=function () {
			player.setVariableValue('ht_anima_C', !player.getVariableValue('ht_anima_C'));
		}
		me._timer_arrowdouble.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._ctn_timer.appendChild(me._timer_arrowdouble);
		me.divSkin.appendChild(me._ctn_timer);
		el=me._ctn_ht=document.createElement('div');
		el.ggId="Ctn_ht";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 420px;';
		hs+='position : absolute;';
		hs+='top : 300px;';
		hs+='visibility : inherit;';
		hs+='width : 100px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ctn_ht.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._ctn_ht.ggUpdatePosition=function (useTransition) {
		}
		me.divSkin.appendChild(me._ctn_ht);
		el=me._map=document.createElement('div');
		el.ggId="Map";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='z-index: 99;';
		hs+='bottom : 0px;';
		hs+='height : 281px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='visibility : inherit;';
		hs+='width : 293px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._map.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._map.logicBlock_position = function() {
			var newLogicStatePosition;
			if (
				((player.getVariableValue('map_open') == false))
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._map.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._map.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._map.style[domTransition]='left 500ms ease 0ms, bottom 500ms ease 0ms';
				if (me._map.ggCurrentLogicStatePosition == 0) {
					me._map.style.left='-298px';
					me._map.style.bottom='0px';
				}
				else {
					me._map.style.left='0px';
					me._map.style.bottom='0px';
				}
			}
		}
		me._map.ggUpdatePosition=function (useTransition) {
		}
		el=me._map_all=document.createElement('div');
		el.ggFilter = '';
		el.ggFilteredIds = [];
		el.ggMapNotLoaded = true;
		el.ggId="Map_All";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_map ";
		el.ggType='map';
		hs ='';
		hs+='background : #ffffff;';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 281px;';
		hs+='left : -10000px;';
		hs+='overflow : hidden;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 293px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._map_all.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._map_all.ggUpdateConditionResize=function () {
			var mapDetails = player.getMapDetails(me._map_all.ggMapId);
			if (!(mapDetails.hasOwnProperty('title'))) return;
			me._map_all.ggCalculateFloorplanSize(mapDetails);
			me._map_all.ggShowSimpleFloorplan(mapDetails);
			me._map_all.ggPlaceMarkersOnSimpleFloorplan();
		}
		me._map_all.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			me._map_all.ggUpdateConditionResize();
		}
		me._map_all.ggNodeChange=function () {
			var mapDetails = player.getMapDetails(me._map_all.ggMapId);
			if (!(mapDetails.hasOwnProperty('title'))) return;
			me._map_all.ggCalculateFloorplanSize(mapDetails);
			me._map_all.ggShowSimpleFloorplan(mapDetails);
			me._map_all.ggPlaceMarkersOnSimpleFloorplan();
			if (me._map_all.ggLastNodeId) {
				var lastActiveMarker = me._map_all.ggSimpleFloorplanMarkerArray[me._map_all.ggLastNodeId];
				if (lastActiveMarker && lastActiveMarker.ggDeactivate) lastActiveMarker.ggDeactivate();
			}
			var id = player.getCurrentNode();
			var marker = me._map_all.ggSimpleFloorplanMarkerArray[id];
			if (marker) {
				if (marker.ggActivate) marker.ggActivate();
			}
			if (player.getMapType(me._map_all.ggMapId) == 'file') {
				var coords = player.getNodeMapCoords(id, me._map_all.ggMapId);
				if (coords.length < 2) {
					var mapId = player.getMapContainingNode(id);
					if (mapId != '') {
							me._map_all.ggChangeMap(mapId);
					}
				}
			}
			me._map_all.ggLastNodeId = id;
		}
		me._map.appendChild(me._map_all);
		el=me._image_arrow_r=document.createElement('div');
		els=me._image_arrow_r__img=document.createElement('img');
		els.className='ggskin ggskin_image_arrow_r';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAhCAYAAADUIPtUAAABVklEQVRIiaXWLUgEQRjG8WfVJCJqEg7RYBPhkEsWo6jJYrhwWRCMgk0wG4wmDQYRo81g8JpwoKBJg/jRXOWC2P6Gc2Fdbl7f2XtgYJmd/fHOwMysgCYwAaiXlgBISiVVJT2rZPoktSWNSbqRNNsLlPw+j0pqSaqVhfIZkHQtaSVaIpx6zGILSA1sMwaaBt4MbMcLCRgH7gxs3wsJ6AcuDOzYC2XtwMDOYyABWwZ2BQx5IQEbBnYPjHghAasG9gRMeiEBc8BLAPsAql5IwCBwZlS3XtxroXxJejDeT3mqSYBTo5q6Z2oV4NZAlrOxFlID3gPANzCfHx9CFowqUmCm+E03pGEgjwQuimLHroG0gOHADP5ARwZySed0+Hf3W8fHiQXkoaaBHHqQDAplz4'+
			'tkULsLsh2DhKBGLJJBnzlkrQxSXKPFski+oqVeEOj81lQkvUbf9YX8AMwbZaQSeX3yAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Image_arrow_R";
		el.ggDx=163;
		el.ggDy=3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 21px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 12px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._image_arrow_r.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._image_arrow_r.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._map.appendChild(me._image_arrow_r);
		el=me._image_arrow_l=document.createElement('div');
		els=me._image_arrow_l__img=document.createElement('img');
		els.className='ggskin ggskin_image_arrow_l';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAhCAYAAADUIPtUAAABUUlEQVRIia3WIUgEQRTG8W9Pk4jIJeE4NNhEELlkMYqaLAaD+UAwCjbBbDCaNAiKGG0XDAoGQVTQpEEQm+dxQWx/w7KwLDtvZ3fuwQuzzP72vVkeTAQoMJqSzoYHgDxKqtcCkFlJT5LqkvpVK2pJupOUvB9VqWhV0n0KiQMokxs4ogyy7UKAri+yZyBfwLQPcmggL8AEUHhGpwbSAYaSvRZyZSBH2f15wChwYyA7eR/OPhgHXg1ky9VBejEJfBjImgtJQ3PAjwP4BOYtJIHaRhWXwEgRAqgmacqYqzdJv14TSMEMARdA5NNakisG9gw0fCEBC8CfA/sGWr6QgBmga1S36AsJaALvBrbpCwkYAx4MbN8XEvF0XxvYiS+U5LmBdcpAAo4N7LYMJODAJZ'+
			'WFBOzmOP0qkIh//0AgAespqBcCCVgKOaNsLgO9CILvR5LU+AeSECzJvaaSmAAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Image_arrow_L";
		el.ggDx=162;
		el.ggDy=3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 21px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 12px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._image_arrow_l.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._image_arrow_l.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._map.appendChild(me._image_arrow_l);
		el=me._image_arrowwrapper_l=document.createElement('div');
		els=me._image_arrowwrapper_l__img=document.createElement('img');
		els.className='ggskin ggskin_image_arrowwrapper_l';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAE2CAYAAADiXou/AAANRElEQVR4nO3d21MTZ+PA8W+ySUhCQoAAWk7h4AHQAtLX/lqtI06dcXRGep7e2Ks6Tq/6x/Sq49iretPpydIZHWfsqLVaf68tBaqCVo4BikDEkJAsSZa8F4uYKIdkyWb3Yr9XHkL68dnsk+d5kpmaksmkBzAhFwIkdJIZcF6+fLklGAxWAoLWoNTMgLS4uNghSZIPsGoNSs0MPB0eHt6WTCZrAYvWoNTMQOzXX391J5PJCsCmNSg1M4Df798WiUSSQInGnrTMAKIouvx+P0AV4NRUlJL52S/u3LljDwQCuwCXhp60UnHF4XC4HD2O3P3791958uQJQLF2nPRWcaIo2vx+vxnYBhRqR3qeOfU3/f399kAgUA+UauRJKw139+5dz/T0dBl6xD148KBieH'+
			'jYDHg18qSVhhNF0f7PP//YkEeuSBvS88wv/sHw8LBzbm6uDnlC1rSXcI8ePfL09/c7gEoNPGm9hJuYmPD29PTYgQoNPGm9hJMkSRgYGHABbjS+tC/hAGZmZhyDg4MVQH2ePWmtiZudnXVdv37dBtTm2ZPWmrhgMOi+ffu2BygDUjdAeW1NHMDo6Gjx3NycJ5FI7ESjvcW6uEgkUnDt2jWzKIot6A0XjUZtly5dKkkmkz40WuOti4vH49a///77lfn5+STylJL30VsXBxCJRBy9vb3mcDj8Khq8126IW15epru727uwsOBDnpTz2oY4gD///LPq8ePHAK+Q5+OKDXHJZNIkimJBb2+vdWlpqREoz5MLyGDkAH777bfiiYmJOvK8GMgId+fOnZrBwUEzsJ08vltkhIvH49a+vj67JEk1gE9l02oZ4QB6enpK7t69ux09'+
			'4h4+fLjtxo0bduQVcl4m5Ixx8Xjc+tdff7ljsVg5sFNF02oZ4wD8fr/nxo0bHmCPSp60ssJNTEx4v//++xLku7Yi25/PtqyePJlMmgYHB71jY2MeSZLaUPmYNut/eSgUcnzzzTeOWCy2B3CoYFota9zi4qL9ypUrlZFIxAU0ouI5cta4ZDJpmpqaKrl+/bo5Eom8jrzHUCWlL2jz+fPnK+fm5sqQbw5VUoSTJMk0MDBQde/ePRPQhEqrlS1NBT///HPJ+Pj4blQ6V9kS7ubNm/V9fX1m5Bsj56+9LeFisZjll19+KZqbm9sD7M6RabUtz/C3bt2quXz5sgA0kOMt5JZxoVDIefXq1ZKFhYVGoC0HptVy8t54//798h9++MGKfOcWkqPVck5w8/Pz7osXL1aEQqFqSZL+Q47ec3O2qhgbGyv9+uuvrZIkdZCjjwpyhl'+
			'tYWCj89ttvfZOTk4XAfnJwQpDT9VggECg6d+6cOxAI7ANq2OJrL6e4RCIhXLx4cUdPTw+SJL2OfEqguJziVk4IbOfOnds+NDRUBbQABUqfT5Vldn9/f+2FCxcc0Wj0NWSgolTbA3R3d/suX75cAOxF4apFNVwgEPB899135ePj4z7g/5Q8h6q7p/7+/uqzZ8+6kPe57WR5hKYqLh6PW65du1bd3d3tTiQSbyJPzhlPL6riQL68Z8+erZmamvJKkvQGWXxErzoOwO/3l509e9YVDofbkZdWGV3evOBisZjlwoULTTdv3iSRSBwiww/88oID+dPIL774om5gYKAEee7b9PLmDQcwNjZW8eOPPxaGQqH/ALs2e3xecQAXL16su3LlioA8ehsurfKOCwaDrp9++sk7PT1dD+zb6LF5xwH09fVVnT9/3g7sYINDcE1woija'+
			'rl27tm1oaKh8Ze7TDw5gcnKy9Msvv/Qgj17VWhbNcKIo2n7//ffKwcFBezwef5019rya4UDe83711VfFkUikhTW+oqQpLhaLWa5evVo3OjqaBJp5YVOkKQ5AFMWC7u5uZyAQ2M8Lo6c5DuDSpUvPRq+SFJMucPPz8+6enh6bJEl1yN98BHSCA7h165ZnZGSknpSDSN3gent7q3t7ewVSNkO6wYmiaBsYGChAvinKQUc4gKGhIdf4+PjqpdUVbnx83NPf329i5abQFW5mZqZ45VuPpaAznCRJ5pGRESfyZ2oOXeEAgsGgLRgMeoAy3eHC4bB15QCyWHe4aDRqGxsbMwNu3eGWlpasU1NTFqBQd7h4PG6ZmZmxAk7d4WKxmOXJkydWwK47nCRJwsLCggWw6RFnXlxctAJW3eEAotGoBb3ilpaWBMCsS1wikTADgoHLtk'+
			'QiIYDOViUvZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUZuCUpkucxWKRQL+4ZUAycNlWUFAgAcu6xDkcjgQQ1x1OEITlwsLCODrFSUVFRQkgpjuczWZLlJaWxgFRdzir1ZqoqKiIAxHd4QoKCuKVlZUJYFF3OIfDEfP5fMtASHc4l8sVr6qqWgSe6g7n8XhiHo8nCMzpCicIwnJ9fX0EiAJRXeEqKiqetra2RoEnoLNVSW1tbbC1tTUJPAad4Rob'+
			'G8O1tbUjwBToCGe322PNzc1LQACYBR3h2tvbJ9rb2yVWYKAj3IEDB4L19fWrlxR0gispKQl1dHTEBEEYZeVmAJ3gjh8/PlpXV2dCHrXlZ3+u6v8tOZPsdvtSV1dXxOv1/oF8M6ym6cjZbLbEkSNHno3aALCQ+veajpzb7Y58+umnT51O5wAvjBpoiLPb7bE333xzqqmpSRQE4b9A5MXHaHZZq6qqnnz22WdB4BEwScqN8CxNRs5ut8c6OzsfNzY2zgK3geRaj9ME19bWNnnq1CkRedSm13tc3i+rx+MJv/POO4Ht27ePAH9t9Ni8406cODF69OhRCbjPyrptvfKK8/l8M++9996i2+3+A3i42ePzhhMEQfr8889Hm5ub55FHbXGzn8nLDWGz2RInT558cPDgQSwWyw3kqWPT8oKrqamZO3PmTNjlcvUBw4CUyc+pfl'+
			'm9Xm/wzJkz/srKyoAgCLfJ4HI+S9WRs1qtic7Ozomurq4Q8Dvy3bnmhLtWquJaW1snzpw5Ewb+AXqz/XnVLqvX6w1++OGHs7W1tWPA/yt5DtVwXV1dY8eOHVsC7pKyackmVXCtra3j7777btThcPyJPKcpKqevOZPJlCwoKIifPn16urGxcRIZtqT0+XI6chaLRTpx4sSjjo4OVhaQ/27p+XLkAsDr9S6cPn065PV6ewE/WUwba5WzkSsqKlr86KOPxlYO/u7wwmZFSTnD+Xy+J5988klcEIQeNlkKZVpOLmtJSUnoxIkTM263ewL4A0jk4nlzgmtpaZl9//3348AgWbx3btaWL6vb7Y4cOXJkvqioaAjoy4FptS3jDhw44D927JiEvBR6ae+5lbaEs9lsibfffnuhrKzsHvAgR6bVtoQ7ePDgSFtb2zIwBARzQ3re'+
			'lnAnT56cr62tfUDKgV8uU3S3CoKQ3LVr19SePXuSyHeoolXHZikdueVTp05NlZWVzbHBjn2rZY0zmUzJysrK+cOHDy87nc7/osJr7VlZ4woLC8WjR49OOZ3OMPKNEMs9Sy5rnNvtjn788cdRm812D/kzKtXK6oYwmUzJpqamgM/nCyK/G+TkPXS9ssJVV1cHPvjgg3nkm2BGHdLzsrqsNTU1wUOHDgWBeyp50soYZ7Va4/v27QvZbLZZ5H2o6mWM27Vr1+NDhw6JyO8GcfVIz8sY19HRMb93795pYExFT1oZ4axWa7ytrU0UBMGP3nD79+/3NzU1LSPfpVvaUWVTRri33nrraXV19Sh5mD5S23CeW9nBx9rb2+MFBQVDqLT6WK9NR+61116b3LZtG8i794xOJHPVhjiz2UxXV1egqKhoDAjlyfT8v7/RXzqdzmh7e/'+
			'uyy+X6mxzs4LNtXZzVao2/+uqr/5aUlJiQT7/zMvGmti7O4XDEjh8/Pm8ymcbI8ZYv09bFOZ3Opc7OzmW73X4fDUYNNphK6urqnpaVlQWR3+T1g/N4PKE33ngjCMyh4h5hs9a8rOXl5eHDhw/HgPE8e9JaE1dRURFtamqaAUby7EnrJZwgCFJzc3MYedLN6AM0tXoJV11dHejo6BDJ85v8Wr2E27FjR3Dl23+qnH9k00u4hoaGSFlZ2SgaX1J4AWe328WdO3fGkA+c8/5e+mJpuN27d880NDQss8ZXd7QoDbd3797g9u3b58jRUf1WS8O1traKXq93BL3h7HZ7rKamZhn5m385O67fSqu4lpaWf0tLSwGeasdJbxW3f//+py6XaxaN1m5rlYoTvV7vQyCsoSctM4DVag3V1NSAPPHqauQ8tbW1j51OpwmY1xqUmhk4'+
			'2dnZuWgymWZQ8XxXSRbg34aGhmKTyTSOyseo2WYBJgoLC6OCIDxFo73CepmSyWRxyu9D5PnIYaP+B5z2MkcItSrsAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Image_arrow-wrapper_L";
		el.ggDy=3;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 200px;';
		hs+='left : 297px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 25px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._image_arrowwrapper_l.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._image_arrowwrapper_l.onclick=function (e) {
			player.setVariableValue('map_open', !player.getVariableValue('map_open'));
			me._image_arrow_r.ggVisible = !me._image_arrow_r.ggVisible;
			var flag=me._image_arrow_r.ggVisible;
			me._image_arrow_r.style[domTransition]='none';
			me._image_arrow_r.style.visibility=((flag)&&(Number(me._image_arrow_r.style.opacity)>0||!me._image_arrow_r.style.opacity))?'inherit':'hidden';
			me._image_arrow_l.ggVisible = !me._image_arrow_l.ggVisible;
			var flag=me._image_arrow_l.ggVisible;
			me._image_arrow_l.style[domTransition]='none';
			me._image_arrow_l.style.visibility=((flag)&&(Number(me._image_arrow_l.style.opacity)>0||!me._image_arrow_l.style.opacity))?'inherit':'hidden';
		}
		me._image_arrowwrapper_l.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._map.appendChild(me._image_arrowwrapper_l);
		el=me._image_2=document.createElement('div');
		els=me._image_2__img=document.createElement('img');
		els.className='ggskin ggskin_image_2';
		hs=basePath + 'images/image_2.png';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="Image 2";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 281px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 293px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._image_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._image_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._map.appendChild(me._image_2);
		el=me._text_4=document.createElement('div');
		els=me._text_4__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 4";
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 36px;';
		hs+='left : 50px;';
		hs+='position : absolute;';
		hs+='top : 57px;';
		hs+='visibility : inherit;';
		hs+='width : 144px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 144px;';
		hs+='height: 36px;';
		hs+='pointer-events: none;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		hs+="font-size: 9px;text-shadow:3px 3px 3px black;";
		els.setAttribute('style',hs);
		els.innerHTML="Port & Yard<br>Management";
		el.appendChild(els);
		me._text_4.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_4.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_4.ggCurrentLogicStateText != newLogicStateText) {
				me._text_4.ggCurrentLogicStateText = newLogicStateText;
				me._text_4.style[domTransition]='';
				if (me._text_4.ggCurrentLogicStateText == 0) {
					me._text_4.ggText="Gestion portuaire<br>& gestion du parc";
					me._text_4__text.innerHTML=me._text_4.ggText;
					if (me._text_4.ggUpdateText) {
					me._text_4.ggUpdateText=function() {
						var hs="Gestion portuaire<br>& gestion du parc";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4.ggUpdatePosition) me._text_4.ggUpdatePosition();
					}
				}
				else if (me._text_4.ggCurrentLogicStateText == 1) {
					me._text_4.ggText="Hafen- und<br>Werftmanagement";
					me._text_4__text.innerHTML=me._text_4.ggText;
					if (me._text_4.ggUpdateText) {
					me._text_4.ggUpdateText=function() {
						var hs="Hafen- und<br>Werftmanagement";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4.ggUpdatePosition) me._text_4.ggUpdatePosition();
					}
				}
				else if (me._text_4.ggCurrentLogicStateText == 2) {
					me._text_4.ggText="Gestione del<br>porto e del piazzale";
					me._text_4__text.innerHTML=me._text_4.ggText;
					if (me._text_4.ggUpdateText) {
					me._text_4.ggUpdateText=function() {
						var hs="Gestione del<br>porto e del piazzale";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4.ggUpdatePosition) me._text_4.ggUpdatePosition();
					}
				}
				else if (me._text_4.ggCurrentLogicStateText == 3) {
					me._text_4.ggText="Gesti\xf3n de puertos";
					me._text_4__text.innerHTML=me._text_4.ggText;
					if (me._text_4.ggUpdateText) {
					me._text_4.ggUpdateText=function() {
						var hs="Gesti\xf3n de puertos";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4.ggUpdatePosition) me._text_4.ggUpdatePosition();
					}
				}
				else if (me._text_4.ggCurrentLogicStateText == 4) {
					me._text_4.ggText="\u6e2f\u53e3\u548c\u8d27\u573a\u7ba1\u7406";
					me._text_4__text.innerHTML=me._text_4.ggText;
					if (me._text_4.ggUpdateText) {
					me._text_4.ggUpdateText=function() {
						var hs="\u6e2f\u53e3\u548c\u8d27\u573a\u7ba1\u7406";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4.ggUpdatePosition) me._text_4.ggUpdatePosition();
					}
				}
				else {
					me._text_4.ggText="Port & Yard<br>Management";
					me._text_4__text.innerHTML=me._text_4.ggText;
					if (me._text_4.ggUpdateText) {
					me._text_4.ggUpdateText=function() {
						var hs="Port & Yard<br>Management";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4.ggUpdatePosition) me._text_4.ggUpdatePosition();
					}
				}
			}
		}
		me._text_4.ggUpdatePosition=function (useTransition) {
		}
		me._map.appendChild(me._text_4);
		el=me._text_4_1=document.createElement('div');
		els=me._text_4_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 4_1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 125px;';
		hs+='position : absolute;';
		hs+='top : 132px;';
		hs+='visibility : inherit;';
		hs+='width : 120px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 120px;';
		hs+='height: 20px;';
		hs+='pointer-events: none;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		hs+="font-size: 9px;text-shadow:3px 3px 3px black;";
		els.setAttribute('style',hs);
		els.innerHTML="Warehouse";
		el.appendChild(els);
		me._text_4_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_4_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_4_1.ggCurrentLogicStateText != newLogicStateText) {
				me._text_4_1.ggCurrentLogicStateText = newLogicStateText;
				me._text_4_1.style[domTransition]='';
				if (me._text_4_1.ggCurrentLogicStateText == 0) {
					me._text_4_1.ggText="Entrep\xf4t";
					me._text_4_1__text.innerHTML=me._text_4_1.ggText;
					if (me._text_4_1.ggUpdateText) {
					me._text_4_1.ggUpdateText=function() {
						var hs="Entrep\xf4t";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_1.ggUpdatePosition) me._text_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_4_1.ggCurrentLogicStateText == 1) {
					me._text_4_1.ggText="Lagerhaus";
					me._text_4_1__text.innerHTML=me._text_4_1.ggText;
					if (me._text_4_1.ggUpdateText) {
					me._text_4_1.ggUpdateText=function() {
						var hs="Lagerhaus";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_1.ggUpdatePosition) me._text_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_4_1.ggCurrentLogicStateText == 2) {
					me._text_4_1.ggText="Magazzino";
					me._text_4_1__text.innerHTML=me._text_4_1.ggText;
					if (me._text_4_1.ggUpdateText) {
					me._text_4_1.ggUpdateText=function() {
						var hs="Magazzino";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_1.ggUpdatePosition) me._text_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_4_1.ggCurrentLogicStateText == 3) {
					me._text_4_1.ggText="Almac\xe9n";
					me._text_4_1__text.innerHTML=me._text_4_1.ggText;
					if (me._text_4_1.ggUpdateText) {
					me._text_4_1.ggUpdateText=function() {
						var hs="Almac\xe9n";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_1.ggUpdatePosition) me._text_4_1.ggUpdatePosition();
					}
				}
				else if (me._text_4_1.ggCurrentLogicStateText == 4) {
					me._text_4_1.ggText="\u4ed3\u5e93";
					me._text_4_1__text.innerHTML=me._text_4_1.ggText;
					if (me._text_4_1.ggUpdateText) {
					me._text_4_1.ggUpdateText=function() {
						var hs="\u4ed3\u5e93";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_1.ggUpdatePosition) me._text_4_1.ggUpdatePosition();
					}
				}
				else {
					me._text_4_1.ggText="Warehouse";
					me._text_4_1__text.innerHTML=me._text_4_1.ggText;
					if (me._text_4_1.ggUpdateText) {
					me._text_4_1.ggUpdateText=function() {
						var hs="Warehouse";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_1.ggUpdatePosition) me._text_4_1.ggUpdatePosition();
					}
				}
			}
		}
		me._text_4_1.ggUpdatePosition=function (useTransition) {
		}
		me._map.appendChild(me._text_4_1);
		el=me._text_4_2=document.createElement('div');
		els=me._text_4_2__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 4_2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 43px;';
		hs+='left : 187px;';
		hs+='position : absolute;';
		hs+='top : 105px;';
		hs+='visibility : inherit;';
		hs+='width : 131px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 131px;';
		hs+='height: 43px;';
		hs+='pointer-events: none;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		hs+="font-size: 9px;text-shadow:3px 3px 3px black;";
		els.setAttribute('style',hs);
		els.innerHTML="Transport";
		el.appendChild(els);
		me._text_4_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_4_2.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_4_2.ggCurrentLogicStateText != newLogicStateText) {
				me._text_4_2.ggCurrentLogicStateText = newLogicStateText;
				me._text_4_2.style[domTransition]='';
				if (me._text_4_2.ggCurrentLogicStateText == 0) {
					me._text_4_2.ggText="Transport";
					me._text_4_2__text.innerHTML=me._text_4_2.ggText;
					if (me._text_4_2.ggUpdateText) {
					me._text_4_2.ggUpdateText=function() {
						var hs="Transport";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_2.ggUpdatePosition) me._text_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_4_2.ggCurrentLogicStateText == 1) {
					me._text_4_2.ggText="Transport";
					me._text_4_2__text.innerHTML=me._text_4_2.ggText;
					if (me._text_4_2.ggUpdateText) {
					me._text_4_2.ggUpdateText=function() {
						var hs="Transport";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_2.ggUpdatePosition) me._text_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_4_2.ggCurrentLogicStateText == 2) {
					me._text_4_2.ggText="Trasporto";
					me._text_4_2__text.innerHTML=me._text_4_2.ggText;
					if (me._text_4_2.ggUpdateText) {
					me._text_4_2.ggUpdateText=function() {
						var hs="Trasporto";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_2.ggUpdatePosition) me._text_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_4_2.ggCurrentLogicStateText == 3) {
					me._text_4_2.ggText="Transporte";
					me._text_4_2__text.innerHTML=me._text_4_2.ggText;
					if (me._text_4_2.ggUpdateText) {
					me._text_4_2.ggUpdateText=function() {
						var hs="Transporte";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_2.ggUpdatePosition) me._text_4_2.ggUpdatePosition();
					}
				}
				else if (me._text_4_2.ggCurrentLogicStateText == 4) {
					me._text_4_2.ggText="\u8fd0\u8f93";
					me._text_4_2__text.innerHTML=me._text_4_2.ggText;
					if (me._text_4_2.ggUpdateText) {
					me._text_4_2.ggUpdateText=function() {
						var hs="\u8fd0\u8f93";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_2.ggUpdatePosition) me._text_4_2.ggUpdatePosition();
					}
				}
				else {
					me._text_4_2.ggText="Transport";
					me._text_4_2__text.innerHTML=me._text_4_2.ggText;
					if (me._text_4_2.ggUpdateText) {
					me._text_4_2.ggUpdateText=function() {
						var hs="Transport";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_2.ggUpdatePosition) me._text_4_2.ggUpdatePosition();
					}
				}
			}
		}
		me._text_4_2.ggUpdatePosition=function (useTransition) {
		}
		me._map.appendChild(me._text_4_2);
		el=me._text_4_3=document.createElement('div');
		els=me._text_4_3__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 4_3";
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.85,sy:0.85 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : 76px;';
		hs+='position : absolute;';
		hs+='top : 210px;';
		hs+='visibility : inherit;';
		hs+='width : 120px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 120px;';
		hs+='height: 20px;';
		hs+='pointer-events: none;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		hs+='overflow-y: auto;';
		hs+="font-size: 9px;text-shadow:3px 3px 3px black;";
		els.setAttribute('style',hs);
		els.innerHTML="Field Service";
		el.appendChild(els);
		me._text_4_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_4_3.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_4_3.ggCurrentLogicStateText != newLogicStateText) {
				me._text_4_3.ggCurrentLogicStateText = newLogicStateText;
				me._text_4_3.style[domTransition]='';
				if (me._text_4_3.ggCurrentLogicStateText == 0) {
					me._text_4_3.ggText="Service sur site";
					me._text_4_3__text.innerHTML=me._text_4_3.ggText;
					if (me._text_4_3.ggUpdateText) {
					me._text_4_3.ggUpdateText=function() {
						var hs="Service sur site";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_3.ggUpdatePosition) me._text_4_3.ggUpdatePosition();
					}
				}
				else if (me._text_4_3.ggCurrentLogicStateText == 1) {
					me._text_4_3.ggText="Au\xdfendienst";
					me._text_4_3__text.innerHTML=me._text_4_3.ggText;
					if (me._text_4_3.ggUpdateText) {
					me._text_4_3.ggUpdateText=function() {
						var hs="Au\xdfendienst";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_3.ggUpdatePosition) me._text_4_3.ggUpdatePosition();
					}
				}
				else if (me._text_4_3.ggCurrentLogicStateText == 2) {
					me._text_4_3.ggText="Field Service";
					me._text_4_3__text.innerHTML=me._text_4_3.ggText;
					if (me._text_4_3.ggUpdateText) {
					me._text_4_3.ggUpdateText=function() {
						var hs="Field Service";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_3.ggUpdatePosition) me._text_4_3.ggUpdatePosition();
					}
				}
				else if (me._text_4_3.ggCurrentLogicStateText == 3) {
					me._text_4_3.ggText="Servicio de campo";
					me._text_4_3__text.innerHTML=me._text_4_3.ggText;
					if (me._text_4_3.ggUpdateText) {
					me._text_4_3.ggUpdateText=function() {
						var hs="Servicio de campo";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_3.ggUpdatePosition) me._text_4_3.ggUpdatePosition();
					}
				}
				else if (me._text_4_3.ggCurrentLogicStateText == 4) {
					me._text_4_3.ggText="\u73b0\u573a\u670d\u52a1";
					me._text_4_3__text.innerHTML=me._text_4_3.ggText;
					if (me._text_4_3.ggUpdateText) {
					me._text_4_3.ggUpdateText=function() {
						var hs="\u73b0\u573a\u670d\u52a1";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_3.ggUpdatePosition) me._text_4_3.ggUpdatePosition();
					}
				}
				else {
					me._text_4_3.ggText="Field Service";
					me._text_4_3__text.innerHTML=me._text_4_3.ggText;
					if (me._text_4_3.ggUpdateText) {
					me._text_4_3.ggUpdateText=function() {
						var hs="Field Service";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_4_3.ggUpdatePosition) me._text_4_3.ggUpdatePosition();
					}
				}
			}
		}
		me._text_4_3.ggUpdatePosition=function (useTransition) {
		}
		me._map.appendChild(me._text_4_3);
		el=me._ctnos=document.createElement('div');
		el.ggId="Ctn-os";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='bottom : 63px;';
		hs+='height : 25px;';
		hs+='left : 340px;';
		hs+='position : absolute;';
		hs+='visibility : hidden;';
		hs+='width : 25px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ctnos.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._ctnos.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.ggUserdata.tags.indexOf("\u65c1\u767d") != -1))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._ctnos.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._ctnos.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._ctnos.style[domTransition]='';
				if (me._ctnos.ggCurrentLogicStateVisible == 0) {
					me._ctnos.style.visibility=(Number(me._ctnos.style.opacity)>0||!me._ctnos.style.opacity)?'inherit':'hidden';
					me._ctnos.ggVisible=true;
				}
				else {
					me._ctnos.style.visibility="hidden";
					me._ctnos.ggVisible=false;
				}
			}
		}
		me._ctnos.ggUpdatePosition=function (useTransition) {
		}
		el=me._img_mute=document.createElement('div');
		els=me._img_mute__img=document.createElement('img');
		els.className='ggskin ggskin_img_mute';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAWCAYAAADTlvzyAAAA80lEQVRIib2VURXCMAxFMw7/q4RJwAGTgAQk4IA5AAfMAVLAwYoC5uDyQXYoBco6Wt5f06Q3SbNOJFKAAXbc1cTGx8Iq4ISjnLAauOIpF6zxQVmAel/HT7CkQG3hKQSbBAT23w4dC9SOLAHj2StgOSx+knewVXMHLNS2dtyb1MDe2eqArefepgauA649YJICA1CLtngWPWXj1Hvr0s0oWYUfqht0BaqcQ2N5faHaeYZWliJyEZFVURRnwIrIQfdt8J2cUKHh/kK9+/DrYKoaeI4B/izNuP0b0AFv/gpUaM3zFOYFKrTy7zUrUKGGxy9tHxt/A5PwIiQyXQetAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="img_mute";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 25px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._img_mute.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._img_mute.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.ggUserdata.customnodeid == "c00")) && 
				((player.nodeVisited(me._img_mute.ggElementNodeId()) == false))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._img_mute.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._img_mute.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._img_mute.style[domTransition]='';
				if (me._img_mute.ggCurrentLogicStateVisible == 0) {
					me._img_mute.style.visibility="hidden";
					me._img_mute.ggVisible=false;
				}
				else {
					me._img_mute.style.visibility=(Number(me._img_mute.style.opacity)>0||!me._img_mute.style.opacity)?'inherit':'hidden';
					me._img_mute.ggVisible=true;
				}
			}
		}
		me._img_mute.onclick=function (e) {
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
					e.ggSubElement.src=e.ggText;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
				}
			} else {
				player.pauseSound("media-panoos");
			}
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility='hidden';
			me._text_1.ggVisible=false;
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility='hidden';
			me._img_mute.ggVisible=false;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
			me._img_voice.ggVisible=true;
		}
		me._img_mute.ggUpdatePosition=function (useTransition) {
		}
		me._ctnos.appendChild(me._img_mute);
		el=me._img_voice=document.createElement('div');
		els=me._img_voice__img=document.createElement('img');
		els.className='ggskin ggskin_img_voice';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAABxklEQVRIib2VoXLbQBRFTzrm8ScIBQSZhLTACsrOJDvRJxiV2vsFVb9ADkyQDYKjGRHNhMggwUYGRUYFLXG/QAW+O9Y4sSUnSh552je7e+8+7b17dDG64qPjy4cjAJ1DF+RJNgaGwAyIjLOr1kDyJOsCE+BapT4QAmllTg+Igdg4O/f1Ru0SwFMFwEdvaxxpTiHAZiCavAROG/AZAwvgGLgXuf0geZJFQKFFe4nkSdbV/7kE/onUCOBo1xXOk+w7cFvD/CcwBx50gm/G2VWeZDHwQ2BBRxsGOmpXi/s1m1djXmGesr4MY4EcAyEXoyvKsizKt0Ws9VGlFqiWajzx/+QQ5i/COJuybhfAoHJCgKBNxT8qh8q/lfttgmwr/5f/+BTvahPEK3ypfKK8aA'+
			'VEyvaW86z8VfmvB1nwvpj4fYyzd/o+Uy68Tnq618WBmonLsgwr4+g13XQAZMvRVgua2Aqs9TAFUukF5FnAzDi73PmeGGfv8iT7o1bsNEiZ4qBCbsxG3DHU3C4xC1l7U23ItYca3hhni1oQAc2BgMMux9Q461u22+pfYbn9/AKce7b7orFOjLMr42wE3Kg0Y2OCe6PxSd4T/wEIRzGjINz2owAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="img_voice";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='bottom : 0px;';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : 0px;';
		hs+='position : absolute;';
		hs+='visibility : hidden;';
		hs+='width : 25px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._img_voice.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._img_voice.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.ggUserdata.customnodeid == "c00")) && 
				((player.nodeVisited(me._img_voice.ggElementNodeId()) == false))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._img_voice.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._img_voice.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._img_voice.style[domTransition]='';
				if (me._img_voice.ggCurrentLogicStateVisible == 0) {
					me._img_voice.style.visibility=(Number(me._img_voice.style.opacity)>0||!me._img_voice.style.opacity)?'inherit':'hidden';
					me._img_voice.ggVisible=true;
				}
				else {
					me._img_voice.style.visibility="hidden";
					me._img_voice.ggVisible=false;
				}
			}
		}
		me._img_voice.onclick=function (e) {
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.playVideo();
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.play();
				}
			} else {
				player.playSound("media-panoos","1");
			}
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility='hidden';
			me._img_voice.ggVisible=false;
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility=(Number(me._img_mute.style.opacity)>0||!me._img_mute.style.opacity)?'inherit':'hidden';
			me._img_mute.ggVisible=true;
			me._text_1.style[domTransition]='none';
			me._text_1.style.visibility='hidden';
			me._text_1.ggVisible=false;
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.style[domTransition]='none';
					e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
					e.ggVisible=true;
				}
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c00"))
				)
			) {
				var list=me.findElements("npc0001_gif",true);
				while(list.length>0) {
					var e=list.pop();
					e.ggSubElement.src='';
					e.style[domTransition]='none';
					e.style.visibility='hidden';
					e.ggVisible=false;
				}
			}
		}
		me._img_voice.ggUpdatePosition=function (useTransition) {
		}
		me._ctnos.appendChild(me._img_voice);
		el=me._text_1=document.createElement('div');
		els=me._text_1__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="Text 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : -35px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -20px;';
		hs+='visibility : hidden;';
		hs+='width : 100px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 100px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML="Click me!";
		el.appendChild(els);
		me._text_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._text_1.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._text_1.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._text_1.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._text_1.style[domTransition]='opacity 0s';
				if (me._text_1.ggCurrentLogicStateAlpha == 0) {
					me._text_1.style.visibility=me._text_1.ggVisible?'inherit':'hidden';
					me._text_1.style.opacity=1;
				}
				else {
					me._text_1.style.visibility="hidden";
					me._text_1.style.opacity=0;
				}
			}
		}
		me._text_1.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getVariableValue('ExtValue') == 18))
			)
			{
				newLogicStateText = 0;
			}
			else if (
				((player.getVariableValue('ExtValue') == 36))
			)
			{
				newLogicStateText = 1;
			}
			else if (
				((player.getVariableValue('ExtValue') == 54))
			)
			{
				newLogicStateText = 2;
			}
			else if (
				((player.getVariableValue('ExtValue') == 72))
			)
			{
				newLogicStateText = 3;
			}
			else if (
				((player.getVariableValue('ExtValue') == 90))
			)
			{
				newLogicStateText = 4;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._text_1.ggCurrentLogicStateText != newLogicStateText) {
				me._text_1.ggCurrentLogicStateText = newLogicStateText;
				me._text_1.style[domTransition]='opacity 0s';
				if (me._text_1.ggCurrentLogicStateText == 0) {
					me._text_1.ggText="Cliquez";
					me._text_1__text.innerHTML=me._text_1.ggText;
					if (me._text_1.ggUpdateText) {
					me._text_1.ggUpdateText=function() {
						var hs="Cliquez";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1.ggUpdatePosition) me._text_1.ggUpdatePosition();
					}
				}
				else if (me._text_1.ggCurrentLogicStateText == 1) {
					me._text_1.ggText="Hier klicken";
					me._text_1__text.innerHTML=me._text_1.ggText;
					if (me._text_1.ggUpdateText) {
					me._text_1.ggUpdateText=function() {
						var hs="Hier klicken";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1.ggUpdatePosition) me._text_1.ggUpdatePosition();
					}
				}
				else if (me._text_1.ggCurrentLogicStateText == 2) {
					me._text_1.ggText="Cliccami";
					me._text_1__text.innerHTML=me._text_1.ggText;
					if (me._text_1.ggUpdateText) {
					me._text_1.ggUpdateText=function() {
						var hs="Cliccami";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1.ggUpdatePosition) me._text_1.ggUpdatePosition();
					}
				}
				else if (me._text_1.ggCurrentLogicStateText == 3) {
					me._text_1.ggText="Haz clic";
					me._text_1__text.innerHTML=me._text_1.ggText;
					if (me._text_1.ggUpdateText) {
					me._text_1.ggUpdateText=function() {
						var hs="Haz clic";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1.ggUpdatePosition) me._text_1.ggUpdatePosition();
					}
				}
				else if (me._text_1.ggCurrentLogicStateText == 4) {
					me._text_1.ggText="\u53d6\u6d88\u975c\u97f3";
					me._text_1__text.innerHTML=me._text_1.ggText;
					if (me._text_1.ggUpdateText) {
					me._text_1.ggUpdateText=function() {
						var hs="\u53d6\u6d88\u975c\u97f3";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1.ggUpdatePosition) me._text_1.ggUpdatePosition();
					}
				}
				else {
					me._text_1.ggText="Click me!";
					me._text_1__text.innerHTML=me._text_1.ggText;
					if (me._text_1.ggUpdateText) {
					me._text_1.ggUpdateText=function() {
						var hs="Click me!";
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._text_1.ggUpdatePosition) me._text_1.ggUpdatePosition();
					}
				}
			}
		}
		me._text_1.ggUpdatePosition=function (useTransition) {
		}
		me._ctnos.appendChild(me._text_1);
		me._map.appendChild(me._ctnos);
		me.divSkin.appendChild(me._map);
		el=me._information_=document.createElement('div');
		el.ggId="information_\u8cc7\u8a0a\u4f86\u6e90";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='z-index: 8;';
		hs+='height : 100%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 100%;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_.ggIsActive=function() {
			return false;
		}
		el.ggElementNodeId=function() {
			return player.getCurrentNode();
		}
		me._information_.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width < 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._information_.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._information_.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._information_.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_.ggCurrentLogicStateScaling == 0) {
					me._information_.ggParameter.sx = 0.65;
					me._information_.ggParameter.sy = 0.65;
					me._information_.style[domTransform]=parameterToTransform(me._information_.ggParameter);
				}
				else {
					me._information_.ggParameter.sx = 1;
					me._information_.ggParameter.sy = 1;
					me._information_.style[domTransform]=parameterToTransform(me._information_.ggParameter);
				}
			}
		}
		me._information_.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.getVariableValue('vis_info_popup_3') == true))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._information_.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._information_.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._information_.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._information_.ggCurrentLogicStateVisible == 0) {
					me._information_.style.visibility=(Number(me._information_.style.opacity)>0||!me._information_.style.opacity)?'inherit':'hidden';
					me._information_.ggVisible=true;
				}
				else {
					me._information_.style.visibility="hidden";
					me._information_.ggVisible=false;
				}
			}
		}
		me._information_.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._screentint_info_continue3=document.createElement('div');
		el.ggId="screentint_info_(continue)3";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+='background : rgba(0,0,0,0.392157);';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 200%;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 200%;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._screentint_info_continue3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._screentint_info_continue3.onclick=function (e) {
			player.setVariableValue('vis_info_popup_3', false);
		}
		me._screentint_info_continue3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_.appendChild(me._screentint_info_continue3);
		el=me._information_bg_continue_3=document.createElement('div');
		el.ggId="information_bg_(continue)_3";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_rectangle ";
		el.ggType='rectangle';
		hs ='';
		hs+=cssPrefix + 'border-radius : 20px;';
		hs+='border-radius : 20px;';
		hs+='background : rgba(30,25,25,0.666667);';
		hs+='border : 0px solid #ffffff;';
		hs+='cursor : default;';
		hs+='height : 640px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 500px;';
		hs+='pointer-events:auto;';
		hs+='box-shadow:15px 15px 15px black;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._information_bg_continue_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._information_bg_continue_3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_.appendChild(me._information_bg_continue_3);
		el=me._info_text_body_continue_more_3=document.createElement('div');
		els=me._info_text_body_continue_more_3__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="info_text_body_(continue)_more_3";
		el.ggDx=0;
		el.ggDy=20;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 547px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 450px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 450px;';
		hs+='height: 547px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: pre-wrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		me._info_text_body_continue_more_3.ggUpdateText=function() {
			var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info3Pano')+""+player.getVariableValue('infolinkSuf');
			if (hs!=this.ggText) {
				this.ggText=hs;
				this.ggTextDiv.innerHTML=hs;
				if (this.ggUpdatePosition) this.ggUpdatePosition();
			}
		}
		me._info_text_body_continue_more_3.ggUpdateText();
		player.addListener('timer', function() {
			me._info_text_body_continue_more_3.ggUpdateText();
		});
		el.appendChild(els);
		me._info_text_body_continue_more_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_text_body_continue_more_3.logicBlock_text = function() {
			var newLogicStateText;
			if (
				((player.getViewerSize().width < 720))
			)
			{
				newLogicStateText = 0;
			}
			else {
				newLogicStateText = -1;
			}
			if (me._info_text_body_continue_more_3.ggCurrentLogicStateText != newLogicStateText) {
				me._info_text_body_continue_more_3.ggCurrentLogicStateText = newLogicStateText;
				me._info_text_body_continue_more_3.style[domTransition]='';
				if (me._info_text_body_continue_more_3.ggCurrentLogicStateText == 0) {
					me._info_text_body_continue_more_3.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('info3Pano')+""+player.getVariableValue('infolinkSuf');
					me._info_text_body_continue_more_3__text.innerHTML=me._info_text_body_continue_more_3.ggText;
					if (me._info_text_body_continue_more_3.ggUpdateText) {
					me._info_text_body_continue_more_3.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infoMlink')+""+player.getVariableValue('info3Pano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_text_body_continue_more_3.ggUpdatePosition) me._info_text_body_continue_more_3.ggUpdatePosition();
					}
				}
				else {
					me._info_text_body_continue_more_3.ggText=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info3Pano')+""+player.getVariableValue('infolinkSuf');
					me._info_text_body_continue_more_3__text.innerHTML=me._info_text_body_continue_more_3.ggText;
					if (me._info_text_body_continue_more_3.ggUpdateText) {
					me._info_text_body_continue_more_3.ggUpdateText=function() {
						var hs=player.getVariableValue('infolinkPre')+""+player.getVariableValue('infolink')+""+player.getVariableValue('info3Pano')+""+player.getVariableValue('infolinkSuf');
						if (hs!=this.ggText) {
							this.ggText=hs;
							this.ggTextDiv.innerHTML=hs;
							if (this.ggUpdatePosition) this.ggUpdatePosition();
						}
					}
					} else {
						if (me._info_text_body_continue_more_3.ggUpdatePosition) me._info_text_body_continue_more_3.ggUpdatePosition();
					}
				}
			}
		}
		me._info_text_body_continue_more_3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_.appendChild(me._info_text_body_continue_more_3);
		el=me._info_popup_close_continue_3=document.createElement('div');
		els=me._info_popup_close_continue_3__img=document.createElement('img');
		els.className='ggskin ggskin_info_popup_close_continue_3';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAYAAACMo1E1AAAC30lEQVRYhcWYPW8TQRCG342QoIjEkYZ0SRcaUESNlBMVpf8EHVLcQhNLiA6J/APSUVDEXQoKG1EjW6KAVHE6UtkgGqqH4nad8eWO27uzk5GiSHuz7zye2W+nBgYkkvYkdSRt++ZdSYmkmaSxb5tI6kv67JybNYlVByoFjmlmx0C6KqhBLtgY6Ppvqc+mgMS0db2ftcHSIIH3RnjmAyY1NRLfb2a03rWBSoCRgerVhSrR7BnIUW1NYBc4M2C7baBK9APgWbS+/3UBbNw2WxVxxgawOo4p5bjSebmAoyrnMPgnq8pYCWAo8WGZU7qqMRYBaMdgWuQw8B971wlm4vd8/EH+g83atZSzAC4pzB7Q943dCJE14BWwERl0M0bX+3Y9R98SB6vMGvDG+34F1i'+
			'PAvnv/5xHac5bQ0KmzdAA7wE/f50sZYA7sB3A/Uj8sLR0BR7EljQVsCub7htIeCRheGYQtANuA+f5hcg7X6nS05pw7VXbgvJD0RNIJsCVpIOmBpFNJe865i6YxVGcylPTfIdtRAP74/6O6GTN6YVJM53BNhIzgI+Cvl/oNbLbUy5jawgEbZMsKBvAEuNNQL2Ru1nhCFICNgIemxI0AlzIhyHaIT5IeK7ttPXXOfVM2Sc4lPZN0DNxqGmNN2fVNyq52sWDrBWBTSXLOnecAP9QEDBwTu0P8/7BnwPy6Fkp5r8Rvy5T4Yywgl4fdTpO99W0VmPHdNoAvIrQX91bfWOdUcht4XQVm/LeAl4CL8F08lfjGMEOmMdlbhfmsTQtXDrOk9G4ILpyEh0UfbfZu4g5RnDXjdHjd5c2Vs/j2ZZzn98hVA7L45DGM7TBZNWAOLP6e'+
			'7MdAAFz6GMyNsUltfRafCqbAQdsses0DAzZspWkmSYDcryvoofYNVPXgryGecrkOBhv5gHv+z75shrZ9M66CDVnh82ufZtavC1W535VAJpJSXX1Nvyvpl3Kv6c65vhrYP/yQ5+qn4GJtAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="info_popup_close_(continue)_3";
		el.ggDx=218;
		el.ggDy=-285;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 32px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 32px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._info_popup_close_continue_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return player.getCurrentNode();
		}
		me._info_popup_close_continue_3.onclick=function (e) {
			player.setVariableValue('vis_info_popup_3', false);
		}
		me._info_popup_close_continue_3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._information_.appendChild(me._info_popup_close_continue_3);
		me.divSkin.appendChild(me._information_);
		me._marker_mp3play.ggMarkerNormal=null;
		me._marker_mp3play.ggMarkerInstances.push(null);
		me._marker_mp3play.ggMarkerActive=null;
		me._marker_mp3play.ggMarkerInstances.push(null);
		for (var i = 0; i < me._marker_mp3play.childNodes.length; i++) {
			me._marker_mp3play.ggMarkerInstances.push(me._marker_mp3play.childNodes[i]);
		}
		me._mediapanoos.ggInitMedia('media/');
		me._mediainfoos.ggInitMedia('media/');
		me._map_all.ggMarkerInstances=[];
		me._map_all.ggMapId = 'Map_All';
		me._map_all.ggLastNodeId=null;
		me._map_all.callChildLogicBlocksHotspot_map_pin_changenode = function(){
			if(me._map_all.ggMarkerInstances) {
				var i;
				for(i = 0; i < me._map_all.ggMarkerInstances.length; i++) {
					if (me._map_all.ggMarkerInstances[i]._map_pin_normal && me._map_all.ggMarkerInstances[i]._map_pin_normal.logicBlock_visible) {
						me._map_all.ggMarkerInstances[i]._map_pin_normal.logicBlock_visible();
					}
					if (me._map_all.ggMarkerInstances[i]._map_pin_tt && me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_alpha) {
						me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_alpha();
					}
				}
			}
		}
		me._map_all.callChildLogicBlocksHotspot_map_pin_configloaded = function(){
			if(me._map_all.ggMarkerInstances) {
				var i;
				for(i = 0; i < me._map_all.ggMarkerInstances.length; i++) {
					if (me._map_all.ggMarkerInstances[i]._map_pin_tt && me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_position) {
						me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_position();
					}
				}
			}
		}
		me._map_all.callChildLogicBlocksHotspot_map_pin_mouseover = function(){
			if(me._map_all.ggMarkerInstances) {
				var i;
				for(i = 0; i < me._map_all.ggMarkerInstances.length; i++) {
					if (me._map_all.ggMarkerInstances[i]._map_pin_active && me._map_all.ggMarkerInstances[i]._map_pin_active.logicBlock_scaling) {
						me._map_all.ggMarkerInstances[i]._map_pin_active.logicBlock_scaling();
					}
					if (me._map_all.ggMarkerInstances[i]._map_pin_normal && me._map_all.ggMarkerInstances[i]._map_pin_normal.logicBlock_scaling) {
						me._map_all.ggMarkerInstances[i]._map_pin_normal.logicBlock_scaling();
					}
				}
			}
		}
		me._map_all.callChildLogicBlocksHotspot_map_pin_mouseover = function(){
			if(me._map_all.ggMarkerInstances) {
				var i;
				for(i = 0; i < me._map_all.ggMarkerInstances.length; i++) {
					if (me._map_all.ggMarkerInstances[i]._map_pin_tt && me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_alpha) {
						me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_alpha();
					}
				}
			}
		}
		me._map_all.callChildLogicBlocksHotspot_map_pin_active = function(){
			if(me._map_all.ggMarkerInstances) {
				var i;
				for(i = 0; i < me._map_all.ggMarkerInstances.length; i++) {
					if (me._map_all.ggMarkerInstances[i]._map_pin_active && me._map_all.ggMarkerInstances[i]._map_pin_active.logicBlock_alpha) {
						me._map_all.ggMarkerInstances[i]._map_pin_active.logicBlock_alpha();
					}
					if (me._map_all.ggMarkerInstances[i]._map_pin_normal && me._map_all.ggMarkerInstances[i]._map_pin_normal.logicBlock_alpha) {
						me._map_all.ggMarkerInstances[i]._map_pin_normal.logicBlock_alpha();
					}
				}
			}
		}
		me._map_all.callChildLogicBlocksHotspot_map_pin_hastouch = function(){
			if(me._map_all.ggMarkerInstances) {
				var i;
				for(i = 0; i < me._map_all.ggMarkerInstances.length; i++) {
					if (me._map_all.ggMarkerInstances[i]._map_pin_tt && me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_position) {
						me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_position();
					}
				}
			}
		}
		me._map_all.callChildLogicBlocksHotspot_map_pin_activehotspotchanged = function(){
			if(me._map_all.ggMarkerInstances) {
				var i;
				for(i = 0; i < me._map_all.ggMarkerInstances.length; i++) {
					if (me._map_all.ggMarkerInstances[i]._map_pin_tt && me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_alpha) {
						me._map_all.ggMarkerInstances[i]._map_pin_tt.logicBlock_alpha();
					}
				}
			}
		}
		me._map_all.ggSimpleFloorplanMarkerArray=[];
		me._map_all.ggFloorplanWidth=0;
		me._map_all.ggFloorplanHeight=0;
		me._map_all__mapdiv=document.createElement('div');
		me._map_all__mapdiv.className='ggskin ggskin_map';
		me._map_all.appendChild(me._map_all__mapdiv);
		me._map_all__img=document.createElement('img');
		me._map_all__img.className='ggskin ggskin_map';
		me._map_all__mapdiv.appendChild(me._map_all__img);
		me._map_all.ggShowSimpleFloorplan=function(mapDetails) {
			var mapWidth = me._map_all.clientWidth;
			var mapHeight = me._map_all.clientHeight;
			var tmpWidth = mapDetails['width'];
			var tmpHeight = mapDetails['height'];
			var levelLimit = 1000;
			var levels = 1;
			while (levelLimit < mapDetails['width'] || levelLimit < mapDetails['height']) {
				tmpWidth /= 2;
				tmpHeight /= 2;
				levelLimit *= 2;
				levels++;
			}
			var level = 1;
			while (levels > level && ((mapWidth * window.devicePixelRatio) >= 2*tmpWidth || (mapHeight * window.devicePixelRatio) >= 2*tmpHeight)) {
				tmpWidth *= 2;
				tmpHeight *= 2;
				levelLimit *= 2;
				level++;
			}
			var imageFilename = basePath + 'images/maptiles/' + me._map_all.ggMapId + '_' + level + '.' + mapDetails['tileformat'];
			me._map_all__img.setAttribute('src', imageFilename);
		me._map_all__mapdiv.setAttribute('style','position: absolute; left: 50%; margin-left: -' + me._map_all.ggFloorplanWidth / 2 + 'px; top: 50%; margin-top: -' + me._map_all.ggFloorplanHeight / 2 + 'px;width:' + me._map_all.ggFloorplanWidth + 'px;height:' + me._map_all.ggFloorplanHeight + 'px;overflow:hidden;;');
		me._map_all__img.setAttribute('style','width:' + me._map_all.ggFloorplanWidth + 'px;height:' + me._map_all.ggFloorplanHeight + 'px;-webkit-user-drag:none;pointer-events:none;');
		}
		me._map_all.ggCalculateFloorplanSize=function(mapDetails) {
			var floorplanWidth = mapDetails['width'];
			var floorplanHeight = mapDetails['height'];
			var frameAR = me._map_all.clientWidth / me._map_all.clientHeight;
			var floorplanAR = floorplanWidth / floorplanHeight;
			if (frameAR > floorplanAR) {
				me._map_all.ggFloorplanHeight = me._map_all.clientHeight;
				me._map_all.ggFloorplanWidth = me._map_all.ggFloorplanHeight * floorplanAR;
			} else {
				me._map_all.ggFloorplanWidth = me._map_all.clientWidth;
				me._map_all.ggFloorplanHeight = me._map_all.ggFloorplanWidth / floorplanAR;
			}
		}
		me._map_all.ggInitMap=function() {
			me._map_all.ggMapNotLoaded = false;
			var mapDetails = player.getMapDetails(me._map_all.ggMapId);
			me._map_all.style.backgroundColor = mapDetails['bgcolor'];
			if (mapDetails.hasOwnProperty('transparent') && mapDetails['transparent']) {
				me._map_all.ggPermeableMap = true;
			} else {
				me._map_all.ggPermeableMap = false;
			}
			me._map_all.ggCalculateFloorplanSize(mapDetails);
			me._map_all.ggShowSimpleFloorplan(mapDetails);
			me._map_all.ggFloorplanNorth = mapDetails['floorplannorth'];
		}
		me._map_all.ggClearMap=function() {
			me._map_all.ggClearMapMarkers();
			me._map_all.ggMapNotLoaded = true;
		}
		me._map_all.ggChangeMap=function(mapId) {
			var newMapType = player.getMapType(mapId)
			if (newMapType == 'web') {
				return;
			}
			me._map_all.ggMapId = mapId;
			if (!me._map_all.ggMapNotLoaded) {
				me._map_all.ggClearMap();
				me._map_all.ggInitMap();
				me._map_all.ggInitMapMarkers();
			}
		}
		me._map_all.ggPlaceMarkersOnSimpleFloorplan=function() {
			var markers=me._map_all.ggSimpleFloorplanMarkerArray;
			for (id in markers) {
				if (markers.hasOwnProperty(id)) {
					marker=markers[id];
					var coords = player.getNodeMapCoordsInPercent(id, me._map_all.ggMapId);
					var xPos = (me._map_all.ggFloorplanWidth * coords[0]) / 100.0;
					var yPos = (me._map_all.ggFloorplanHeight * coords[1]) / 100.0;
					marker.radarXPos = xPos;
					marker.radarYPos = yPos;
					xPos -= me._map_all.ggHMarkerAnchorOffset;
					yPos -= me._map_all.ggVMarkerAnchorOffset;
					marker.style['position'] = 'absolute';
					marker.style['left'] = xPos + 'px';
					marker.style['top'] = yPos + 'px';
					marker.style['z-index'] = me._map_all.style['z-index'] + 2;
				}
			}
		}
		me._map_all.ggInitMapMarkers=function() {
			me._map_all.ggClearMapMarkers();
			var ids=player.getNodeIds();
			me._map_all.ggFilteredIds = [];
			if (me._map_all.ggFilter != '') {
				var filter = me._map_all.ggFilter.split(',');
				for (i=0; i < ids.length; i++) {
					var nodeId = ids[i];
					var nodeData = player.getNodeUserdata(nodeId);
					for (var j=0; j < filter.length; j++) {
						if (nodeData['tags'].indexOf(filter[j].trim()) != -1) me._map_all.ggFilteredIds.push(nodeId);
					}
				}
				if (me._map_all.ggFilteredIds.length > 0) ids = me._map_all.ggFilteredIds;
			}
			for(var i=0; i < ids.length; i++) {
				var id = ids[i];
				var coords = player.getNodeMapCoordsInPercent(id, me._map_all.ggMapId);
				if (coords.length>=2) {
					me._map_all.ggHMarkerAnchorOffset = 12;
					me._map_all.ggVMarkerAnchorOffset = 12;
					var markerParent = new Object();
					markerParent.ggElementNodeId=function() { return id };
					var markerClass = new SkinElement_map_pin_Class(me, markerParent);
					me._map_all.ggMarkerInstances.push(markerClass);
					var marker = markerClass._map_pin;
					me._map_all.ggSimpleFloorplanMarkerArray[id] = marker;
					me._map_all__mapdiv.appendChild(marker);
				}
			}
			me._map_all.ggPlaceMarkersOnSimpleFloorplan();
			skin.updateSize(me._map_all);
		me._map_all.callChildLogicBlocksHotspot_map_pin_changenode();
		me._map_all.callChildLogicBlocksHotspot_map_pin_configloaded();
		me._map_all.callChildLogicBlocksHotspot_map_pin_mouseover();
		me._map_all.callChildLogicBlocksHotspot_map_pin_mouseover();
		me._map_all.callChildLogicBlocksHotspot_map_pin_active();
		me._map_all.callChildLogicBlocksHotspot_map_pin_hastouch();
		me._map_all.callChildLogicBlocksHotspot_map_pin_activehotspotchanged();
		}
		me._map_all.ggClearMapMarkers=function() {
			for (id in me._map_all.ggSimpleFloorplanMarkerArray) {
				if (me._map_all.ggSimpleFloorplanMarkerArray.hasOwnProperty(id)) {
					me._map_all__mapdiv.removeChild(me._map_all.ggSimpleFloorplanMarkerArray[id]);
				}
			}
			me._map_all.ggMarkerInstances=[];
			me._map_all.ggSimpleFloorplanMarkerArray=[];
		}
		player.addListener('sizechanged', function() {
			me.updateSize(me.divSkin);
		});
		player.addListener('configloaded', function() {
			if (
				(
					((player.getVariableValue('ExtValueSu') == "en"))
				)
			) {
				me._txt_menu_en.onclick.call(me._txt_menu_en);
			}
			if (
				(
					((player.getVariableValue('ExtValueSu') == "fr"))
				)
			) {
				me._txt_menu_fr.onclick.call(me._txt_menu_fr);
			}
			if (
				(
					((player.getVariableValue('ExtValueSu') == "de"))
				)
			) {
				me._txt_menu_de.onclick.call(me._txt_menu_de);
			}
			if (
				(
					((player.getVariableValue('ExtValueSu') == "it"))
				)
			) {
				me._txt_menu_it.onclick.call(me._txt_menu_it);
			}
			if (
				(
					((player.getVariableValue('ExtValueSu') == "es"))
				)
			) {
				me._txt_menu_es.onclick.call(me._txt_menu_es);
			}
			if (
				(
					((player.getVariableValue('ExtValueSu') == "zh"))
				)
			) {
				me._txt_menu_cn.onclick.call(me._txt_menu_cn);
			}
			if (
				(
					((player.getVariableValue('ExtValueSu') == "mono"))
				)
			) {
				me._txt_banner_menu_lan.style[domTransition]='none';
				me._txt_banner_menu_lan.style.visibility='hidden';
				me._txt_banner_menu_lan.ggVisible=false;
			}
			me._map_all.ggClearMap();
			me._map_all.ggInitMap(false);
			me._map_all.ggInitMapMarkers(true);
		});
		player.addListener('beforechangenode', function() {
			if (me._mediapanoos.ggApiPlayer) {
				if (me._mediapanoos.ggApiPlayerType == 'youtube') {
					let youtubeMediaFunction = function() {
						me._mediapanoos.ggApiPlayer.pauseVideo();
						me._mediapanoos.ggApiPlayer.seekTo(0);
					};
					if (me._mediapanoos.ggApiPlayerReady) {
						youtubeMediaFunction();
					} else {
						let youtubeApiInterval = setInterval(function() {
							if (me._mediapanoos.ggApiPlayerReady) {
								clearInterval(youtubeApiInterval);
								youtubeMediaFunction();
							}
						}, 100);
					}
				} else if (me._mediapanoos.ggApiPlayerType == 'vimeo') {
					me._mediapanoos.ggApiPlayer.pause();
					me._mediapanoos.ggApiPlayer.setCurrentTime(0);
				}
			} else {
				player.stopSound("media-panoos");
			}
			me._img_mute.style[domTransition]='none';
			me._img_mute.style.visibility=(Number(me._img_mute.style.opacity)>0||!me._img_mute.style.opacity)?'inherit':'hidden';
			me._img_mute.ggVisible=true;
			me._img_voice.style[domTransition]='none';
			me._img_voice.style.visibility='hidden';
			me._img_voice.ggVisible=false;
			var list=me.findElements("npc_gif",true);
			while(list.length>0) {
				var e=list.pop();
				e.style[domTransition]='none';
				e.style.visibility=(Number(e.style.opacity)>0||!e.style.opacity)?'inherit':'hidden';
				e.ggVisible=true;
			}
			var list=me.findElements("npc0001_gif",true);
			while(list.length>0) {
				var e=list.pop();
				e.ggSubElement.src='';
				e.style[domTransition]='none';
				e.style.visibility='hidden';
				e.ggVisible=false;
			}
		});
	};
	this.hotspotProxyClick=function(id, url) {
	}
	this.hotspotProxyDoubleClick=function(id, url) {
	}
	me.hotspotProxyOver=function(id, url) {
	}
	me.hotspotProxyOut=function(id, url) {
	}
	me.callChildLogicBlocksHotspot_hotspot_npc_sizechanged = function(){
		if(hotspotTemplates['Hotspot_npc']) {
			var i;
			for(i = 0; i < hotspotTemplates['Hotspot_npc'].length; i++) {
				if (hotspotTemplates['Hotspot_npc'][i]._npc && hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_position) {
					hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_position();
				}
				if (hotspotTemplates['Hotspot_npc'][i]._npc && hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_scaling) {
					hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_hotspot_npc_changenode = function(){
		if(hotspotTemplates['Hotspot_npc']) {
			var i;
			for(i = 0; i < hotspotTemplates['Hotspot_npc'].length; i++) {
				if (hotspotTemplates['Hotspot_npc'][i]._npc && hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_visible) {
					hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_hotspot_npc_changevisitednodes = function(){
		if(hotspotTemplates['Hotspot_npc']) {
			var i;
			for(i = 0; i < hotspotTemplates['Hotspot_npc'].length; i++) {
				if (hotspotTemplates['Hotspot_npc'][i]._npc_gif && hotspotTemplates['Hotspot_npc'][i]._npc_gif.logicBlock_visible) {
					hotspotTemplates['Hotspot_npc'][i]._npc_gif.logicBlock_visible();
				}
				if (hotspotTemplates['Hotspot_npc'][i]._npc0001_gif && hotspotTemplates['Hotspot_npc'][i]._npc0001_gif.logicBlock_visible) {
					hotspotTemplates['Hotspot_npc'][i]._npc0001_gif.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_hotspot_npc_activehotspotchanged = function(){
		if(hotspotTemplates['Hotspot_npc']) {
			var i;
			for(i = 0; i < hotspotTemplates['Hotspot_npc'].length; i++) {
				if (hotspotTemplates['Hotspot_npc'][i]._npc && hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_visible) {
					hotspotTemplates['Hotspot_npc'][i]._npc.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info_2_sizechanged = function(){
		if(hotspotTemplates['ht_info_延讀資訊2']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊2'].length; i++) {
				if (hotspotTemplates['ht_info_延讀資訊2'][i]._external_gif_2 && hotspotTemplates['ht_info_延讀資訊2'][i]._external_gif_2.logicBlock_scaling) {
					hotspotTemplates['ht_info_延讀資訊2'][i]._external_gif_2.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info_2_changenode = function(){
		if(hotspotTemplates['ht_info_延讀資訊2']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊2'].length; i++) {
				if (hotspotTemplates['ht_info_延讀資訊2'][i]._ht_info_customimage_continue_2 && hotspotTemplates['ht_info_延讀資訊2'][i]._ht_info_customimage_continue_2.logicBlock_visible) {
					hotspotTemplates['ht_info_延讀資訊2'][i]._ht_info_customimage_continue_2.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info_2_activehotspotchanged = function(){
		if(hotspotTemplates['ht_info_延讀資訊2']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊2'].length; i++) {
				if (hotspotTemplates['ht_info_延讀資訊2'][i]._ht_info_customimage_continue_2 && hotspotTemplates['ht_info_延讀資訊2'][i]._ht_info_customimage_continue_2.logicBlock_visible) {
					hotspotTemplates['ht_info_延讀資訊2'][i]._ht_info_customimage_continue_2.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info__sizechanged = function(){
		if(hotspotTemplates['ht_info_延讀資訊']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊'].length; i++) {
				if (hotspotTemplates['ht_info_延讀資訊'][i]._external_gif && hotspotTemplates['ht_info_延讀資訊'][i]._external_gif.logicBlock_scaling) {
					hotspotTemplates['ht_info_延讀資訊'][i]._external_gif.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info__changenode = function(){
		if(hotspotTemplates['ht_info_延讀資訊']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊'].length; i++) {
				if (hotspotTemplates['ht_info_延讀資訊'][i]._ht_info_customimage_continue && hotspotTemplates['ht_info_延讀資訊'][i]._ht_info_customimage_continue.logicBlock_visible) {
					hotspotTemplates['ht_info_延讀資訊'][i]._ht_info_customimage_continue.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info__activehotspotchanged = function(){
		if(hotspotTemplates['ht_info_延讀資訊']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊'].length; i++) {
				if (hotspotTemplates['ht_info_延讀資訊'][i]._ht_info_customimage_continue && hotspotTemplates['ht_info_延讀資訊'][i]._ht_info_customimage_continue.logicBlock_visible) {
					hotspotTemplates['ht_info_延讀資訊'][i]._ht_info_customimage_continue.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info_vr_sizechanged = function(){
		if(hotspotTemplates['ht_info_崁入(VR)']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_崁入(VR)'].length; i++) {
				if (hotspotTemplates['ht_info_崁入(VR)'][i]._external_vr_gif && hotspotTemplates['ht_info_崁入(VR)'][i]._external_vr_gif.logicBlock_scaling) {
					hotspotTemplates['ht_info_崁入(VR)'][i]._external_vr_gif.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info_vr_changenode = function(){
		if(hotspotTemplates['ht_info_崁入(VR)']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_崁入(VR)'].length; i++) {
				if (hotspotTemplates['ht_info_崁入(VR)'][i]._ht_info_customimage && hotspotTemplates['ht_info_崁入(VR)'][i]._ht_info_customimage.logicBlock_visible) {
					hotspotTemplates['ht_info_崁入(VR)'][i]._ht_info_customimage.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_info_vr_activehotspotchanged = function(){
		if(hotspotTemplates['ht_info_崁入(VR)']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_崁入(VR)'].length; i++) {
				if (hotspotTemplates['ht_info_崁入(VR)'][i]._ht_info_customimage && hotspotTemplates['ht_info_崁入(VR)'][i]._ht_info_customimage.logicBlock_visible) {
					hotspotTemplates['ht_info_崁入(VR)'][i]._ht_info_customimage.logicBlock_visible();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_0_sizechanged = function(){
		if(hotspotTemplates['ht_near_0*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_0*'].length; i++) {
				if (hotspotTemplates['ht_near_0*'][i]._container_9 && hotspotTemplates['ht_near_0*'][i]._container_9.logicBlock_scaling) {
					hotspotTemplates['ht_near_0*'][i]._container_9.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_0_changenode = function(){
		if(hotspotTemplates['ht_near_0*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_0*'].length; i++) {
				if (hotspotTemplates['ht_near_0*'][i]._arrowdouble_b7 && hotspotTemplates['ht_near_0*'][i]._arrowdouble_b7.logicBlock_alpha) {
					hotspotTemplates['ht_near_0*'][i]._arrowdouble_b7.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_0*'][i]._arrowdouble_a7 && hotspotTemplates['ht_near_0*'][i]._arrowdouble_a7.logicBlock_alpha) {
					hotspotTemplates['ht_near_0*'][i]._arrowdouble_a7.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_0_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_0*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_0*'].length; i++) {
				if (hotspotTemplates['ht_near_0*'][i]._arrowdouble_b7 && hotspotTemplates['ht_near_0*'][i]._arrowdouble_b7.logicBlock_alpha) {
					hotspotTemplates['ht_near_0*'][i]._arrowdouble_b7.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_0*'][i]._arrowdouble_a7 && hotspotTemplates['ht_near_0*'][i]._arrowdouble_a7.logicBlock_alpha) {
					hotspotTemplates['ht_near_0*'][i]._arrowdouble_a7.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_45_sizechanged = function(){
		if(hotspotTemplates['ht_near_45*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_45*'].length; i++) {
				if (hotspotTemplates['ht_near_45*'][i]._container_8 && hotspotTemplates['ht_near_45*'][i]._container_8.logicBlock_scaling) {
					hotspotTemplates['ht_near_45*'][i]._container_8.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_45_changenode = function(){
		if(hotspotTemplates['ht_near_45*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_45*'].length; i++) {
				if (hotspotTemplates['ht_near_45*'][i]._arrowdouble_b6 && hotspotTemplates['ht_near_45*'][i]._arrowdouble_b6.logicBlock_alpha) {
					hotspotTemplates['ht_near_45*'][i]._arrowdouble_b6.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_45*'][i]._arrowdouble_a6 && hotspotTemplates['ht_near_45*'][i]._arrowdouble_a6.logicBlock_alpha) {
					hotspotTemplates['ht_near_45*'][i]._arrowdouble_a6.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_45_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_45*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_45*'].length; i++) {
				if (hotspotTemplates['ht_near_45*'][i]._arrowdouble_b6 && hotspotTemplates['ht_near_45*'][i]._arrowdouble_b6.logicBlock_alpha) {
					hotspotTemplates['ht_near_45*'][i]._arrowdouble_b6.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_45*'][i]._arrowdouble_a6 && hotspotTemplates['ht_near_45*'][i]._arrowdouble_a6.logicBlock_alpha) {
					hotspotTemplates['ht_near_45*'][i]._arrowdouble_a6.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_90_sizechanged = function(){
		if(hotspotTemplates['ht_near_90*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_90*'].length; i++) {
				if (hotspotTemplates['ht_near_90*'][i]._container_7 && hotspotTemplates['ht_near_90*'][i]._container_7.logicBlock_scaling) {
					hotspotTemplates['ht_near_90*'][i]._container_7.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_90_changenode = function(){
		if(hotspotTemplates['ht_near_90*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_90*'].length; i++) {
				if (hotspotTemplates['ht_near_90*'][i]._arrowdouble_b5 && hotspotTemplates['ht_near_90*'][i]._arrowdouble_b5.logicBlock_alpha) {
					hotspotTemplates['ht_near_90*'][i]._arrowdouble_b5.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_90*'][i]._arrowdouble_a5 && hotspotTemplates['ht_near_90*'][i]._arrowdouble_a5.logicBlock_alpha) {
					hotspotTemplates['ht_near_90*'][i]._arrowdouble_a5.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_90_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_90*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_90*'].length; i++) {
				if (hotspotTemplates['ht_near_90*'][i]._arrowdouble_b5 && hotspotTemplates['ht_near_90*'][i]._arrowdouble_b5.logicBlock_alpha) {
					hotspotTemplates['ht_near_90*'][i]._arrowdouble_b5.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_90*'][i]._arrowdouble_a5 && hotspotTemplates['ht_near_90*'][i]._arrowdouble_a5.logicBlock_alpha) {
					hotspotTemplates['ht_near_90*'][i]._arrowdouble_a5.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_135_sizechanged = function(){
		if(hotspotTemplates['ht_near_135*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_135*'].length; i++) {
				if (hotspotTemplates['ht_near_135*'][i]._container_6 && hotspotTemplates['ht_near_135*'][i]._container_6.logicBlock_scaling) {
					hotspotTemplates['ht_near_135*'][i]._container_6.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_135_changenode = function(){
		if(hotspotTemplates['ht_near_135*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_135*'].length; i++) {
				if (hotspotTemplates['ht_near_135*'][i]._arrowdouble_b4 && hotspotTemplates['ht_near_135*'][i]._arrowdouble_b4.logicBlock_alpha) {
					hotspotTemplates['ht_near_135*'][i]._arrowdouble_b4.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_135*'][i]._arrowdouble_a4 && hotspotTemplates['ht_near_135*'][i]._arrowdouble_a4.logicBlock_alpha) {
					hotspotTemplates['ht_near_135*'][i]._arrowdouble_a4.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_135_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_135*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_135*'].length; i++) {
				if (hotspotTemplates['ht_near_135*'][i]._arrowdouble_b4 && hotspotTemplates['ht_near_135*'][i]._arrowdouble_b4.logicBlock_alpha) {
					hotspotTemplates['ht_near_135*'][i]._arrowdouble_b4.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_135*'][i]._arrowdouble_a4 && hotspotTemplates['ht_near_135*'][i]._arrowdouble_a4.logicBlock_alpha) {
					hotspotTemplates['ht_near_135*'][i]._arrowdouble_a4.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_180_sizechanged = function(){
		if(hotspotTemplates['ht_near_180*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_180*'].length; i++) {
				if (hotspotTemplates['ht_near_180*'][i]._container_5 && hotspotTemplates['ht_near_180*'][i]._container_5.logicBlock_scaling) {
					hotspotTemplates['ht_near_180*'][i]._container_5.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_180_changenode = function(){
		if(hotspotTemplates['ht_near_180*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_180*'].length; i++) {
				if (hotspotTemplates['ht_near_180*'][i]._arrowdouble_b3 && hotspotTemplates['ht_near_180*'][i]._arrowdouble_b3.logicBlock_alpha) {
					hotspotTemplates['ht_near_180*'][i]._arrowdouble_b3.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_180*'][i]._arrowdouble_a3 && hotspotTemplates['ht_near_180*'][i]._arrowdouble_a3.logicBlock_alpha) {
					hotspotTemplates['ht_near_180*'][i]._arrowdouble_a3.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_180_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_180*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_180*'].length; i++) {
				if (hotspotTemplates['ht_near_180*'][i]._arrowdouble_b3 && hotspotTemplates['ht_near_180*'][i]._arrowdouble_b3.logicBlock_alpha) {
					hotspotTemplates['ht_near_180*'][i]._arrowdouble_b3.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_180*'][i]._arrowdouble_a3 && hotspotTemplates['ht_near_180*'][i]._arrowdouble_a3.logicBlock_alpha) {
					hotspotTemplates['ht_near_180*'][i]._arrowdouble_a3.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_225_sizechanged = function(){
		if(hotspotTemplates['ht_near_225*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_225*'].length; i++) {
				if (hotspotTemplates['ht_near_225*'][i]._container_4 && hotspotTemplates['ht_near_225*'][i]._container_4.logicBlock_scaling) {
					hotspotTemplates['ht_near_225*'][i]._container_4.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_225_changenode = function(){
		if(hotspotTemplates['ht_near_225*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_225*'].length; i++) {
				if (hotspotTemplates['ht_near_225*'][i]._arrowdouble_b2 && hotspotTemplates['ht_near_225*'][i]._arrowdouble_b2.logicBlock_alpha) {
					hotspotTemplates['ht_near_225*'][i]._arrowdouble_b2.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_225*'][i]._arrowdouble_a2 && hotspotTemplates['ht_near_225*'][i]._arrowdouble_a2.logicBlock_alpha) {
					hotspotTemplates['ht_near_225*'][i]._arrowdouble_a2.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_225_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_225*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_225*'].length; i++) {
				if (hotspotTemplates['ht_near_225*'][i]._arrowdouble_b2 && hotspotTemplates['ht_near_225*'][i]._arrowdouble_b2.logicBlock_alpha) {
					hotspotTemplates['ht_near_225*'][i]._arrowdouble_b2.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_225*'][i]._arrowdouble_a2 && hotspotTemplates['ht_near_225*'][i]._arrowdouble_a2.logicBlock_alpha) {
					hotspotTemplates['ht_near_225*'][i]._arrowdouble_a2.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_270_sizechanged = function(){
		if(hotspotTemplates['ht_near_270*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_270*'].length; i++) {
				if (hotspotTemplates['ht_near_270*'][i]._container_1 && hotspotTemplates['ht_near_270*'][i]._container_1.logicBlock_scaling) {
					hotspotTemplates['ht_near_270*'][i]._container_1.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_270_changenode = function(){
		if(hotspotTemplates['ht_near_270*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_270*'].length; i++) {
				if (hotspotTemplates['ht_near_270*'][i]._arrowdouble_b1 && hotspotTemplates['ht_near_270*'][i]._arrowdouble_b1.logicBlock_alpha) {
					hotspotTemplates['ht_near_270*'][i]._arrowdouble_b1.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_270*'][i]._arrowdouble_a1 && hotspotTemplates['ht_near_270*'][i]._arrowdouble_a1.logicBlock_alpha) {
					hotspotTemplates['ht_near_270*'][i]._arrowdouble_a1.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_270_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_270*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_270*'].length; i++) {
				if (hotspotTemplates['ht_near_270*'][i]._arrowdouble_b1 && hotspotTemplates['ht_near_270*'][i]._arrowdouble_b1.logicBlock_alpha) {
					hotspotTemplates['ht_near_270*'][i]._arrowdouble_b1.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_270*'][i]._arrowdouble_a1 && hotspotTemplates['ht_near_270*'][i]._arrowdouble_a1.logicBlock_alpha) {
					hotspotTemplates['ht_near_270*'][i]._arrowdouble_a1.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_300_sizechanged = function(){
		if(hotspotTemplates['ht_near_300*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_300*'].length; i++) {
				if (hotspotTemplates['ht_near_300*'][i]._container_2 && hotspotTemplates['ht_near_300*'][i]._container_2.logicBlock_scaling) {
					hotspotTemplates['ht_near_300*'][i]._container_2.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_300_changenode = function(){
		if(hotspotTemplates['ht_near_300*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_300*'].length; i++) {
				if (hotspotTemplates['ht_near_300*'][i]._arrowdouble_b0 && hotspotTemplates['ht_near_300*'][i]._arrowdouble_b0.logicBlock_alpha) {
					hotspotTemplates['ht_near_300*'][i]._arrowdouble_b0.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_300*'][i]._arrowdouble_a0 && hotspotTemplates['ht_near_300*'][i]._arrowdouble_a0.logicBlock_alpha) {
					hotspotTemplates['ht_near_300*'][i]._arrowdouble_a0.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_300_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_300*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_300*'].length; i++) {
				if (hotspotTemplates['ht_near_300*'][i]._arrowdouble_b0 && hotspotTemplates['ht_near_300*'][i]._arrowdouble_b0.logicBlock_alpha) {
					hotspotTemplates['ht_near_300*'][i]._arrowdouble_b0.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_300*'][i]._arrowdouble_a0 && hotspotTemplates['ht_near_300*'][i]._arrowdouble_a0.logicBlock_alpha) {
					hotspotTemplates['ht_near_300*'][i]._arrowdouble_a0.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_315_sizechanged = function(){
		if(hotspotTemplates['ht_near_315*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_315*'].length; i++) {
				if (hotspotTemplates['ht_near_315*'][i]._container_3 && hotspotTemplates['ht_near_315*'][i]._container_3.logicBlock_scaling) {
					hotspotTemplates['ht_near_315*'][i]._container_3.logicBlock_scaling();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_315_changenode = function(){
		if(hotspotTemplates['ht_near_315*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_315*'].length; i++) {
				if (hotspotTemplates['ht_near_315*'][i]._arrowdouble_b && hotspotTemplates['ht_near_315*'][i]._arrowdouble_b.logicBlock_alpha) {
					hotspotTemplates['ht_near_315*'][i]._arrowdouble_b.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_315*'][i]._arrowdouble_a && hotspotTemplates['ht_near_315*'][i]._arrowdouble_a.logicBlock_alpha) {
					hotspotTemplates['ht_near_315*'][i]._arrowdouble_a.logicBlock_alpha();
				}
			}
		}
	}
	me.callChildLogicBlocksHotspot_ht_near_315_varchanged_ht_anima_C = function(){
		if(hotspotTemplates['ht_near_315*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_315*'].length; i++) {
				if (hotspotTemplates['ht_near_315*'][i]._arrowdouble_b && hotspotTemplates['ht_near_315*'][i]._arrowdouble_b.logicBlock_alpha) {
					hotspotTemplates['ht_near_315*'][i]._arrowdouble_b.logicBlock_alpha();
				}
				if (hotspotTemplates['ht_near_315*'][i]._arrowdouble_a && hotspotTemplates['ht_near_315*'][i]._arrowdouble_a.logicBlock_alpha) {
					hotspotTemplates['ht_near_315*'][i]._arrowdouble_a.logicBlock_alpha();
				}
			}
		}
	}
	player.addListener('changenode', function() {
		me.ggUserdata=player.userdata;
		var newMarker=[];
		var id=player.getCurrentNode();
		var i,j;
		var tags=me.ggUserdata.tags;
		for (i=0;i<nodeMarker.length;i++) {
			var match=false;
			if ((nodeMarker[i].ggMarkerNodeId.length > 0) && (nodeMarker[i].ggMarkerNodeId.charAt(0)=='{') && (nodeMarker[i].ggMarkerNodeId.substr(1, nodeMarker[i].ggMarkerNodeId.length - 2)==id) && (id!='')) match=true;  // }
			for(j=0;j<tags.length;j++) {
				if (nodeMarker[i].ggMarkerNodeId==tags[j]) match=true;
			}
			if (match) {
				newMarker.push(nodeMarker[i]);
			}
		}
		for(i=0;i<activeNodeMarker.length;i++) {
			if (newMarker.indexOf(activeNodeMarker[i])<0) {
				if (activeNodeMarker[i].ggMarkerNormal) {
					activeNodeMarker[i].ggMarkerNormal.style.visibility='inherit';
				}
				if (activeNodeMarker[i].ggMarkerActive) {
					activeNodeMarker[i].ggMarkerActive.style.visibility='hidden';
				}
				if (activeNodeMarker[i].ggDeactivate) {
					activeNodeMarker[i].ggDeactivate();
				}
				activeNodeMarker[i].ggIsMarkerActive=false;
			}
		}
		for(i=0;i<newMarker.length;i++) {
			if (activeNodeMarker.indexOf(newMarker[i])<0) {
				if (newMarker[i].ggMarkerNormal) {
					newMarker[i].ggMarkerNormal.style.visibility='hidden';
				}
				if (newMarker[i].ggMarkerActive) {
					newMarker[i].ggMarkerActive.style.visibility='inherit';
				}
				if (newMarker[i].ggActivate) {
					newMarker[i].ggActivate();
				}
				newMarker[i].ggIsMarkerActive=true;
			}
		}
		activeNodeMarker=newMarker;
	});
	me.skinTimerEvent=function() {
		me.ggCurrentTime=new Date().getTime();
		if (me._timer_maphide.ggLastIsActive!=me._timer_maphide.ggIsActive()) {
			me._timer_maphide.ggLastIsActive=me._timer_maphide.ggIsActive();
			if (me._timer_maphide.ggLastIsActive) {
			} else {
				if (
					(
						((player.getViewerSize().width < 1080))
					)
				) {
					me._image_arrowwrapper_l.onclick.call(me._image_arrowwrapper_l);
				}
			}
		}
		me._timer_maphide.ggUpdateConditionTimer();
		if (me._timer_flashing.ggLastIsActive!=me._timer_flashing.ggIsActive()) {
			me._timer_flashing.ggLastIsActive=me._timer_flashing.ggIsActive();
			if (me._timer_flashing.ggLastIsActive) {
				player.setVariableValue('ht_anima_B', !player.getVariableValue('ht_anima_B'));
			} else {
			}
		}
		if (me._timer_direction.ggLastIsActive!=me._timer_direction.ggIsActive()) {
			me._timer_direction.ggLastIsActive=me._timer_direction.ggIsActive();
			if (me._timer_direction.ggLastIsActive) {
				player.setVariableValue('ht_anima_A', !player.getVariableValue('ht_anima_A'));
			} else {
			}
		}
		if (me._timer_arrowdouble.ggLastIsActive!=me._timer_arrowdouble.ggIsActive()) {
			me._timer_arrowdouble.ggLastIsActive=me._timer_arrowdouble.ggIsActive();
			if (me._timer_arrowdouble.ggLastIsActive) {
				player.setVariableValue('ht_anima_C', !player.getVariableValue('ht_anima_C'));
			} else {
			}
		}
	};
	player.addListener('timer', me.skinTimerEvent);
	function SkinHotspotClass_hotspot_npc(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._hotspot_npc=document.createElement('div');
		el.ggId="Hotspot_npc";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._hotspot_npc.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._hotspot_npc.onclick=function (e) {
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_npc.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_npc.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_npc.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._hotspot_npc.ggUpdatePosition=function (useTransition) {
		}
		el=me._npc=document.createElement('div');
		el.ggId="npc";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:0.7,sy:0.7 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._npc.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._npc.logicBlock_position = function() {
			var newLogicStatePosition;
			if (
				((player.getViewerSize().width >= 1440))
			)
			{
				newLogicStatePosition = 0;
			}
			else if (
				((player.getViewerSize().width < 1440)) && 
				((player.getViewerSize().width >= 1080))
			)
			{
				newLogicStatePosition = 1;
			}
			else if (
				((player.getViewerSize().width < 1080)) && 
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStatePosition = 2;
			}
			else if (
				((player.getViewerSize().width < 720))
			)
			{
				newLogicStatePosition = 3;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._npc.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._npc.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._npc.style[domTransition]='left 0s, top 0s, ' + cssPrefix + 'transform 0s';
				if (me._npc.ggCurrentLogicStatePosition == 0) {
					this.ggDx = 300;
					this.ggDy = 0;
					me._npc.ggUpdatePosition(true);
				}
				else if (me._npc.ggCurrentLogicStatePosition == 1) {
					this.ggDx = 200;
					this.ggDy = 0;
					me._npc.ggUpdatePosition(true);
				}
				else if (me._npc.ggCurrentLogicStatePosition == 2) {
					this.ggDx = 50;
					this.ggDy = 0;
					me._npc.ggUpdatePosition(true);
				}
				else if (me._npc.ggCurrentLogicStatePosition == 3) {
					this.ggDx = -50;
					this.ggDy = 0;
					me._npc.ggUpdatePosition(true);
				}
				else {
					me._npc.ggDx=0;
					me._npc.ggDy=0;
					me._npc.ggUpdatePosition(true);
				}
			}
		}
		me._npc.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width < 1440)) && 
				((player.getViewerSize().width >= 1080)) || 
				((player.getViewerSize().height < 900)) && 
				((player.getViewerSize().height >= 840))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width < 1080)) && 
				((player.getViewerSize().width >= 720)) || 
				((player.getViewerSize().height < 840)) && 
				((player.getViewerSize().height >= 720))
			)
			{
				newLogicStateScaling = 1;
			}
			else if (
				((player.getViewerSize().width < 720)) || 
				((player.getViewerSize().height < 720))
			)
			{
				newLogicStateScaling = 2;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._npc.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._npc.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._npc.style[domTransition]='left 0s, top 0s, ' + cssPrefix + 'transform 0s';
				if (me._npc.ggCurrentLogicStateScaling == 0) {
					me._npc.ggParameter.sx = 0.58;
					me._npc.ggParameter.sy = 0.58;
					me._npc.style[domTransform]=parameterToTransform(me._npc.ggParameter);
				}
				else if (me._npc.ggCurrentLogicStateScaling == 1) {
					me._npc.ggParameter.sx = 0.4;
					me._npc.ggParameter.sy = 0.4;
					me._npc.style[domTransform]=parameterToTransform(me._npc.ggParameter);
				}
				else if (me._npc.ggCurrentLogicStateScaling == 2) {
					me._npc.ggParameter.sx = 0.3;
					me._npc.ggParameter.sy = 0.3;
					me._npc.style[domTransform]=parameterToTransform(me._npc.ggParameter);
				}
				else {
					me._npc.ggParameter.sx = 0.7;
					me._npc.ggParameter.sy = 0.7;
					me._npc.style[domTransform]=parameterToTransform(me._npc.ggParameter);
				}
			}
		}
		me._npc.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.ggUserdata.customnodeid == "c00"))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._npc.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._npc.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._npc.style[domTransition]='left 0s, top 0s, ' + cssPrefix + 'transform 0s';
				if (me._npc.ggCurrentLogicStateVisible == 0) {
					me._npc.style.visibility=(Number(me._npc.style.opacity)>0||!me._npc.style.opacity)?'inherit':'hidden';
					me._npc.ggVisible=true;
				}
				else {
					me._npc.style.visibility=(Number(me._npc.style.opacity)>0||!me._npc.style.opacity)?'inherit':'hidden';
					me._npc.ggVisible=true;
				}
			}
		}
		me._npc.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		el=me._npc_gif=document.createElement('div');
		els=me._npc_gif__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._npc_gif.ggUpdatePosition();}
		el.ggText=basePath + "assets/Gif/npc.gif";
		els.setAttribute('src', el.ggText);
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="npc_gif";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 1080px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 1080px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._npc_gif.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._npc_gif.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.nodeVisited(me._npc_gif.ggElementNodeId()) == true))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._npc_gif.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._npc_gif.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._npc_gif.style[domTransition]='';
				if (me._npc_gif.ggCurrentLogicStateVisible == 0) {
					me._npc_gif.style.visibility=(Number(me._npc_gif.style.opacity)>0||!me._npc_gif.style.opacity)?'inherit':'hidden';
					me._npc_gif.ggVisible=true;
				}
				else {
					me._npc_gif.style.visibility="hidden";
					me._npc_gif.ggVisible=false;
				}
			}
		}
		me._npc_gif.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._npc_gif.clientWidth;
			var parentHeight = me._npc_gif.clientHeight;
			var img = me._npc_gif__img;
			var aspectRatioDiv = me._npc_gif.clientWidth / me._npc_gif.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			if (img.naturalWidth < parentWidth) parentWidth = img.naturalWidth;
			if (img.naturalHeight < parentHeight) parentHeight = img.naturalHeight;
			var currentWidth,currentHeight;
			currentWidth = parentWidth;
			currentHeight = parentHeight;
			img.style.width=parentWidth + 'px';
			img.style.height=parentHeight + 'px';
			img.style.right='0px';
			img.style.bottom='0px';
		}
		me._npc.appendChild(me._npc_gif);
		el=me._npc0001_gif=document.createElement('div');
		els=me._npc0001_gif__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._npc0001_gif.ggUpdatePosition();}
		el.ggText=basePath + "assets/Gif/npc0001.gif";
		els.setAttribute('src', el.ggText);
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="npc0001_gif";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : default;';
		hs+='height : 1080px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 1080px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._npc0001_gif.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._npc0001_gif.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((player.nodeVisited(me._npc0001_gif.ggElementNodeId()) == true))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._npc0001_gif.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._npc0001_gif.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._npc0001_gif.style[domTransition]='';
				if (me._npc0001_gif.ggCurrentLogicStateVisible == 0) {
					me._npc0001_gif.style.visibility="hidden";
					me._npc0001_gif__img.src = '';
					me._npc0001_gif.ggVisible=false;
				}
				else {
					me._npc0001_gif.style.visibility=(Number(me._npc0001_gif.style.opacity)>0||!me._npc0001_gif.style.opacity)?'inherit':'hidden';
					me._npc0001_gif.ggSubElement.src=me._npc0001_gif.ggText;
					me._npc0001_gif.ggVisible=true;
				}
			}
		}
		me._npc0001_gif.ggMediaEnded=function () {
			me._npc0001_gif.style[domTransition]='none';
			me._npc0001_gif.style.visibility=(Number(me._npc0001_gif.style.opacity)>0||!me._npc0001_gif.style.opacity)?'inherit':'hidden';
			me._npc0001_gif.ggVisible=true;
			me._npc0001_gif.ggSubElement.src=me._npc0001_gif.ggText;
		}
		me._npc0001_gif.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._npc0001_gif.clientWidth;
			var parentHeight = me._npc0001_gif.clientHeight;
			var img = me._npc0001_gif__img;
			var aspectRatioDiv = me._npc0001_gif.clientWidth / me._npc0001_gif.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			if (img.naturalWidth < parentWidth) parentWidth = img.naturalWidth;
			if (img.naturalHeight < parentHeight) parentHeight = img.naturalHeight;
			var currentWidth,currentHeight;
			currentWidth = parentWidth;
			currentHeight = parentHeight;
			img.style.width=parentWidth + 'px';
			img.style.height=parentHeight + 'px';
			img.style.right='0px';
			img.style.bottom='0px';
		}
		me._npc.appendChild(me._npc0001_gif);
		el=me._npc_png=document.createElement('div');
		els=me._npc_png__img=document.createElement('img');
		els.className='ggskin ggskin_npc_png';
		hs=basePath + 'images/npc_png.png';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="npc_png";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 1080px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 1080px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._npc_png.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._npc_png.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._npc.appendChild(me._npc_png);
		me._hotspot_npc.appendChild(me._npc);
		me.__div = me._hotspot_npc;
	};
	function SkinHotspotClass_ht_info_2(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_info_2=document.createElement('div');
		el.ggId="ht_info_\u5ef6\u8b80\u8cc7\u8a0a2";
		el.ggDx=-368;
		el.ggDy=-263;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='z-index: 8;';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ht_info_2.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_info_2.onclick=function (e) {
			player.setVariableValue('vis_info_popup_2', true);
			if (
				(
					((me.ggUserdata.tags.indexOf("info") != -1))
				)
			) {
				gtag('event', 'hotspot2_clicked', {
  'event_category': player.getVariableValue('UA_category'),
  'event_label': player.hotspot.title
});
			}
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_2.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_2.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_2.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_2.ggUpdatePosition=function (useTransition) {
		}
		el=me._ht_info_customimage_continue_2=document.createElement('div');
		els=me._ht_info_customimage_continue_2__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._ht_info_customimage_continue_2.ggUpdatePosition();}
		if ((hotspot) && (hotspot.customimage)) {
			el.ggText=hotspot.customimage;
			els.setAttribute('src', hotspot.customimage);
			els.style.width=hotspot.customimagewidth + 'px';
			els.style.height=hotspot.customimageheight + 'px';
			me.ggUse3d = hotspot.use3D;
			me.gg3dDistance = hotspot.distance3D;
		}
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ht_info_CustomImage_(continue)_2";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ht_info_customimage_continue_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._ht_info_customimage_continue_2.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.hotspot.customimage == ""))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._ht_info_customimage_continue_2.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._ht_info_customimage_continue_2.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._ht_info_customimage_continue_2.style[domTransition]='';
				if (me._ht_info_customimage_continue_2.ggCurrentLogicStateVisible == 0) {
					me._ht_info_customimage_continue_2.style.visibility="hidden";
					me._ht_info_customimage_continue_2__img.src = '';
					me._ht_info_customimage_continue_2.ggVisible=false;
				}
				else {
					me._ht_info_customimage_continue_2.style.visibility=(Number(me._ht_info_customimage_continue_2.style.opacity)>0||!me._ht_info_customimage_continue_2.style.opacity)?'inherit':'hidden';
					me._ht_info_customimage_continue_2.ggSubElement.src=me._ht_info_customimage_continue_2.ggText;
					me._ht_info_customimage_continue_2.ggVisible=true;
				}
			}
		}
		me._ht_info_customimage_continue_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._ht_info_customimage_continue_2.clientWidth;
			var parentHeight = me._ht_info_customimage_continue_2.clientHeight;
			var img = me._ht_info_customimage_continue_2__img;
			var aspectRatioDiv = me._ht_info_customimage_continue_2.clientWidth / me._ht_info_customimage_continue_2.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			if (img.naturalWidth < parentWidth) parentWidth = img.naturalWidth;
			if (img.naturalHeight < parentHeight) parentHeight = img.naturalHeight;
			var currentWidth,currentHeight;
			if ((hotspot) && (hotspot.customimage)) {
				currentWidth  = hotspot.customimagewidth;
				currentHeight = hotspot.customimageheight;
			}
			img.style.left='50%';
			img.style.marginLeft='-' + currentWidth/2 + 'px';
			img.style.top='50%';
			img.style.marginTop='-' + currentHeight/2 + 'px';
		}
		me._ht_info_2.appendChild(me._ht_info_customimage_continue_2);
		el=me._external_gif_2=document.createElement('div');
		els=me._external_gif_2__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._external_gif_2.ggUpdatePosition();}
		el.ggText=basePath + "assets/Gif/橘熱點-1.gif";
		els.setAttribute('src', el.ggText);
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="External_GIF_2";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 30px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 30px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._external_gif_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._external_gif_2.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._external_gif_2.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._external_gif_2.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._external_gif_2.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._external_gif_2.ggCurrentLogicStateScaling == 0) {
					me._external_gif_2.ggParameter.sx = 1;
					me._external_gif_2.ggParameter.sy = 1;
					me._external_gif_2.style[domTransform]=parameterToTransform(me._external_gif_2.ggParameter);
				}
				else if (me._external_gif_2.ggCurrentLogicStateScaling == 1) {
					me._external_gif_2.ggParameter.sx = 0.7;
					me._external_gif_2.ggParameter.sy = 0.7;
					me._external_gif_2.style[domTransform]=parameterToTransform(me._external_gif_2.ggParameter);
				}
				else {
					me._external_gif_2.ggParameter.sx = 1;
					me._external_gif_2.ggParameter.sy = 1;
					me._external_gif_2.style[domTransform]=parameterToTransform(me._external_gif_2.ggParameter);
				}
			}
		}
		me._external_gif_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._external_gif_2.clientWidth;
			var parentHeight = me._external_gif_2.clientHeight;
			var img = me._external_gif_2__img;
			var aspectRatioDiv = me._external_gif_2.clientWidth / me._external_gif_2.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			var currentWidth,currentHeight;
			currentWidth = parentWidth;
			currentHeight = parentHeight;
			img.style.width=parentWidth + 'px';
			img.style.height=parentHeight + 'px';
			img.style.left='50%';
			img.style.marginLeft='-' + currentWidth/2 + 'px';
			img.style.top='50%';
			img.style.marginTop='-' + currentHeight/2 + 'px';
		}
		me._ht_info_2.appendChild(me._external_gif_2);
		if ((hotspot) && (hotspot.customimage)) {
			el.style.width=hotspot.customimagewidth + 'px';
			el.style.height=hotspot.customimageheight + 'px';
		}
		me.__div = me._ht_info_2;
	};
	function SkinHotspotClass_ht_info_(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_info_=document.createElement('div');
		el.ggId="ht_info_\u5ef6\u8b80\u8cc7\u8a0a";
		el.ggDx=-368;
		el.ggDy=-263;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='z-index: 8;';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ht_info_.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_info_.onclick=function (e) {
			player.setVariableValue('vis_info_popup', true);
			if (
				(
					((me.ggUserdata.customnodeid == "c04")) || 
					((me.ggUserdata.customnodeid == "c041"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/1.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c08")) || 
					((me.ggUserdata.customnodeid == "c081")) || 
					((me.ggUserdata.customnodeid == "c085"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/2.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "c09")) || 
					((me.ggUserdata.customnodeid == "c091"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/3.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "b02")) || 
					((me.ggUserdata.customnodeid == "b021"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/4.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "b05")) || 
					((me.ggUserdata.customnodeid == "b06")) || 
					((me.ggUserdata.customnodeid == "b061"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/5.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "a03")) || 
					((me.ggUserdata.customnodeid == "a031"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/6.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "a06")) || 
					((me.ggUserdata.customnodeid == "a07")) || 
					((me.ggUserdata.customnodeid == "a071"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/7.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "d03")) || 
					((me.ggUserdata.customnodeid == "d031")) || 
					((me.ggUserdata.customnodeid == "d04"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/8.mp3");
			}
			if (
				(
					((me.ggUserdata.customnodeid == "d05")) || 
					((me.ggUserdata.customnodeid == "d06"))
				)
			) {
				skin._mediainfoos.ggInitMedia("assets\/mp3\/info\/"+player.getVariableValue('ExtValue')+"\/9.mp3");
			}
			skin._img_mute.onclick.call(skin._img_mute);
			skin._img_voice_1.onclick.call(skin._img_voice_1);
			skin._text_2.style[domTransition]='none';
			skin._text_2.style.visibility=(Number(skin._text_2.style.opacity)>0||!skin._text_2.style.opacity)?'inherit':'hidden';
			skin._text_2.ggVisible=true;
			if (
				(
					((me.ggUserdata.tags.indexOf("info") != -1))
				)
			) {
				gtag('event', 'hotspot1_clicked', {
  'event_category': player.getVariableValue('UA_category'),
  'event_label': player.hotspot.title
});
			}
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_.ggUpdatePosition=function (useTransition) {
		}
		el=me._ht_info_customimage_continue=document.createElement('div');
		els=me._ht_info_customimage_continue__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._ht_info_customimage_continue.ggUpdatePosition();}
		if ((hotspot) && (hotspot.customimage)) {
			el.ggText=hotspot.customimage;
			els.setAttribute('src', hotspot.customimage);
			els.style.width=hotspot.customimagewidth + 'px';
			els.style.height=hotspot.customimageheight + 'px';
			me.ggUse3d = hotspot.use3D;
			me.gg3dDistance = hotspot.distance3D;
		}
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ht_info_CustomImage_(continue)";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ht_info_customimage_continue.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._ht_info_customimage_continue.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.hotspot.customimage == ""))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._ht_info_customimage_continue.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._ht_info_customimage_continue.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._ht_info_customimage_continue.style[domTransition]='';
				if (me._ht_info_customimage_continue.ggCurrentLogicStateVisible == 0) {
					me._ht_info_customimage_continue.style.visibility="hidden";
					me._ht_info_customimage_continue__img.src = '';
					me._ht_info_customimage_continue.ggVisible=false;
				}
				else {
					me._ht_info_customimage_continue.style.visibility=(Number(me._ht_info_customimage_continue.style.opacity)>0||!me._ht_info_customimage_continue.style.opacity)?'inherit':'hidden';
					me._ht_info_customimage_continue.ggSubElement.src=me._ht_info_customimage_continue.ggText;
					me._ht_info_customimage_continue.ggVisible=true;
				}
			}
		}
		me._ht_info_customimage_continue.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._ht_info_customimage_continue.clientWidth;
			var parentHeight = me._ht_info_customimage_continue.clientHeight;
			var img = me._ht_info_customimage_continue__img;
			var aspectRatioDiv = me._ht_info_customimage_continue.clientWidth / me._ht_info_customimage_continue.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			if (img.naturalWidth < parentWidth) parentWidth = img.naturalWidth;
			if (img.naturalHeight < parentHeight) parentHeight = img.naturalHeight;
			var currentWidth,currentHeight;
			if ((hotspot) && (hotspot.customimage)) {
				currentWidth  = hotspot.customimagewidth;
				currentHeight = hotspot.customimageheight;
			}
			img.style.left='50%';
			img.style.marginLeft='-' + currentWidth/2 + 'px';
			img.style.top='50%';
			img.style.marginTop='-' + currentHeight/2 + 'px';
		}
		me._ht_info_.appendChild(me._ht_info_customimage_continue);
		el=me._external_gif=document.createElement('div');
		els=me._external_gif__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._external_gif.ggUpdatePosition();}
		el.ggText=basePath + "assets/Gif/橘熱點-1.gif";
		els.setAttribute('src', el.ggText);
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="External_GIF";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 30px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 30px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._external_gif.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._external_gif.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._external_gif.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._external_gif.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._external_gif.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._external_gif.ggCurrentLogicStateScaling == 0) {
					me._external_gif.ggParameter.sx = 1;
					me._external_gif.ggParameter.sy = 1;
					me._external_gif.style[domTransform]=parameterToTransform(me._external_gif.ggParameter);
				}
				else if (me._external_gif.ggCurrentLogicStateScaling == 1) {
					me._external_gif.ggParameter.sx = 0.7;
					me._external_gif.ggParameter.sy = 0.7;
					me._external_gif.style[domTransform]=parameterToTransform(me._external_gif.ggParameter);
				}
				else {
					me._external_gif.ggParameter.sx = 1;
					me._external_gif.ggParameter.sy = 1;
					me._external_gif.style[domTransform]=parameterToTransform(me._external_gif.ggParameter);
				}
			}
		}
		me._external_gif.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._external_gif.clientWidth;
			var parentHeight = me._external_gif.clientHeight;
			var img = me._external_gif__img;
			var aspectRatioDiv = me._external_gif.clientWidth / me._external_gif.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			var currentWidth,currentHeight;
			currentWidth = parentWidth;
			currentHeight = parentHeight;
			img.style.width=parentWidth + 'px';
			img.style.height=parentHeight + 'px';
			img.style.left='50%';
			img.style.marginLeft='-' + currentWidth/2 + 'px';
			img.style.top='50%';
			img.style.marginTop='-' + currentHeight/2 + 'px';
		}
		me._ht_info_.appendChild(me._external_gif);
		if ((hotspot) && (hotspot.customimage)) {
			el.style.width=hotspot.customimagewidth + 'px';
			el.style.height=hotspot.customimageheight + 'px';
		}
		me.__div = me._ht_info_;
	};
	function SkinHotspotClass_ht_info_vr(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_info_vr=document.createElement('div');
		el.ggId="ht_info_\u5d01\u5165(VR)";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -119px;';
		hs+='position : absolute;';
		hs+='top : 1px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ht_info_vr.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_info_vr.onclick=function (e) {
			skin._info_text_body_vr.ggText=player.getVariableValue('arPre')+""+me.hotspot.description+""+player.getVariableValue('arSuf');
			skin._info_text_body_vr.ggTextDiv.innerHTML=skin._info_text_body_vr.ggText;
			if (skin._info_text_body_vr.ggUpdateText) {
				skin._info_text_body_vr.ggUpdateText=function() {
					var hs=player.getVariableValue('arPre')+""+me.hotspot.description+""+player.getVariableValue('arSuf');
					if (hs!=this.ggText) {
						this.ggText=hs;
						this.ggTextDiv.innerHTML=hs;
						if (this.ggUpdatePosition) this.ggUpdatePosition();
					}
				}
			}
			if (skin._info_text_body_vr.ggUpdatePosition) {
				skin._info_text_body_vr.ggUpdatePosition();
			}
			skin._info_text_body_vr.ggTextDiv.scrollTop = 0;
			player.setVariableValue('vis_info_vr_popup', true);
			if (
				(
					((me.ggUserdata.tags.indexOf("info") != -1))
				)
			) {
				gtag('event', 'hotspotAR_clicked', {
  'event_category': player.getVariableValue('UA_category'),
  'event_label': player.hotspot.title
});
			}
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_vr.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_vr.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_vr.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_info_vr.ggUpdatePosition=function (useTransition) {
		}
		el=me._ht_info_customimage=document.createElement('div');
		els=me._ht_info_customimage__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._ht_info_customimage.ggUpdatePosition();}
		if ((hotspot) && (hotspot.customimage)) {
			el.ggText=hotspot.customimage;
			els.setAttribute('src', hotspot.customimage);
			els.style.width=hotspot.customimagewidth + 'px';
			els.style.height=hotspot.customimageheight + 'px';
			me.ggUse3d = hotspot.use3D;
			me.gg3dDistance = hotspot.distance3D;
		}
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ht_info_CustomImage";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._ht_info_customimage.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._ht_info_customimage.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.hotspot.customimage == ""))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._ht_info_customimage.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._ht_info_customimage.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._ht_info_customimage.style[domTransition]='';
				if (me._ht_info_customimage.ggCurrentLogicStateVisible == 0) {
					me._ht_info_customimage.style.visibility="hidden";
					me._ht_info_customimage__img.src = '';
					me._ht_info_customimage.ggVisible=false;
				}
				else {
					me._ht_info_customimage.style.visibility=(Number(me._ht_info_customimage.style.opacity)>0||!me._ht_info_customimage.style.opacity)?'inherit':'hidden';
					me._ht_info_customimage.ggSubElement.src=me._ht_info_customimage.ggText;
					me._ht_info_customimage.ggVisible=true;
				}
			}
		}
		me._ht_info_customimage.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._ht_info_customimage.clientWidth;
			var parentHeight = me._ht_info_customimage.clientHeight;
			var img = me._ht_info_customimage__img;
			var aspectRatioDiv = me._ht_info_customimage.clientWidth / me._ht_info_customimage.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			if (img.naturalWidth < parentWidth) parentWidth = img.naturalWidth;
			if (img.naturalHeight < parentHeight) parentHeight = img.naturalHeight;
			var currentWidth,currentHeight;
			if ((hotspot) && (hotspot.customimage)) {
				currentWidth  = hotspot.customimagewidth;
				currentHeight = hotspot.customimageheight;
			}
			img.style.left='50%';
			img.style.marginLeft='-' + currentWidth/2 + 'px';
			img.style.top='50%';
			img.style.marginTop='-' + currentHeight/2 + 'px';
		}
		me._ht_info_vr.appendChild(me._ht_info_customimage);
		el=me._external_vr_gif=document.createElement('div');
		els=me._external_vr_gif__img=document.createElement('img');
		els.className='ggskin ggskin_external';
		els.setAttribute('style','position: absolute;-webkit-user-drag:none;pointer-events:none;;');
		els.onload=function() {me._external_vr_gif.ggUpdatePosition();}
		el.ggText=basePath + "assets/Gif/ar.gif";
		els.setAttribute('src', el.ggText);
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		hs ='';
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="External_VR_GIF";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_external ";
		el.ggType='external';
		hs ='';
		hs+='border : 0px solid #000000;';
		hs+='cursor : pointer;';
		hs+='height : 32px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._external_vr_gif.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._external_vr_gif.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._external_vr_gif.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._external_vr_gif.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._external_vr_gif.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._external_vr_gif.ggCurrentLogicStateScaling == 0) {
					me._external_vr_gif.ggParameter.sx = 1;
					me._external_vr_gif.ggParameter.sy = 1;
					me._external_vr_gif.style[domTransform]=parameterToTransform(me._external_vr_gif.ggParameter);
				}
				else if (me._external_vr_gif.ggCurrentLogicStateScaling == 1) {
					me._external_vr_gif.ggParameter.sx = 0.7;
					me._external_vr_gif.ggParameter.sy = 0.7;
					me._external_vr_gif.style[domTransform]=parameterToTransform(me._external_vr_gif.ggParameter);
				}
				else {
					me._external_vr_gif.ggParameter.sx = 1;
					me._external_vr_gif.ggParameter.sy = 1;
					me._external_vr_gif.style[domTransform]=parameterToTransform(me._external_vr_gif.ggParameter);
				}
			}
		}
		me._external_vr_gif.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			var parentWidth = me._external_vr_gif.clientWidth;
			var parentHeight = me._external_vr_gif.clientHeight;
			var img = me._external_vr_gif__img;
			var aspectRatioDiv = me._external_vr_gif.clientWidth / me._external_vr_gif.clientHeight;
			var aspectRatioImg = img.naturalWidth / img.naturalHeight;
			var currentWidth,currentHeight;
			currentWidth = parentWidth;
			currentHeight = parentHeight;
			img.style.width=parentWidth + 'px';
			img.style.height=parentHeight + 'px';
			img.style.left='50%';
			img.style.marginLeft='-' + currentWidth/2 + 'px';
			img.style.top='50%';
			img.style.marginTop='-' + currentHeight/2 + 'px';
		}
		me._ht_info_vr.appendChild(me._external_vr_gif);
		if ((hotspot) && (hotspot.customimage)) {
			el.style.width=hotspot.customimagewidth + 'px';
			el.style.height=hotspot.customimageheight + 'px';
		}
		me.__div = me._ht_info_vr;
	};
	function SkinHotspotClass_ht_near_0(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_0=document.createElement('div');
		el.ggId="ht_near_0*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_0.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_0.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_0.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_0.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_0.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_0.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_9=document.createElement('div');
		el.ggId="Container 9";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_9.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_9.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_9.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_9.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_9.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_9.ggCurrentLogicStateScaling == 0) {
					me._container_9.ggParameter.sx = 1;
					me._container_9.ggParameter.sy = 1;
					me._container_9.style[domTransform]=parameterToTransform(me._container_9.ggParameter);
				}
				else if (me._container_9.ggCurrentLogicStateScaling == 1) {
					me._container_9.ggParameter.sx = 0.65;
					me._container_9.ggParameter.sy = 0.65;
					me._container_9.style[domTransform]=parameterToTransform(me._container_9.ggParameter);
				}
				else {
					me._container_9.ggParameter.sx = 1;
					me._container_9.ggParameter.sy = 1;
					me._container_9.style[domTransform]=parameterToTransform(me._container_9.ggParameter);
				}
			}
		}
		me._container_9.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b7=document.createElement('div');
		els=me._arrowdouble_b7__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b7';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(0deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b7.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b7.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b7.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b7.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b7.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b7.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b7.style.opacity == 0.0) { me._arrowdouble_b7.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b7.style.opacity=0;
				}
				else {
					me._arrowdouble_b7.style.visibility=me._arrowdouble_b7.ggVisible?'inherit':'hidden';
					me._arrowdouble_b7.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b7.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_9.appendChild(me._arrowdouble_b7);
		el=me._arrowdouble_a7=document.createElement('div');
		els=me._arrowdouble_a7__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a7';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(0deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a7.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a7.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a7.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a7.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a7.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a7.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a7.style.visibility=me._arrowdouble_a7.ggVisible?'inherit':'hidden';
					me._arrowdouble_a7.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a7.style.opacity == 0.0) { me._arrowdouble_a7.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a7.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a7.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_9.appendChild(me._arrowdouble_a7);
		me._ht_near_0.appendChild(me._container_9);
		me.__div = me._ht_near_0;
	};
	function SkinHotspotClass_ht_near_45(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_45=document.createElement('div');
		el.ggId="ht_near_45*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_45.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_45.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_45.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_45.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_45.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_45.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_8=document.createElement('div');
		el.ggId="Container 8";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_8.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_8.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_8.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_8.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_8.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_8.ggCurrentLogicStateScaling == 0) {
					me._container_8.ggParameter.sx = 1;
					me._container_8.ggParameter.sy = 1;
					me._container_8.style[domTransform]=parameterToTransform(me._container_8.ggParameter);
				}
				else if (me._container_8.ggCurrentLogicStateScaling == 1) {
					me._container_8.ggParameter.sx = 0.65;
					me._container_8.ggParameter.sy = 0.65;
					me._container_8.style[domTransform]=parameterToTransform(me._container_8.ggParameter);
				}
				else {
					me._container_8.ggParameter.sx = 1;
					me._container_8.ggParameter.sy = 1;
					me._container_8.style[domTransform]=parameterToTransform(me._container_8.ggParameter);
				}
			}
		}
		me._container_8.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b6=document.createElement('div');
		els=me._arrowdouble_b6__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b6';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(45deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b6.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b6.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b6.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b6.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b6.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b6.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b6.style.opacity == 0.0) { me._arrowdouble_b6.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b6.style.opacity=0;
				}
				else {
					me._arrowdouble_b6.style.visibility=me._arrowdouble_b6.ggVisible?'inherit':'hidden';
					me._arrowdouble_b6.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b6.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_8.appendChild(me._arrowdouble_b6);
		el=me._arrowdouble_a6=document.createElement('div');
		els=me._arrowdouble_a6__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a6';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(45deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a6.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a6.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a6.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a6.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a6.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a6.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a6.style.visibility=me._arrowdouble_a6.ggVisible?'inherit':'hidden';
					me._arrowdouble_a6.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a6.style.opacity == 0.0) { me._arrowdouble_a6.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a6.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a6.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_8.appendChild(me._arrowdouble_a6);
		me._ht_near_45.appendChild(me._container_8);
		me.__div = me._ht_near_45;
	};
	function SkinHotspotClass_ht_near_90(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_90=document.createElement('div');
		el.ggId="ht_near_90*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_90.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_90.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_90.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_90.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_90.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_90.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_7=document.createElement('div');
		el.ggId="Container 7";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_7.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_7.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_7.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_7.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_7.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_7.ggCurrentLogicStateScaling == 0) {
					me._container_7.ggParameter.sx = 1;
					me._container_7.ggParameter.sy = 1;
					me._container_7.style[domTransform]=parameterToTransform(me._container_7.ggParameter);
				}
				else if (me._container_7.ggCurrentLogicStateScaling == 1) {
					me._container_7.ggParameter.sx = 0.65;
					me._container_7.ggParameter.sy = 0.65;
					me._container_7.style[domTransform]=parameterToTransform(me._container_7.ggParameter);
				}
				else {
					me._container_7.ggParameter.sx = 1;
					me._container_7.ggParameter.sy = 1;
					me._container_7.style[domTransform]=parameterToTransform(me._container_7.ggParameter);
				}
			}
		}
		me._container_7.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b5=document.createElement('div');
		els=me._arrowdouble_b5__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b5';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(90deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b5.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b5.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b5.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b5.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b5.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b5.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b5.style.opacity == 0.0) { me._arrowdouble_b5.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b5.style.opacity=0;
				}
				else {
					me._arrowdouble_b5.style.visibility=me._arrowdouble_b5.ggVisible?'inherit':'hidden';
					me._arrowdouble_b5.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b5.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_7.appendChild(me._arrowdouble_b5);
		el=me._arrowdouble_a5=document.createElement('div');
		els=me._arrowdouble_a5__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a5';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(90deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a5.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a5.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a5.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a5.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a5.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a5.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a5.style.visibility=me._arrowdouble_a5.ggVisible?'inherit':'hidden';
					me._arrowdouble_a5.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a5.style.opacity == 0.0) { me._arrowdouble_a5.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a5.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a5.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_7.appendChild(me._arrowdouble_a5);
		me._ht_near_90.appendChild(me._container_7);
		me.__div = me._ht_near_90;
	};
	function SkinHotspotClass_ht_near_135(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_135=document.createElement('div');
		el.ggId="ht_near_135*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_135.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_135.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_135.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_135.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_135.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_135.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_6=document.createElement('div');
		el.ggId="Container 6";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_6.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_6.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_6.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_6.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_6.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_6.ggCurrentLogicStateScaling == 0) {
					me._container_6.ggParameter.sx = 1;
					me._container_6.ggParameter.sy = 1;
					me._container_6.style[domTransform]=parameterToTransform(me._container_6.ggParameter);
				}
				else if (me._container_6.ggCurrentLogicStateScaling == 1) {
					me._container_6.ggParameter.sx = 0.65;
					me._container_6.ggParameter.sy = 0.65;
					me._container_6.style[domTransform]=parameterToTransform(me._container_6.ggParameter);
				}
				else {
					me._container_6.ggParameter.sx = 1;
					me._container_6.ggParameter.sy = 1;
					me._container_6.style[domTransform]=parameterToTransform(me._container_6.ggParameter);
				}
			}
		}
		me._container_6.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b4=document.createElement('div');
		els=me._arrowdouble_b4__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b4';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(135deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b4.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b4.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b4.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b4.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b4.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b4.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b4.style.opacity == 0.0) { me._arrowdouble_b4.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b4.style.opacity=0;
				}
				else {
					me._arrowdouble_b4.style.visibility=me._arrowdouble_b4.ggVisible?'inherit':'hidden';
					me._arrowdouble_b4.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b4.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_6.appendChild(me._arrowdouble_b4);
		el=me._arrowdouble_a4=document.createElement('div');
		els=me._arrowdouble_a4__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a4';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(135deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a4.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a4.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a4.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a4.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a4.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a4.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a4.style.visibility=me._arrowdouble_a4.ggVisible?'inherit':'hidden';
					me._arrowdouble_a4.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a4.style.opacity == 0.0) { me._arrowdouble_a4.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a4.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a4.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_6.appendChild(me._arrowdouble_a4);
		me._ht_near_135.appendChild(me._container_6);
		me.__div = me._ht_near_135;
	};
	function SkinHotspotClass_ht_near_180(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_180=document.createElement('div');
		el.ggId="ht_near_180*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_180.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_180.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_180.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_180.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_180.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_180.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_5=document.createElement('div');
		el.ggId="Container 5";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_5.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_5.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_5.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_5.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_5.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_5.ggCurrentLogicStateScaling == 0) {
					me._container_5.ggParameter.sx = 1;
					me._container_5.ggParameter.sy = 1;
					me._container_5.style[domTransform]=parameterToTransform(me._container_5.ggParameter);
				}
				else if (me._container_5.ggCurrentLogicStateScaling == 1) {
					me._container_5.ggParameter.sx = 0.65;
					me._container_5.ggParameter.sy = 0.65;
					me._container_5.style[domTransform]=parameterToTransform(me._container_5.ggParameter);
				}
				else {
					me._container_5.ggParameter.sx = 1;
					me._container_5.ggParameter.sy = 1;
					me._container_5.style[domTransform]=parameterToTransform(me._container_5.ggParameter);
				}
			}
		}
		me._container_5.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b3=document.createElement('div');
		els=me._arrowdouble_b3__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b3';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(180deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b3.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b3.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b3.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b3.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b3.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b3.style.opacity == 0.0) { me._arrowdouble_b3.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b3.style.opacity=0;
				}
				else {
					me._arrowdouble_b3.style.visibility=me._arrowdouble_b3.ggVisible?'inherit':'hidden';
					me._arrowdouble_b3.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_5.appendChild(me._arrowdouble_b3);
		el=me._arrowdouble_a3=document.createElement('div');
		els=me._arrowdouble_a3__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a3';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(180deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a3.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a3.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a3.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a3.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a3.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a3.style.visibility=me._arrowdouble_a3.ggVisible?'inherit':'hidden';
					me._arrowdouble_a3.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a3.style.opacity == 0.0) { me._arrowdouble_a3.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a3.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a3.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_5.appendChild(me._arrowdouble_a3);
		me._ht_near_180.appendChild(me._container_5);
		me.__div = me._ht_near_180;
	};
	function SkinHotspotClass_ht_near_225(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_225=document.createElement('div');
		el.ggId="ht_near_225*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_225.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_225.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_225.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_225.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_225.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_225.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_4=document.createElement('div');
		el.ggId="Container 4";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_4.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_4.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_4.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_4.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_4.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_4.ggCurrentLogicStateScaling == 0) {
					me._container_4.ggParameter.sx = 1;
					me._container_4.ggParameter.sy = 1;
					me._container_4.style[domTransform]=parameterToTransform(me._container_4.ggParameter);
				}
				else if (me._container_4.ggCurrentLogicStateScaling == 1) {
					me._container_4.ggParameter.sx = 0.65;
					me._container_4.ggParameter.sy = 0.65;
					me._container_4.style[domTransform]=parameterToTransform(me._container_4.ggParameter);
				}
				else {
					me._container_4.ggParameter.sx = 1;
					me._container_4.ggParameter.sy = 1;
					me._container_4.style[domTransform]=parameterToTransform(me._container_4.ggParameter);
				}
			}
		}
		me._container_4.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b2=document.createElement('div');
		els=me._arrowdouble_b2__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b2';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(225deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b2.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b2.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b2.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b2.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b2.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b2.style.opacity == 0.0) { me._arrowdouble_b2.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b2.style.opacity=0;
				}
				else {
					me._arrowdouble_b2.style.visibility=me._arrowdouble_b2.ggVisible?'inherit':'hidden';
					me._arrowdouble_b2.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_4.appendChild(me._arrowdouble_b2);
		el=me._arrowdouble_a2=document.createElement('div');
		els=me._arrowdouble_a2__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a2';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(225deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a2.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a2.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a2.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a2.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a2.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a2.style.visibility=me._arrowdouble_a2.ggVisible?'inherit':'hidden';
					me._arrowdouble_a2.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a2.style.opacity == 0.0) { me._arrowdouble_a2.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a2.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_4.appendChild(me._arrowdouble_a2);
		me._ht_near_225.appendChild(me._container_4);
		me.__div = me._ht_near_225;
	};
	function SkinHotspotClass_ht_near_270(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_270=document.createElement('div');
		el.ggId="ht_near_270*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_270.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_270.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_270.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_270.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_270.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_270.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_1=document.createElement('div');
		el.ggId="Container 1";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_1.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_1.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_1.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_1.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_1.ggCurrentLogicStateScaling == 0) {
					me._container_1.ggParameter.sx = 1;
					me._container_1.ggParameter.sy = 1;
					me._container_1.style[domTransform]=parameterToTransform(me._container_1.ggParameter);
				}
				else if (me._container_1.ggCurrentLogicStateScaling == 1) {
					me._container_1.ggParameter.sx = 0.65;
					me._container_1.ggParameter.sy = 0.65;
					me._container_1.style[domTransform]=parameterToTransform(me._container_1.ggParameter);
				}
				else {
					me._container_1.ggParameter.sx = 1;
					me._container_1.ggParameter.sy = 1;
					me._container_1.style[domTransform]=parameterToTransform(me._container_1.ggParameter);
				}
			}
		}
		me._container_1.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b1=document.createElement('div');
		els=me._arrowdouble_b1__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b1';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(270deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b1.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b1.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b1.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b1.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b1.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b1.style.opacity == 0.0) { me._arrowdouble_b1.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b1.style.opacity=0;
				}
				else {
					me._arrowdouble_b1.style.visibility=me._arrowdouble_b1.ggVisible?'inherit':'hidden';
					me._arrowdouble_b1.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_1.appendChild(me._arrowdouble_b1);
		el=me._arrowdouble_a1=document.createElement('div');
		els=me._arrowdouble_a1__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a1';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(270deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a1.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a1.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a1.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a1.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a1.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a1.style.visibility=me._arrowdouble_a1.ggVisible?'inherit':'hidden';
					me._arrowdouble_a1.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a1.style.opacity == 0.0) { me._arrowdouble_a1.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a1.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a1.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_1.appendChild(me._arrowdouble_a1);
		me._ht_near_270.appendChild(me._container_1);
		me.__div = me._ht_near_270;
	};
	function SkinHotspotClass_ht_near_300(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_300=document.createElement('div');
		el.ggId="ht_near_300*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_300.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_300.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_300.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_300.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_300.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_300.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_2=document.createElement('div');
		el.ggId="Container 2";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_2.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_2.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_2.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_2.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_2.ggCurrentLogicStateScaling == 0) {
					me._container_2.ggParameter.sx = 1;
					me._container_2.ggParameter.sy = 1;
					me._container_2.style[domTransform]=parameterToTransform(me._container_2.ggParameter);
				}
				else if (me._container_2.ggCurrentLogicStateScaling == 1) {
					me._container_2.ggParameter.sx = 0.65;
					me._container_2.ggParameter.sy = 0.65;
					me._container_2.style[domTransform]=parameterToTransform(me._container_2.ggParameter);
				}
				else {
					me._container_2.ggParameter.sx = 1;
					me._container_2.ggParameter.sy = 1;
					me._container_2.style[domTransform]=parameterToTransform(me._container_2.ggParameter);
				}
			}
		}
		me._container_2.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b0=document.createElement('div');
		els=me._arrowdouble_b0__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b0';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(300deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b0.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b0.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b0.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b0.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b0.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b0.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b0.style.opacity == 0.0) { me._arrowdouble_b0.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b0.style.opacity=0;
				}
				else {
					me._arrowdouble_b0.style.visibility=me._arrowdouble_b0.ggVisible?'inherit':'hidden';
					me._arrowdouble_b0.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b0.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_2.appendChild(me._arrowdouble_b0);
		el=me._arrowdouble_a0=document.createElement('div');
		els=me._arrowdouble_a0__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a0';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(300deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a0.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a0.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a0.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a0.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a0.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a0.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a0.style.visibility=me._arrowdouble_a0.ggVisible?'inherit':'hidden';
					me._arrowdouble_a0.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a0.style.opacity == 0.0) { me._arrowdouble_a0.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a0.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a0.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_2.appendChild(me._arrowdouble_a0);
		me._ht_near_300.appendChild(me._container_2);
		me.__div = me._ht_near_300;
	};
	function SkinHotspotClass_ht_near_315(parentScope,hotspot) {
		var me=this;
		var flag=false;
		var hs='';
		me.parentScope=parentScope;
		me.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):''; // }
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		el=me._ht_near_315=document.createElement('div');
		el.ggId="ht_near_315*";
		el.ggDx=-50;
		el.ggDy=-10;
		el.ggParameter={ rx:0,ry:0,a:90,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_hotspot ";
		el.ggType='hotspot';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 0px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 0px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		el.style[domTransform]=parameterToTransform(el.ggParameter);
		me._ht_near_315.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			if (me.hotspot.url!='' && me.hotspot.url.charAt(0)=='{') { // }
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			} else {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				} else {
					return player.getCurrentNode();
				}
			}
		}
		me._ht_near_315.onclick=function (e) {
			player.openNext(me.hotspot.url,me.hotspot.target);
			skin.hotspotProxyClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_315.ondblclick=function (e) {
			skin.hotspotProxyDoubleClick(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_315.onmouseover=function (e) {
			player.setActiveHotspot(me.hotspot);
			skin.hotspotProxyOver(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_315.onmouseout=function (e) {
			player.setActiveHotspot(null);
			skin.hotspotProxyOut(me.hotspot.id, me.hotspot.url);
		}
		me._ht_near_315.ggUpdatePosition=function (useTransition) {
		}
		el=me._container_3=document.createElement('div');
		el.ggId="Container 3";
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -25px;';
		hs+='position : absolute;';
		hs+='top : -25px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._container_3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._container_3.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((player.getViewerSize().width >= 720))
			)
			{
				newLogicStateScaling = 0;
			}
			else if (
				((player.getViewerSize().width > 350))
			)
			{
				newLogicStateScaling = 1;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._container_3.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._container_3.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._container_3.style[domTransition]='' + cssPrefix + 'transform 0s';
				if (me._container_3.ggCurrentLogicStateScaling == 0) {
					me._container_3.ggParameter.sx = 1;
					me._container_3.ggParameter.sy = 1;
					me._container_3.style[domTransform]=parameterToTransform(me._container_3.ggParameter);
				}
				else if (me._container_3.ggCurrentLogicStateScaling == 1) {
					me._container_3.ggParameter.sx = 0.65;
					me._container_3.ggParameter.sy = 0.65;
					me._container_3.style[domTransform]=parameterToTransform(me._container_3.ggParameter);
				}
				else {
					me._container_3.ggParameter.sx = 1;
					me._container_3.ggParameter.sy = 1;
					me._container_3.style[domTransform]=parameterToTransform(me._container_3.ggParameter);
				}
			}
		}
		me._container_3.ggUpdatePosition=function (useTransition) {
		}
		el=me._arrowdouble_b=document.createElement('div');
		els=me._arrowdouble_b__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_b';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGIklEQVRoge2aeYgbVRzHP292MsnE2lULRbsVW1fFC916VEURvG/rVRWPSsUD8cSiKKXihQeKFypSsVpRSq21CooitSreZwt7mGObTbRxMVG73XaP5vr5x8usY91sXrK7ukq/MCST97u+836/N7/3iBIR/g+w/u0AxgrbiUw0bCdSA44sX+OK8SZyFfBF+Zo/rp5EZDyukIiskL9jmYg44+FzPEjMFpH0MCQ8bBCRlrH2O9apdSfwFTBtBJkmYC1w25h6HqMnsrOIrBlhFiphtYhMnigzcirQBRxXh+4JwI/ASaOOYpRP4rE6ZqESHh1NLPUq7iUi348hCQ/fiMjMemKqJ7WuACLALEP5B4CHDGUPA+LA5TVHVQPrgIi8WsPT7RWROT7980RkSw36L4'+
			'tIg2l8piRmiUiqhiA+EZFdh7Gzm4h8XoOdLhE5eKyILKjBsYjI3Z5usVgkHo8Tj8cpFot+m/fXaPPmanEqkYo7xMnACuBkwyzNAOcBnwFs2rSJZDJJLpcDwHEcZsyYQWNjoyd/LPAGMMXQ/nvAhcDm4QYrETkBeB3YydDJW8AlQL+IkMlkSKfTlEolHMdpAsjlcmnLsmhqamLq1KkopQDCwHLgTEM/v5XJrNl2YLhV62FgdQ0kbgDOAfoBEokEnZ2dWJZFMBg8GogC0WAwONuyLDo7O0kkEp5uP3AWcJOhrynAB8B92w74Z2RPYBkw29BoBJgLtIFOpa6uLgqFAo7jACwC7t1G507goVwuh23bzJw5059qB6JTbW9D/58ClwEp+OuMnFgDieeB/T0SyWSSaDTqpdIuwMfDkAB4EFjjOM6UUqlENBolmUx6Y23AfsCL'+
			'hjEcA5zi3fiJlAyUB4ALgGsA6evrIxKJkM1mCQaD2LZ9Nrp3OnYEG8cBCdu2zwgGg2SzWSKRCH19fQBF4Ep0HWw1iGcoZj+RagdcXwLNwEqAnp4eYrEYvb29hEIhlFLPoot+B4MAJgNvK6WeDoVC9Pb2EovF6Onp8cZXlH19XcXOUMymLcoDwFFAd6FQoKuri2g0ilIK13WbRaQNuM7Qlh/Xi8ha13WblVJEo9GhOgPSwBHAIyaGqhH5HTgeWAgwMDBAa2sr2WwW13WxLGu+iESAA+og4aFFRDosy5rnui7ZbJbW1lYGBga88dvRtbCxXiLvAjOADwG6u7vp6OhARHBdN4Re4ZYA9ihIeHCApcArrusGRYSOjg66u7u98ffRq+p7lQz4iSjf91uA04HN+XyeRCJBKpXCsiwCgcBhIrIeuNgwyDfQtWOCS0UkHggEDr'+
			'Usi1QqRSKRIJ/PA/QApwG3Dhezn8iUsvAhwJMAGzdupL29nUwmQzgcpqGh4Q4R+YaR9+R+LATOR78wFxnq7C4i3zY0NCwIh8NkMhna2toYHBz0xh9Hn5P1ArsMMfK9EHcHcsAvAOl0mlQqRSgUIhAINIrISnTrYoJu4Fz0QYQfR6NnaKqhnfeVUhfk8/nNxWKRpqYmpk0beobT0CmZ3JYIAIODg6xfv57+/n4cx0EpdQrwGnrJNMEqdNrlKoy76PqaY2ivB5hbKpVW5/N5wuEwzc3NhEKhvwgNEcnlcmzZsoUNGzawdetWgsEgwBPAzYYOBbgaeMFQ/lrgOUNZgMeABV5s06dPZ9KkSV479CeRdevWUSgUvILeU0RWAQcZOmlH10FnDYEB7AO8iW5NTPCdUur8fD6fKpVK2LZNS0sL4Ct2pRSO42Db9jwR+QFzEk+j'+
			'G75aSQDEyrqLDeUPFZGYbduXltN+aMBPpBG9li9FF1E19KE3UjeaRl0BJXSaXVy2WQ0O8AqwRCm1o/ejf/mdC8wzdP4ZOi1WGcqbYDk6xb40lJ8PXOTd1HMcdA+6hf65Dt1q+And0/1t41QNtXS/v6LfA3fX6qQO3IVu93+rIldz9/sW+hT98/riqgsfAXsA75gImxDx9uSVXnDjiT70wUTVd9lIRGLo4ntmjIIaDZ5CL9PxSgKViCwG9kUfMEwUtKMf7EvDDfqJ7Fz+vAy9rk/E/3YU0cvuFeX7oSMr/6ZoDXA48O0/F1fdeBnowHf4MNKR6X8K2//5MNGwnchEwx81ngryhLvmbwAAAABJRU5ErkJggg==';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_b";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(315deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_b.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_b.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_b.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_b.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_b.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_b.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._arrowdouble_b.style.opacity == 0.0) { me._arrowdouble_b.style.visibility="hidden"; } }, 505);
					me._arrowdouble_b.style.opacity=0;
				}
				else {
					me._arrowdouble_b.style.visibility=me._arrowdouble_b.ggVisible?'inherit':'hidden';
					me._arrowdouble_b.style.opacity=1;
				}
			}
		}
		me._arrowdouble_b.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_3.appendChild(me._arrowdouble_b);
		el=me._arrowdouble_a=document.createElement('div');
		els=me._arrowdouble_a__img=document.createElement('img');
		els.className='ggskin ggskin_arrowdouble_a';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAGOUlEQVRoge2af4xcVRXHP2dld3Znd2lndoeIy+62O/Nmp7PbGX4ZlZYSQhtNrBVo1KiNaWwLIWI1KhB/YYQGys9AINEStPGPNlpNNGkTaGiNIIoJUGJXbTSl1LI0EkrBMD/evpl5xz/uzmRsht37Zmb50fBNXvZl3r3nnM/eH+fcNyOqytmgjnc7gHbpA5D3ms4akHOqN4cPH14oH5cCIeBPC2E8k8kACz8iXwKeBZ4GblxIRwsF0gHsBHbVffYQsBfoWyiH7dYkcBTY2ODZWuAlYEW7nbYb5EZgClg6R5tBzFS7rZ2O2wUSxkybhwL0+SFmAzivHQG0A2QVcAIzbYLqMsxUu7rVIFoF2QY8CQy0YCMM/BZ4uJVAmgUZwkyL77fi/Ax9DfgbkGqmcz'+
			'Mg6zG70mWW7e/DrAcbTQD/AK4LGlRQkEeA3wDdFm1dYIOqfkdVtwGfAt6w6CfAjlk/PbaB2YKkgH8CWyzbHxKR8Uqlssv3fXzfp1wu7wcSwEFLG+uBY8DHbRrbgFwHHAGSlgHcKyKXFAqFEyJCKpVifHyczs5OPM87DawGvmtp68PAM1isxblAujHDu8PS6VsisrpSqdyUz+eJRqOkUil6enoIh8NMTk4Si8XI5XJUKpXtIvJR4KSl7W3AHzDJNBDISsywrrd0dAAYcV33IEA8HsdxHLq7u8EkvAtEhNHRURzHQVUpFArPichSYI+ljytmY/q0LcitwB+B8y0d3Ays8Tzvza6uLpLJJLFYrPrsQswudAy4EiAWizExMUEkEiGfz3uq+gUR2QzYvDzoB/YBD8wFcj3wBPBjS4B/i8jFvu/fk8vlGBgYIJvN0tvbW33+'+
			'LeAFTLLsBH4P3A4QCoVIJpMMDQ3heR6lUulnIpLC5BEbfQM4BDiNQK7BLEQb7RYRx3XdF1SVZDLJ0qW1OvFc4DFM/jhTPwD+glnEDA8Pk0ql8H0f13X/JSLLsc/wFzE7ymeC2KgsIht93/9yPp8vRSIR0uk0g4O1NXgVcByTM95OH8PUV58D6O/vZ/ny5dWphu/7XxeRzwBvWcTjNwPyVyBZKpV+4fs+Q0NDjI2NEQqFqs/vwiz6iIWtbswifxToCIVCxONxEokEvu8zMzOzb3YjeGoeO7V1ZQvyIHCh53kvqSrxeJzh4WE6OjoARjHH2ZstbdVrEyZHLRMRBgcHcRyHnp4eXNd9HbNT3WpjaD6QvIis833/m8VikcWLF5PJZFi0aFH1+ecxu9KlTUBUlQT+jika6e/vJ51OE4lEKBaL+L5/u4isAP7TLMhTIhJ3XX'+
			'ev53k4jkMikaCzsxPgQ5hp8StMGd6qBLPIfwf0iwiJRALHcfA8D9d1/ywiY5hyf16Q+vvvqeoVhULh1XA4TDqdJhqNVp9NYKbDJssg9wK/tGz7WeBFTEImGo2STqfp7e0ll8sVVfVaEbmhrr00Cr4LOAWsKpfLd87MzDAwMMCyZcvo66u9+NiK2esd7HQHsA74Iiav2CiGScg/Aujr62N8fJyRkRFKpRKe5/0UyACvUVeySPVt/NTU1AhQ9jzvpIiwZMmS+lHowfxX11kG8wYmLz15xucXYTLzRyztPD1r5xTA6dOnOX78OKpKV1dXTFW7MpnMK1A3IsVi8UQ+nz8ZDofJZrP1EJdjzuS2EPuBkQYQYDL9KLDb0tZKYLrqOxqNks1mCYfD5PP510ql0iu1lqqKqnL06FGmp6epVCq1z1T1Ng2mrXV957s2qGo5gO2f'+
			'VPtWKhWmp6c5cuRIzd7bORlW1WcCOHlRVTMBIKrXqKoeCuBnSlXHG9lqZHy9quYCGP+5qp7TBET9dX8Af76qbpkL5JOq+nAAgzOq+pUWAeqvtar6egD/e1Q10gjk8QBGDqnqWBshqtegqh4IEMfmat9mXgfdD1yMOSy1W6cIdqavKQjIm8Aa4NtBnTSh7Zj6bc76iiaq34PAEkyZ/k7peUw++rVNYxuQWzDD/d8WgmpWJUyF/dX5Gs4F8jJmeO9uU1CtaCcQx5T7DVUPInX3uzCF4fMLE1dTOob5NqzhdzD1IOfO/r0B2ADMLGxcTWsrs+d96o7VteoX+ARQxhxb3w+axMyiKfh/kPe1zppfPnwA8l7TWQPyP/Xx7oa5aBfaAAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="ArrowDouble_a";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='height : 50px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 50px;';
		hs+='pointer-events:auto;';
		hs+='transform: rotateY(-60deg) rotateZ(315deg)';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._arrowdouble_a.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._arrowdouble_a.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((player.getVariableValue('ht_anima_C') == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._arrowdouble_a.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._arrowdouble_a.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._arrowdouble_a.style[domTransition]='opacity 500ms ease 0ms';
				if (me._arrowdouble_a.ggCurrentLogicStateAlpha == 0) {
					me._arrowdouble_a.style.visibility=me._arrowdouble_a.ggVisible?'inherit':'hidden';
					me._arrowdouble_a.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._arrowdouble_a.style.opacity == 0.0) { me._arrowdouble_a.style.visibility="hidden"; } }, 505);
					me._arrowdouble_a.style.opacity=0;
				}
			}
		}
		me._arrowdouble_a.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._container_3.appendChild(me._arrowdouble_a);
		me._ht_near_315.appendChild(me._container_3);
		me.__div = me._ht_near_315;
	};
	me.addSkinHotspot=function(hotspot) {
		var hsinst = null;
		if (hotspot.skinid=='Hotspot_npc') {
			hotspot.skinid = 'Hotspot_npc';
			hsinst = new SkinHotspotClass_hotspot_npc(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_hotspot_npc_sizechanged();;
			me.callChildLogicBlocksHotspot_hotspot_npc_changenode();;
			me.callChildLogicBlocksHotspot_hotspot_npc_changevisitednodes();;
			me.callChildLogicBlocksHotspot_hotspot_npc_activehotspotchanged();;
		} else
		if (hotspot.skinid=='ht_info_延讀資訊2') {
			hotspot.skinid = 'ht_info_延讀資訊2';
			hsinst = new SkinHotspotClass_ht_info_2(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_info_2_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_info_2_changenode();;
			me.callChildLogicBlocksHotspot_ht_info_2_activehotspotchanged();;
		} else
		if (hotspot.skinid=='ht_info_延讀資訊') {
			hotspot.skinid = 'ht_info_延讀資訊';
			hsinst = new SkinHotspotClass_ht_info_(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_info__sizechanged();;
			me.callChildLogicBlocksHotspot_ht_info__changenode();;
			me.callChildLogicBlocksHotspot_ht_info__activehotspotchanged();;
		} else
		if (hotspot.skinid=='ht_info_崁入(VR)') {
			hotspot.skinid = 'ht_info_崁入(VR)';
			hsinst = new SkinHotspotClass_ht_info_vr(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_info_vr_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_info_vr_changenode();;
			me.callChildLogicBlocksHotspot_ht_info_vr_activehotspotchanged();;
		} else
		if (hotspot.skinid=='ht_near_0*') {
			hotspot.skinid = 'ht_near_0*';
			hsinst = new SkinHotspotClass_ht_near_0(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_0_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_0_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_0_varchanged_ht_anima_C();;
		} else
		if (hotspot.skinid=='ht_near_45*') {
			hotspot.skinid = 'ht_near_45*';
			hsinst = new SkinHotspotClass_ht_near_45(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_45_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_45_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_45_varchanged_ht_anima_C();;
		} else
		if (hotspot.skinid=='ht_near_90*') {
			hotspot.skinid = 'ht_near_90*';
			hsinst = new SkinHotspotClass_ht_near_90(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_90_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_90_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_90_varchanged_ht_anima_C();;
		} else
		if (hotspot.skinid=='ht_near_135*') {
			hotspot.skinid = 'ht_near_135*';
			hsinst = new SkinHotspotClass_ht_near_135(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_135_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_135_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_135_varchanged_ht_anima_C();;
		} else
		if (hotspot.skinid=='ht_near_180*') {
			hotspot.skinid = 'ht_near_180*';
			hsinst = new SkinHotspotClass_ht_near_180(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_180_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_180_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_180_varchanged_ht_anima_C();;
		} else
		if (hotspot.skinid=='ht_near_225*') {
			hotspot.skinid = 'ht_near_225*';
			hsinst = new SkinHotspotClass_ht_near_225(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_225_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_225_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_225_varchanged_ht_anima_C();;
		} else
		if (hotspot.skinid=='ht_near_270*') {
			hotspot.skinid = 'ht_near_270*';
			hsinst = new SkinHotspotClass_ht_near_270(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_270_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_270_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_270_varchanged_ht_anima_C();;
		} else
		if (hotspot.skinid=='ht_near_300*') {
			hotspot.skinid = 'ht_near_300*';
			hsinst = new SkinHotspotClass_ht_near_300(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_300_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_300_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_300_varchanged_ht_anima_C();;
		} else
		{
			hotspot.skinid = 'ht_near_315*';
			hsinst = new SkinHotspotClass_ht_near_315(me, hotspot);
			if (!hotspotTemplates.hasOwnProperty(hotspot.skinid)) {
				hotspotTemplates[hotspot.skinid] = [];
			}
			hotspotTemplates[hotspot.skinid].push(hsinst);
			me.callChildLogicBlocksHotspot_ht_near_315_sizechanged();;
			me.callChildLogicBlocksHotspot_ht_near_315_changenode();;
			me.callChildLogicBlocksHotspot_ht_near_315_varchanged_ht_anima_C();;
		}
		return hsinst;
	}
	me.removeSkinHotspots=function() {
		if(hotspotTemplates['Hotspot_npc']) {
			var i;
			for(i = 0; i < hotspotTemplates['Hotspot_npc'].length; i++) {
				hotspotTemplates['Hotspot_npc'][i] = null;
			}
		}
		if(hotspotTemplates['ht_info_延讀資訊2']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊2'].length; i++) {
				hotspotTemplates['ht_info_延讀資訊2'][i] = null;
			}
		}
		if(hotspotTemplates['ht_info_延讀資訊']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_延讀資訊'].length; i++) {
				hotspotTemplates['ht_info_延讀資訊'][i] = null;
			}
		}
		if(hotspotTemplates['ht_info_崁入(VR)']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_info_崁入(VR)'].length; i++) {
				hotspotTemplates['ht_info_崁入(VR)'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_0*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_0*'].length; i++) {
				hotspotTemplates['ht_near_0*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_45*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_45*'].length; i++) {
				hotspotTemplates['ht_near_45*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_90*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_90*'].length; i++) {
				hotspotTemplates['ht_near_90*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_135*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_135*'].length; i++) {
				hotspotTemplates['ht_near_135*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_180*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_180*'].length; i++) {
				hotspotTemplates['ht_near_180*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_225*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_225*'].length; i++) {
				hotspotTemplates['ht_near_225*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_270*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_270*'].length; i++) {
				hotspotTemplates['ht_near_270*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_300*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_300*'].length; i++) {
				hotspotTemplates['ht_near_300*'][i] = null;
			}
		}
		if(hotspotTemplates['ht_near_315*']) {
			var i;
			for(i = 0; i < hotspotTemplates['ht_near_315*'].length; i++) {
				hotspotTemplates['ht_near_315*'][i] = null;
			}
		}
		hotspotTemplates = [];
	}
	function SkinElement_map_pin_Class(parentScope,ggParent) {
		var me=this;
		var flag=false;
		me.parentScope=parentScope;
		me.ggParent=ggParent;
		var nodeId=ggParent.ggElementNodeId();
		me.ggNodeId=nodeId;
		me.ggUserdata=skin.player.getNodeUserdata(nodeId);
		me.elementMouseDown=[];
		me.elementMouseOver=[];
		
		me.findElements=function(id,regex) {
			return skin.findElements(id,regex);
		}
		
		el=me._map_pin=document.createElement('div');
		el.ggId="map_pin";
		el.ggDx=-478;
		el.ggDy=52;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_container ";
		el.ggType='container';
		hs ='';
		hs+='height : 24px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : inherit;';
		hs+='width : 24px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		me._map_pin.ggIsActive=function() {
			return player.getCurrentNode()==this.ggElementNodeId();
		}
		el.ggElementNodeId=function() {
			return me.ggNodeId;
		}
		me._map_pin.onclick=function (e) {
			if (
				(
					((me._map_pin.ggIsActive() == false))
				)
			) {
				player.openNext("{"+me.ggNodeId+"}","");
			}
		}
		me._map_pin.onmouseover=function (e) {
			me.elementMouseOver['map_pin']=true;
			me._map_pin_tt.logicBlock_alpha();
		}
		me._map_pin.onmouseout=function (e) {
			me.elementMouseOver['map_pin']=false;
			me._map_pin_tt.logicBlock_alpha();
		}
		me._map_pin.ontouchend=function (e) {
			me.elementMouseOver['map_pin']=false;
			me._map_pin_tt.logicBlock_alpha();
		}
		me._map_pin.ggUpdatePosition=function (useTransition) {
		}
		el=me._map_pin_active=document.createElement('div');
		els=me._map_pin_active__img=document.createElement('img');
		els.className='ggskin ggskin_map_pin_active';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAACPElEQVQ4ja2VP0wUQRTGf5BLaChMDAWVxPISXOSgo78pbigtKPzTGGI0uSH03lUWZ5zChGBFoKCQzi1YYywO7fhzHAUNhdhgQYPJJcRqLfbNOc7d7WHil2zy9u33vnlvZt7bkXK1Qh4SG48BETAjrmOgrYz+lRc3Mkg4sfEDoArcB1rAoXwqia8JrCmjP9xYOLHxjpiryujvAxa+A7wGOsroJ7nCUvYV8EgZ/b5vKb0LPATeAbf87SkEvKuQkNg4ItvjeXHtA3vK6HMAZfRWYuNPwAVw28WNegI7kqkv+pbssDaB5/JsAt8SG790PGX0D8AkNt74S1gOCr/8xMbHIjQINeE48S1gMrHxop9xFVgNMo1yRB2ixMY1730FeAZAmqZjaZpel6sVytUKaZ'+
			'pG6b9jyotPy9UKBcms5WcRptU5+Mrl9joAE0vLjM8thJR7wLnY7cTGUYGsow490nwYdbm9zs/mbve9j/As4BqlBUSjIeN/oUB2nR57vv2QNLG03Nf2cOTZJaBRANpkve+wF0aNzy30K9/HiWdPK6NPR6UhmtL7SEfV81QC1F0XJjYuAh/hzz1eIxsoiHhNKhmGtnAd3gCNrrCMvo4MFCc+MyTzunCQbJ8CZ8roz9A73a6Bu9L7zjdFdk9nxXUEnLjyhVMEviiju0Oo39i8AIz0/lBIpq980R5hj7wBTAIryujTAYJFsj09U0a/CL/n/ZoWyQZKmewgXduXgGmy02+4Pb2xcLCIG/YAB4Oq8PEbuIUWPg2t/bAAAAAASUVORK5CYII=';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_image';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="map_pin_active";
		el.ggDx=0;
		el.ggDy=0;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_image ";
		el.ggType='image';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 25px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 25px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 100%';
		me._map_pin_active.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._map_pin_active.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((me.elementMouseOver['map_pin_active'] == true))
			)
			{
				newLogicStateScaling = 0;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._map_pin_active.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._map_pin_active.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._map_pin_active.style[domTransition]='' + cssPrefix + 'transform 200ms ease 0ms, opacity 500ms ease 0ms';
				if (me._map_pin_active.ggCurrentLogicStateScaling == 0) {
					me._map_pin_active.ggParameter.sx = 1.1;
					me._map_pin_active.ggParameter.sy = 1.1;
					me._map_pin_active.style[domTransform]=parameterToTransform(me._map_pin_active.ggParameter);
				}
				else {
					me._map_pin_active.ggParameter.sx = 1;
					me._map_pin_active.ggParameter.sy = 1;
					me._map_pin_active.style[domTransform]=parameterToTransform(me._map_pin_active.ggParameter);
				}
			}
		}
		me._map_pin_active.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((me._map_pin_active.ggIsActive() == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._map_pin_active.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._map_pin_active.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._map_pin_active.style[domTransition]='' + cssPrefix + 'transform 200ms ease 0ms, opacity 500ms ease 0ms';
				if (me._map_pin_active.ggCurrentLogicStateAlpha == 0) {
					me._map_pin_active.style.visibility=me._map_pin_active.ggVisible?'inherit':'hidden';
					me._map_pin_active.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._map_pin_active.style.opacity == 0.0) { me._map_pin_active.style.visibility="hidden"; } }, 505);
					me._map_pin_active.style.opacity=0;
				}
			}
		}
		me._map_pin_active.onmouseover=function (e) {
			me.elementMouseOver['map_pin_active']=true;
			me._map_pin_active.logicBlock_scaling();
		}
		me._map_pin_active.onmouseout=function (e) {
			me.elementMouseOver['map_pin_active']=false;
			me._map_pin_active.logicBlock_scaling();
		}
		me._map_pin_active.ontouchend=function (e) {
			me.elementMouseOver['map_pin_active']=false;
			me._map_pin_active.logicBlock_scaling();
		}
		me._map_pin_active.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._map_pin.appendChild(me._map_pin_active);
		el=me._map_pin_normal=document.createElement('div');
		els=me._map_pin_normal__img=document.createElement('img');
		els.className='ggskin ggskin_map_pin_normal';
		hs='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAWCAYAAAAfD8YZAAACNElEQVQ4jYXUTYhVZRzH8c/z3Dtzh3G844hlje8JjSQTLtoIthGUFkngwl0uxH1iIggSVtRCFNqIrgotfNsGlS7cCbkJzEj0Voja4MuEM3qdueK952lx5sjleqb+cDiHP78vv//Lc57wcNebSmI93sYSTOAq/ugVVUvAMWzHOEZwH2twHre7haHLuYqv8SGRLCskxIAMjmJfmfMFwmai7MkdIc1Sq9GapTosDC0jtT8mvYqdELESu7GZIHt0XW3DOxZ9+oPFR34xfOC86qpRaapBqMgrsxW1KrZgGxXZ9J8GNr2vvvf7F+VUXhvTP/6eqc/e9bzxlzA0iuyDwnktVqd2S6wvsGDH5y+PsH/I4PZPCK1iFsuxIiIRUmo9Vhldq/J66epUVoyLI69I7V'+
			'ZhGqJ8j/dC34A0PSl7PFkKp9lpaaYpVPrgHiYififdCLWF2hO3PLv8bSncunhM1mySw1fRiLiSrykJQys1Tx8y+9PhvBvozHh6Zo+ZiyfF+uoifwmN4pAsREOoLvW8KXt6R9+at8Qly3UmbmrfvSXW36DST+r8jI1F4/AEZ6Q21UFxeEz77kPPrlzWmWyJi9YRq6QOnCtaKWD4Jn9lyITBEXFkmTBQR6co9z5OlsG/4ruu+XY9L+IUHpXBcLhs0nPxD450J3rhazg+D/wVHvwXDF+g2ZP7DV/2Csvgv7G/J/eRuR/6/2Dy0n+c+z4hPxQvRdk1VMQhLMbB+QTzOZPfV2cxNZ/gX7kyrmOYdeu1AAAAAElFTkSuQmCC';
		els.setAttribute('src',hs);
		els.ggNormalSrc=hs;
		els.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;;');
		els.className='ggskin ggskin_button';
		els['ondragstart']=function() { return false; };
		player.checkLoaded.push(els);
		el.appendChild(els);
		el.ggSubElement = els;
		el.ggId="map_pin_normal";
		el.ggDx=0;
		el.ggDy=-8;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=false;
		el.className="ggskin ggskin_button ";
		el.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 22px;';
		hs+='left : -10000px;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 15px;';
		hs+='pointer-events:auto;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 100%';
		me._map_pin_normal.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._map_pin_normal.logicBlock_scaling = function() {
			var newLogicStateScaling;
			if (
				((me.elementMouseOver['map_pin_normal'] == true))
			)
			{
				newLogicStateScaling = 0;
			}
			else {
				newLogicStateScaling = -1;
			}
			if (me._map_pin_normal.ggCurrentLogicStateScaling != newLogicStateScaling) {
				me._map_pin_normal.ggCurrentLogicStateScaling = newLogicStateScaling;
				me._map_pin_normal.style[domTransition]='' + cssPrefix + 'transform 200ms ease 0ms, opacity 500ms ease 0ms';
				if (me._map_pin_normal.ggCurrentLogicStateScaling == 0) {
					me._map_pin_normal.ggParameter.sx = 1.1;
					me._map_pin_normal.ggParameter.sy = 1.1;
					me._map_pin_normal.style[domTransform]=parameterToTransform(me._map_pin_normal.ggParameter);
				}
				else {
					me._map_pin_normal.ggParameter.sx = 1;
					me._map_pin_normal.ggParameter.sy = 1;
					me._map_pin_normal.style[domTransform]=parameterToTransform(me._map_pin_normal.ggParameter);
				}
			}
		}
		me._map_pin_normal.logicBlock_visible = function() {
			var newLogicStateVisible;
			if (
				((me.ggUserdata.tags.indexOf("\u9328\u9ede") != -1))
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me._map_pin_normal.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me._map_pin_normal.ggCurrentLogicStateVisible = newLogicStateVisible;
				me._map_pin_normal.style[domTransition]='' + cssPrefix + 'transform 200ms ease 0ms, opacity 500ms ease 0ms';
				if (me._map_pin_normal.ggCurrentLogicStateVisible == 0) {
					me._map_pin_normal.style.visibility=(Number(me._map_pin_normal.style.opacity)>0||!me._map_pin_normal.style.opacity)?'inherit':'hidden';
					me._map_pin_normal.ggVisible=true;
				}
				else {
					me._map_pin_normal.style.visibility="hidden";
					me._map_pin_normal.ggVisible=false;
				}
			}
		}
		me._map_pin_normal.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((me._map_pin_normal.ggIsActive() == true))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._map_pin_normal.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._map_pin_normal.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._map_pin_normal.style[domTransition]='' + cssPrefix + 'transform 200ms ease 0ms, opacity 500ms ease 0ms';
				if (me._map_pin_normal.ggCurrentLogicStateAlpha == 0) {
					setTimeout(function() { if (me._map_pin_normal.style.opacity == 0.0) { me._map_pin_normal.style.visibility="hidden"; } }, 505);
					me._map_pin_normal.style.opacity=0;
				}
				else {
					me._map_pin_normal.style.visibility=me._map_pin_normal.ggVisible?'inherit':'hidden';
					me._map_pin_normal.style.opacity=1;
				}
			}
		}
		me._map_pin_normal.onmouseover=function (e) {
			me.elementMouseOver['map_pin_normal']=true;
			me._map_pin_normal.logicBlock_scaling();
		}
		me._map_pin_normal.onmouseout=function (e) {
			me.elementMouseOver['map_pin_normal']=false;
			me._map_pin_normal.logicBlock_scaling();
		}
		me._map_pin_normal.ontouchend=function (e) {
			me.elementMouseOver['map_pin_normal']=false;
			me._map_pin_normal.logicBlock_scaling();
		}
		me._map_pin_normal.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
		}
		me._map_pin.appendChild(me._map_pin_normal);
		el=me._map_pin_tt=document.createElement('div');
		els=me._map_pin_tt__text=document.createElement('div');
		el.className='ggskin ggskin_textdiv';
		el.ggTextDiv=els;
		el.ggId="map_pin_tt";
		el.ggDx=0;
		el.ggDy=27;
		el.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		el.ggVisible=true;
		el.className="ggskin ggskin_text ";
		el.ggType='text';
		hs ='';
		hs+='height : 20px;';
		hs+='left : -10000px;';
		hs+='opacity : 0;';
		hs+='position : absolute;';
		hs+='top : -10000px;';
		hs+='visibility : hidden;';
		hs+='width : 100px;';
		hs+='pointer-events:none;';
		el.setAttribute('style',hs);
		el.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs += 'box-sizing: border-box;';
		hs+='cursor: default;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: auto;';
		hs+='height: auto;';
		hs+='pointer-events: none;';
		hs+='background: #ffffff;';
		hs+='border: 0px solid #000000;';
		hs+='border-radius: 10px;';
		hs+=cssPrefix + 'border-radius: 10px;';
		hs+='color: #000000;';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 2px 5px 2px 5px;';
		hs+='overflow: hidden;';
		els.setAttribute('style',hs);
		els.innerHTML=me.ggUserdata.title;
		el.appendChild(els);
		me._map_pin_tt.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		el.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.ggNodeId;
		}
		me._map_pin_tt.logicBlock_position = function() {
			var newLogicStatePosition;
			if (
				((player.getHasTouch() == true))
			)
			{
				newLogicStatePosition = 0;
			}
			else {
				newLogicStatePosition = -1;
			}
			if (me._map_pin_tt.ggCurrentLogicStatePosition != newLogicStatePosition) {
				me._map_pin_tt.ggCurrentLogicStatePosition = newLogicStatePosition;
				me._map_pin_tt.style[domTransition]='left 0s, top 0s, opacity 500ms ease 0ms';
				if (me._map_pin_tt.ggCurrentLogicStatePosition == 0) {
					this.ggDx = 0;
					this.ggDy = -38;
					me._map_pin_tt.ggUpdatePosition(true);
				}
				else {
					me._map_pin_tt.ggDx=0;
					me._map_pin_tt.ggDy=27;
					me._map_pin_tt.ggUpdatePosition(true);
				}
			}
		}
		me._map_pin_tt.logicBlock_alpha = function() {
			var newLogicStateAlpha;
			if (
				((me.elementMouseOver['map_pin'] == true)) && 
				((me.ggUserdata.title != ""))
			)
			{
				newLogicStateAlpha = 0;
			}
			else {
				newLogicStateAlpha = -1;
			}
			if (me._map_pin_tt.ggCurrentLogicStateAlpha != newLogicStateAlpha) {
				me._map_pin_tt.ggCurrentLogicStateAlpha = newLogicStateAlpha;
				me._map_pin_tt.style[domTransition]='left 0s, top 0s, opacity 500ms ease 0ms';
				if (me._map_pin_tt.ggCurrentLogicStateAlpha == 0) {
					me._map_pin_tt.style.visibility=me._map_pin_tt.ggVisible?'inherit':'hidden';
					me._map_pin_tt.style.opacity=1;
				}
				else {
					setTimeout(function() { if (me._map_pin_tt.style.opacity == 0.0) { me._map_pin_tt.style.visibility="hidden"; } }, 505);
					me._map_pin_tt.style.opacity=0;
				}
			}
		}
		me._map_pin_tt.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var pw=this.parentNode.clientWidth;
				var w=this.offsetWidth + 0;
					this.style.left=(this.ggDx + pw/2 - w/2) + 'px';
				var ph=this.parentNode.clientHeight;
				var h=this.offsetHeight;
					this.style.top=(this.ggDy + ph/2 - h/2) + 'px';
			}
			this.style[domTransition]='left 0';
			this.ggTextDiv.style.left=((98-this.ggTextDiv.offsetWidth)/2) + 'px';
		}
		me._map_pin.appendChild(me._map_pin_tt);
	};
	me.addSkin();
	var style = document.createElement('style');
	style.type = 'text/css';
	style.appendChild(document.createTextNode('.ggskin { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;} .ggskin.info{ font-family: "Poppins", sans-serif; font-size: 14px; }'));
	document.head.appendChild(style);
	me._information_2.logicBlock_scaling();
	me._info_text_body_continue_more_2.logicBlock_text();
	me._information_0.logicBlock_scaling();
	me._info_text_body_continue_more.logicBlock_text();
	me._information_vr.logicBlock_scaling();
	me._button_silhouette_next_previous.logicBlock_position();
	me._button_silhouette_next_previous.logicBlock_scaling();
	me._footer_bg.logicBlock_size();
	me._footer_bg.logicBlock_alpha();
	me._footer_bg.logicBlock_backgroundcolor();
	me._menu_footer2.logicBlock_visible();
	me._menu_footer.logicBlock_visible();
	me._image_1.logicBlock_position();
	me._banner_text_1.logicBlock_visible();
	me._banner_text_2.logicBlock_visible();
	me._menu.logicBlock_position();
	me._information_.logicBlock_scaling();
	me._info_text_body_continue_more_3.logicBlock_text();
	me._init_ga.logicBlock_visible();
	me._information_2.logicBlock_visible();
	me._information_0.logicBlock_visible();
	me._text_2.logicBlock_alpha();
	me._text_2.logicBlock_text();
	me._information_vr.logicBlock_visible();
	me._image_qr.logicBlock_externalurl();
	me._text_qr.logicBlock_text();
	me._info_title2_vr.logicBlock_text();
	me._info_title_vr.logicBlock_text();
	me._image_3.logicBlock_visible();
	me._text_1_6_2.logicBlock_text();
	me._text_1_5_2.logicBlock_text();
	me._text_1_4_2.logicBlock_text();
	me._text_1_3_2.logicBlock_text();
	me._text_1_2_2.logicBlock_text();
	me._text_1_6_1.logicBlock_text();
	me._text_1_5_1.logicBlock_text();
	me._text_1_4_1.logicBlock_text();
	me._text_1_3_1.logicBlock_text();
	me._text_1_2_1.logicBlock_text();
	me._banner_text_1.logicBlock_text();
	me._banner_text_2.logicBlock_text();
	me._banner_menu1.logicBlock_text();
	me._txt_banner_menu_lan.logicBlock_visible();
	me._map.logicBlock_position();
	me._text_4.logicBlock_text();
	me._text_4_1.logicBlock_text();
	me._text_4_2.logicBlock_text();
	me._text_4_3.logicBlock_text();
	me._ctnos.logicBlock_visible();
	me._img_mute.logicBlock_visible();
	me._img_voice.logicBlock_visible();
	me._text_1.logicBlock_alpha();
	me._text_1.logicBlock_text();
	me._information_.logicBlock_visible();
	player.addListener('sizechanged', function(args) { me._information_2.logicBlock_scaling();me._info_text_body_continue_more_2.logicBlock_text();me._information_0.logicBlock_scaling();me._info_text_body_continue_more.logicBlock_text();me._information_vr.logicBlock_scaling();me._button_silhouette_next_previous.logicBlock_position();me._button_silhouette_next_previous.logicBlock_scaling();me._footer_bg.logicBlock_size();me._footer_bg.logicBlock_alpha();me._footer_bg.logicBlock_backgroundcolor();me._menu_footer2.logicBlock_visible();me._menu_footer.logicBlock_visible();me._image_1.logicBlock_position();me._banner_text_1.logicBlock_visible();me._banner_text_2.logicBlock_visible();me._menu.logicBlock_position();me._information_.logicBlock_scaling();me._info_text_body_continue_more_3.logicBlock_text(); });
	player.addListener('changenode', function(args) { me._init_ga.logicBlock_visible();me._information_2.logicBlock_visible();me._information_0.logicBlock_visible();me._text_2.logicBlock_alpha();me._text_2.logicBlock_text();me._information_vr.logicBlock_visible();me._image_qr.logicBlock_externalurl();me._text_qr.logicBlock_text();me._info_title2_vr.logicBlock_text();me._info_title_vr.logicBlock_text();me._image_3.logicBlock_visible();me._text_1_6_2.logicBlock_text();me._text_1_5_2.logicBlock_text();me._text_1_4_2.logicBlock_text();me._text_1_3_2.logicBlock_text();me._text_1_2_2.logicBlock_text();me._text_1_6_1.logicBlock_text();me._text_1_5_1.logicBlock_text();me._text_1_4_1.logicBlock_text();me._text_1_3_1.logicBlock_text();me._text_1_2_1.logicBlock_text();me._banner_text_1.logicBlock_text();me._banner_text_2.logicBlock_text();me._banner_menu1.logicBlock_text();me._txt_banner_menu_lan.logicBlock_visible();me._map.logicBlock_position();me._text_4.logicBlock_text();me._text_4_1.logicBlock_text();me._text_4_2.logicBlock_text();me._text_4_3.logicBlock_text();me._ctnos.logicBlock_visible();me._img_mute.logicBlock_visible();me._img_voice.logicBlock_visible();me._text_1.logicBlock_alpha();me._text_1.logicBlock_text();me._information_.logicBlock_visible(); });
	player.addListener('changevisitednodes', function(args) { me._img_mute.logicBlock_visible();me._img_voice.logicBlock_visible(); });
	player.addListener('activehotspotchanged', function(args) { me._image_qr.logicBlock_externalurl();me._image_3.logicBlock_visible();me._img_mute.logicBlock_visible();me._img_voice.logicBlock_visible(); });
	player.addListener('varchanged_UA_ID', function(args) { me._init_ga.logicBlock_visible(); });
	player.addListener('varchanged_UA_category', function(args) { me._init_ga.logicBlock_visible(); });
	player.addListener('varchanged_vis_info_popup_2', function(args) { me._information_2.logicBlock_visible(); });
	player.addListener('varchanged_vis_info_popup', function(args) { me._information_0.logicBlock_visible(); });
	player.addListener('varchanged_vis_info_vr_popup', function(args) { me._information_vr.logicBlock_visible(); });
	player.addListener('varchanged_map_open', function(args) { me._map.logicBlock_position(); });
	player.addListener('varchanged_vis_info_popup_3', function(args) { me._information_.logicBlock_visible(); });
	player.addListener('varchanged_ExtValue', function(args) { me._text_2.logicBlock_text();me._text_qr.logicBlock_text();me._info_title2_vr.logicBlock_text();me._info_title_vr.logicBlock_text();me._text_1_6_2.logicBlock_text();me._text_1_5_2.logicBlock_text();me._text_1_4_2.logicBlock_text();me._text_1_3_2.logicBlock_text();me._text_1_2_2.logicBlock_text();me._text_1_6_1.logicBlock_text();me._text_1_5_1.logicBlock_text();me._text_1_4_1.logicBlock_text();me._text_1_3_1.logicBlock_text();me._text_1_2_1.logicBlock_text();me._banner_text_1.logicBlock_text();me._banner_text_2.logicBlock_text();me._banner_menu1.logicBlock_text();me._txt_banner_menu_lan.logicBlock_visible();me._text_4.logicBlock_text();me._text_4_1.logicBlock_text();me._text_4_2.logicBlock_text();me._text_4_3.logicBlock_text();me._text_1.logicBlock_text(); });
	player.addListener('varchanged_ht_anima_C', function(args) { me._text_2.logicBlock_alpha();me._text_1.logicBlock_alpha(); });
	player.addListener('changenode', function(args) { me._map_all.callChildLogicBlocksHotspot_map_pin_changenode(); });
	player.addListener('configloaded', function(args) { me._map_all.callChildLogicBlocksHotspot_map_pin_configloaded(); });
	player.addListener('mouseover', function(args) { me._map_all.callChildLogicBlocksHotspot_map_pin_mouseover(); });
	player.addListener('mouseover', function(args) { me._map_all.callChildLogicBlocksHotspot_map_pin_mouseover(); });
	player.addListener('changenode', function(args) { me._map_all.callChildLogicBlocksHotspot_map_pin_active(); });
	player.addListener('hastouch', function(args) { me._map_all.callChildLogicBlocksHotspot_map_pin_hastouch(); });
	player.addListener('activehotspotchanged', function(args) { me._map_all.callChildLogicBlocksHotspot_map_pin_activehotspotchanged(); });
	player.addListener('sizechanged', function(args) { me.callChildLogicBlocksHotspot_hotspot_npc_sizechanged();me.callChildLogicBlocksHotspot_ht_info_2_sizechanged();me.callChildLogicBlocksHotspot_ht_info__sizechanged();me.callChildLogicBlocksHotspot_ht_info_vr_sizechanged();me.callChildLogicBlocksHotspot_ht_near_0_sizechanged();me.callChildLogicBlocksHotspot_ht_near_45_sizechanged();me.callChildLogicBlocksHotspot_ht_near_90_sizechanged();me.callChildLogicBlocksHotspot_ht_near_135_sizechanged();me.callChildLogicBlocksHotspot_ht_near_180_sizechanged();me.callChildLogicBlocksHotspot_ht_near_225_sizechanged();me.callChildLogicBlocksHotspot_ht_near_270_sizechanged();me.callChildLogicBlocksHotspot_ht_near_300_sizechanged();me.callChildLogicBlocksHotspot_ht_near_315_sizechanged(); });
	player.addListener('changenode', function(args) { me.callChildLogicBlocksHotspot_hotspot_npc_changenode();me.callChildLogicBlocksHotspot_ht_info_2_changenode();me.callChildLogicBlocksHotspot_ht_info__changenode();me.callChildLogicBlocksHotspot_ht_info_vr_changenode();me.callChildLogicBlocksHotspot_ht_near_0_changenode();me.callChildLogicBlocksHotspot_ht_near_45_changenode();me.callChildLogicBlocksHotspot_ht_near_90_changenode();me.callChildLogicBlocksHotspot_ht_near_135_changenode();me.callChildLogicBlocksHotspot_ht_near_180_changenode();me.callChildLogicBlocksHotspot_ht_near_225_changenode();me.callChildLogicBlocksHotspot_ht_near_270_changenode();me.callChildLogicBlocksHotspot_ht_near_300_changenode();me.callChildLogicBlocksHotspot_ht_near_315_changenode(); });
	player.addListener('changevisitednodes', function(args) { me.callChildLogicBlocksHotspot_hotspot_npc_changevisitednodes(); });
	player.addListener('activehotspotchanged', function(args) { me.callChildLogicBlocksHotspot_hotspot_npc_activehotspotchanged();me.callChildLogicBlocksHotspot_ht_info_2_activehotspotchanged();me.callChildLogicBlocksHotspot_ht_info__activehotspotchanged();me.callChildLogicBlocksHotspot_ht_info_vr_activehotspotchanged(); });
	player.addListener('varchanged_ht_anima_C', function(args) { me.callChildLogicBlocksHotspot_ht_near_0_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_45_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_90_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_135_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_180_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_225_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_270_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_300_varchanged_ht_anima_C();me.callChildLogicBlocksHotspot_ht_near_315_varchanged_ht_anima_C(); });
	player.addListener('hotspotsremoved', function(args) { me.removeSkinHotspots(); });
	me.skinTimerEvent();
};